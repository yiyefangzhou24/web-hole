# WebHole

## 是什么？能干什么？

- WebHole是一款加密网站管理软件，类似但不同于菜刀、冰蝎、蚁剑，能通过C/S的工作模式，方便的管理服务器的文件、数据库，并执行命令。

![主界面](https://gitee.com/yiyefangzhou24/web-hole/raw/master/src/res/screenshot/1.png "主界面")

![攻击载荷管理](https://gitee.com/yiyefangzhou24/web-hole/raw/master/src/res/screenshot/3.png "攻击载荷管理")

![文件管理](https://gitee.com/yiyefangzhou24/web-hole/raw/master/src/res/screenshot/2.png "文件管理")

![数据库管理](https://gitee.com/yiyefangzhou24/web-hole/raw/master/src/res/screenshot/4.png "数据库管理")

![命令终端](https://gitee.com/yiyefangzhou24/web-hole/raw/master/src/res/screenshot/5.png "命令终端")

![自定义脚本执行](https://gitee.com/yiyefangzhou24/web-hole/raw/master/src/res/screenshot/6.png "自定义脚本执行")

## 有什么特点？

- 服务端和客户端通讯过程使用AES对称加密，服务端和客户端通讯内容为全加密的十六进制字符串，无特征，WAF无法识别拦截，且可以通过调整iv加密向量，调整加密后的内容特征。

- 攻击载荷可完全自定义，配适不同脚本类型无需重新编译控制端程序，并可以通过编辑攻击载荷和webshell，可以完全自定义通讯内容。

- 能够自行定义HTTP头。

- 能够自定义每个WebHole的请求cookies。

## 版本更新日志

- [[更新日志]](https://gitee.com/yiyefangzhou24/web-hole/blob/master/doc/%E7%89%88%E6%9C%AC%E6%9B%B4%E6%96%B0%E6%97%A5%E5%BF%97.MD "更新日志")

## 魔改小贴士

###### 魔改内容

控制端APP无需修改，只需要通过编辑攻击载荷功能 + 修改Webshell即可修改对应功能。

###### 通讯格式规范

- 请求内容

请求内容格式没有固定格式。如PHP/ASP/.NET等支持eval执行自定义代码的，可直接在攻击载荷中编写脚本控制被控端输出；如JSP/CFM不支持eval执行自定义代码的，将攻击载荷变成POST参数，提交被控端脚本执行。

- 响应内容

被控端输出内容（请求响应）必须遵照以下格式输出，才能被控制端APP正确解析。[[网络通讯格式规范]](https://gitee.com/yiyefangzhou24/web-hole/blob/master/doc/%E9%80%9A%E8%AE%AF%E6%A0%BC%E5%BC%8F.MD "[传送门]")

## Program进度

作者是个业余爱好者，平时打螺丝工作很忙，只能使用业余时间coding。鉴于个人能力、经历有限，只实现了部分功能，期待有兴趣的Contributors一同参与。

- 目前已经实现的工作：

1. 完成了控制端程序设计。

2. 完成了PHP的Webshell编写。

3. 完成了JSP的Webshell编写

4. 完成了ASP.NET的webshell编写

- 还有没完成的工作：

1. ASP的Webshell没实现（ASP在不使用三方库的情况下，暂无法实现AES流量加密）。

## 几个要说明的问题

- 传输内容编码问题。客户端和服务端的通联因为要采用base64加密，所有字符串内容统一使用UTF-8编码，客户端和服务端在传输内容前要做相应转换。

- 服务端编码的选择。服务端添加/修改Webshell配置的时候，选择的编码指的是客户端（受控端）的操作系统编码，如Linux选择UTF-8，简体中文Windows选择GB2312。

- 数据库编码。该功能为预留参数，选择与否没有关系，客户端在配置数据库连接串时应当规定查询结果使用UTF-8编码输出。

## 编译环境

- 服务端（控制端）采用QT 5.14.1编译。

- **编译打包发布后，后要把根目录bin目录中的所有文件拷贝到程序目录下。**

## 联系方式

- 电子邮件：yiyefangzhou24@qq.com

- 技术博客：[https://blog.csdn.net/yiyefangzhou24](https://blog.csdn.net/yiyefangzhou24)
