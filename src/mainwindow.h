#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTableWidgetItem>
#include <QDebug>
#include <QUrl>
#include <QDesktopServices>
#include <QSystemTrayIcon>
#include "mdhole.h"
#include "qlog.h"
#include "httpcore.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

protected:
    void changeEvent(QEvent *event) override;   //最小化事件响应

private slots:
    // 双击列表项信号槽
    void on_tableWidget_itemDoubleClicked(QTableWidgetItem *item);

    // 右击菜单项
    void on_tableWidget_customContextMenuRequested(const QPoint &pos);

    // 更新所有Webshell信息信号槽
    void on_update_all_hole();

    // 更新指定Webshell信息信号槽
    void on_update_one_hole(MdHole mdHole);

    // 编辑HTTP头信息信号槽
    void on_action_http_header_triggered();

    // 打开日志文件信号槽
    void on_action_log_triggered();

    // 错误信息信号槽
    void on_error(ERR_CODE code, QString url, QString msg);

    // 网络请求响应信号
    void on_response(int type, QString url, QVariant data);

    // 关于软件菜单信号槽
    void on_action_about_app_triggered();

    // 编辑攻击载荷信号槽
    void on_action_payload_triggered();

    // 退出程序信号槽
    void on_action_exit_triggered();

    // 测试所有webshell信号槽
    void on_action_testall_triggered();

    // 外观选择信号槽
    void on_styleActionGroup_triggered(QAction *action);

    // 日志等级选择信号槽
    void on_loglevelActionGroup_triggered(QAction *action);

    // 设置代理信号槽
    void on_action_proxy_triggered();

    // 双击托盘图标信号
    void on_trayIcon_activated(QSystemTrayIcon::ActivationReason reason);

private:
    /**
     * @brief insertListItem UI函数 - 插入一个Webshell
     * @param mdHole webshell详细信息类
     */
    void insertListItem(MdHole mdHole);

    /**
     * @brief updateListItem UI函数 - 修改一个Webshell
     * @param mdHole
     */
    void updateListItem(MdHole mdHole);


private:
    Ui::MainWindow *ui;

    QList<MdHole> mdHoleList;

    QStringList strErrType = {"数据格式错误" , "网络错误", "脚本执行错误", "控制端程序错误"};

    QSystemTrayIcon *trayIcon;
};
#endif // MAINWINDOW_H
