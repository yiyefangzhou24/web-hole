#ifndef MDHOLE_H
#define MDHOLE_H

// **
//
// Webshell Model类
//    仅用来存放Webshell数据集
//
// created by yiyefangzhou24
// created time 2024/2/9
//
// **

#include <QString>

class MdHole
{
public:

    int getId();

    void setId(int id);
    
    QString getUrl();
    
    void setUrl(QString url);
    
    QString getPasswd();
    
    void setPasswd(QString passwd);
    
    QString getDbConfig();
    
    void setDbConfig(QString dbconfig);

    QString getCookies();

    void setCookies(QString cookies);
    
    QString getInfo();

    void setInfo(QString info);

    QString getCode();

    void setCode(QString code);

    QString getStatus();

    void setStatus(QString status);

    QString getTime();

    void setTime(QString time);

    QString getIv();

    void setIv(QString iv);

    QString getDbType();

    void setDbType(QString dbtype);

    QString getDbCode();

    void setDbCode(QString dbcode);

    QString getDbHost();

    void setDbHost(QString dbhost);

    QString getDbUser();

    void setDbUser(QString dbuser);

    QString getDbPasswd();

    void setDbPasswd(QString dbpasswd);

    QString getScript();

    void setScript(QString script);
    
private:

    int id;
    
    QString url;
    
    QString passwd;

    QString dbtype;

    QString dbcode;

    QString dbhost;

    QString dbuser;

    QString dbpasswd;

    QString cookies;
    
    QString info;
    
    QString code;

    QString status;

    QString time;

    QString iv;

    QString script;
};

#endif // MDHOLE_H
