#include "texteditdialog.h"
#include "ui_texteditdialog.h"

TextEditDialog::TextEditDialog(MdHole mdHole, bool isNew, QString path,  QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TextEditDialog)
{
    ui->setupUi(this);

    // 初始化成员变量
    this->mdHole = mdHole;
    this->isNew = isNew;
    this->path = path;

    // UI设置
    ui->progressBar->setVisible(false);
    ui->label_status->setText("当前文本编码 [" + mdHole.getCode() + "]");
    Qt::WindowFlags flags=Qt::Dialog;
    flags |=Qt::WindowCloseButtonHint;
    flags |=Qt::WindowMaximizeButtonHint;
    setWindowFlags(flags);
    addMenu();      // 添加菜单

    // 绑定响应信号
    connect(&http, &HttpCore::sigResponse, this, &TextEditDialog::on_response);

    // ** 临时文件说明 **
    // 因为下载动作是在另一个线程中进行的，不能和窗口公用一个临时文件。
    // 所以程序会创建两个临时文件：
    //    1.editTmpFile 当前编辑窗口显示的文件数据，该文件随窗口一起打开关闭，不能共享读写
    //    2.downTmpFileName 用于下载文件的临时数据，该文件创建后立即关闭，每次读写后关闭，窗口关闭时删除
    //
    if(!editTmpFile.open())
    {
        QMessageBox::critical(this, "错误", "创建临时文件失败");
    }

    // 如果是编辑文件，先下载文件
    if(!isNew)
    {
        QTemporaryFile downTmpFile;
        if(downTmpFile.open())
        {
            downTmpFileName = downTmpFile.fileName();
            downTmpFile.setAutoRemove(false);
            downTmpFile.close();
            http.downloadRequest(path, downTmpFileName);
        }else
        {
            QMessageBox::critical(this, "错误", "创建下载临时文件失败");
        }
    }
}

TextEditDialog::~TextEditDialog()
{
    if(editTmpFile.isOpen())
        editTmpFile.close();
    if(QFile::exists(downTmpFileName))
        QFile::remove(downTmpFileName);
    delete ui;
}

void TextEditDialog::closeEvent(QCloseEvent *e)
{
    // 这里是个坑，必须手动disconnect信号关联
    // 因为qt内存new的对话框在短期内没有释放，再次new还是之前的对话框数据，会产生2次信号重复绑定
    disconnect(&http, &HttpCore::sigResponse, this, &TextEditDialog::on_response);
}

// ********************** 以下为UI私有函数 ******************************
void TextEditDialog::addMenu()
{
    QMenuBar * menuBar = new QMenuBar(this);
    QMenu * setMenu = menuBar->addMenu("设置");
    QMenu * netMenu = menuBar->addMenu("网络");
    QAction * saveAction= setMenu->addAction("保存");
    setMenu->addSeparator();
    QAction * fontSizeAction= setMenu->addAction("字体大小");
    QMenu * codeMenu= setMenu->addMenu("编码");
    setMenu->addSeparator();
    QAction * exitAction= setMenu->addAction("退出");
    QMenu * blockSizeMenu= netMenu->addMenu("分次上传大小");

    QAction * block256Action= blockSizeMenu->addAction("256KB");
    QAction * block512Action= blockSizeMenu->addAction("512KB");
    QAction * block1MAction= blockSizeMenu->addAction("1MB");
    QAction * block2MAction= blockSizeMenu->addAction("2MB");
    QActionGroup* blockActionGroup = new QActionGroup(this);
    blockActionGroup->addAction(block256Action);
    blockActionGroup->addAction(block512Action);
    blockActionGroup->addAction(block1MAction);
    blockActionGroup->addAction(block2MAction);
    block256Action->setCheckable(true);
    block512Action->setCheckable(true);
    block1MAction->setCheckable(true);
    block2MAction->setCheckable(true);
    block512Action->setChecked(true);
    QAction * utf8Action= codeMenu->addAction("UTF-8");
    QAction * gbkAction= codeMenu->addAction("GB2312");
    QAction * big5Action= codeMenu->addAction("BIG5");
    QAction * isoAction= codeMenu->addAction("ISO-8859-1");
    QActionGroup* codeActionGroup = new QActionGroup(this);
    codeActionGroup->addAction(utf8Action);
    codeActionGroup->addAction(gbkAction);
    codeActionGroup->addAction(big5Action);
    codeActionGroup->addAction(isoAction);
    utf8Action->setCheckable(true);
    gbkAction->setCheckable(true);
    big5Action->setCheckable(true);
    isoAction->setCheckable(true);
    ui->verticalLayout_menu->insertWidget(0, menuBar);

    if(mdHole.getCode() == "UTF-8"){
        utf8Action->setChecked(true);
    }else if(mdHole.getCode() == "GB2312"){
        gbkAction->setChecked(true);
    }else if(mdHole.getCode() == "BIG5"){
        big5Action->setChecked(true);
    }else if(mdHole.getCode() == "ISO-8859-1"){
        isoAction->setChecked(true);
    }
    // 选择分块大小
    connect(blockActionGroup, &QActionGroup::triggered ,[=](QAction *action){
        if(action->text() == "256KB")
        {
            blockSize = 128 * 1024;
        }else if(action->text() == "512KB")
        {
            blockSize = 256 * 1024;
        }else if(action->text() == "1MB")
        {
            blockSize = 512 * 1024;
        }else if(action->text() == "2MB")
        {
            blockSize = 1024 * 1024;
        }
    });
    // 服务器编码
    connect(codeActionGroup, &QActionGroup::triggered ,[=](QAction *action){
        mdHole.setCode(action->text());
        ui->label_status->setText("当前文本编码 [" + mdHole.getCode() + "]");
        // 转换成Unicode显示
        //readFile();
        editTmpFile.seek(0);
        QTextCodec *codec = QTextCodec::codecForName(mdHole.getCode().toLatin1());
        ui->plainTextEdit->setPlainText(codec->toUnicode(editTmpFile.readAll()));
    });

    // 保存文件
    connect(saveAction, &QAction::triggered, [=](){
        saveFile();
    });

    // 退出程序
    connect(exitAction, &QAction::triggered, [=](){
       close();
    });

    // 字体大小
    connect(fontSizeAction, &QAction::triggered, [=](){
        bool isOk = false;
        int size = QInputDialog::getInt(this, "字体大小", "编辑器显示文字大小", ui->plainTextEdit->font().pointSize(), 1, 120, 1, &isOk);
        if(isOk)
        {
            QFont font;
            font.setPointSize(size);
            ui->plainTextEdit->setFont(font);
        }
    });
}

void TextEditDialog::saveFile()
{
    if(isNew)        // 新建文件
    {
        bool isOk = false;
        QString text = QInputDialog::getText(this,
                                             "保存的文件名",
                                             "",
                                             QLineEdit::Normal,
                                             "",
                                             &isOk);
        if(isOk)
        {
            // 转换成服务器编码存储
            QTextCodec *codec = QTextCodec::codecForName(mdHole.getCode().toLatin1());
            editTmpFile.write(codec->fromUnicode(ui->plainTextEdit->toPlainText()));

            // 上传文件
            editTmpFile.seek(0);
            QString reFile = path.contains("\\") ? path + "\\" + text : path + "/" + text;
            QString loFile = editTmpFile.fileName();
            int tol = qCeil(editTmpFile.size() *1.0 / blockSize);
            http.uploadRequest(reFile, loFile, "w+", 0, tol, editTmpFile.read(blockSize));
        }
    }else                   // 修改文件
    {
        editTmpFile.resize(0);
        // 转换成服务器编码存储
        QTextCodec *codec = QTextCodec::codecForName(mdHole.getCode().toLatin1());
        editTmpFile.write(codec->fromUnicode(ui->plainTextEdit->toPlainText()));

        // 上传文件
        editTmpFile.seek(0);
        QString reFile = path;
        QString loFile = editTmpFile.fileName();
        int tol = qCeil(editTmpFile.size() *1.0 / blockSize);
        http.uploadRequest(reFile, loFile, "w+", 0, tol, editTmpFile.read(blockSize));
    }
}

// ********************** 以下为网络通信槽函数 *****************************
void TextEditDialog::on_response(int type, QString url, QVariant data)
{
    if(url == mdHole.getUrl())
    {
        if(type == RET_DOWNLOAD)      // |-- 下载文件
        {
            if(data.toStringList().length() == 3)
            {
                int tol = data.toStringList().at(1).toInt();
                int idx = data.toStringList().at(2).toInt();
                ui->progressBar->setVisible(true);
                ui->progressBar->setRange(0, tol);
                ui->progressBar->setValue(idx + 1);
                if(idx +1 >= tol)
                {
                    //下载完成后延时2秒隐藏进度条
                    QTimer * timer = new QTimer(this);
                    timer->setInterval(2000);
                    connect(timer, &QTimer::timeout, [=](){
                        ui->progressBar->setVisible(false);
                        timer->stop();
                        timer->deleteLater();
                    });
                    timer->start();
                    //读取文件内容
                    QFile file(downTmpFileName);
                    if(!file.open(QIODevice::ReadOnly))
                    {
                        QMessageBox::critical(this, "错误", "打开下载临时文件失败");
                        return;
                    }
                    QByteArray fileData = file.readAll();
                    // 同步到窗口临时文件中
                    editTmpFile.write(fileData);
                    // 转换成Unicode显示
                    QTextCodec *codec = QTextCodec::codecForName(mdHole.getCode().toLatin1());
                    ui->plainTextEdit->setPlainText(codec->toUnicode(fileData));
                    file.close();
                }
            }
        }else if(type == RET_UPLOAD)      // |-- 上传文件
        {
            if(data.toStringList().length() == 4)
            {
                int tol = data.toStringList().at(2).toInt();
                int idx = data.toStringList().at(3).toInt();
                ui->progressBar->setVisible(true);
                ui->progressBar->setRange(0, tol);
                ui->progressBar->setValue(idx + 1);
                if(idx +1 >= tol)
                {
                    //下载完成后延时2秒隐藏进度条
                    QTimer * timer = new QTimer(this);
                    timer->setInterval(2000);
                    connect(timer, &QTimer::timeout, [=](){
                        ui->progressBar->setVisible(false);
                        timer->stop();
                        timer->deleteLater();
                    });
                    timer->start();
                }else
                {
                    editTmpFile.seek((idx + 1) * blockSize);
                    http.uploadRequest(data.toStringList().at(0), data.toStringList().at(1), "a", idx + 1, tol, editTmpFile.read(blockSize));
                }
            }
        }
    }
}
