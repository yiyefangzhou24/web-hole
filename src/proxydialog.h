#ifndef PROXYDIALOG_H
#define PROXYDIALOG_H

#include <QDialog>
#include <QMessageBox>
#include "mdproxy.h"

namespace Ui {
class ProxyDialog;
}

class ProxyDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ProxyDialog(QWidget *parent = nullptr);
    ~ProxyDialog();

private slots:

    void on_pushButton_cancel_clicked();

    void on_pushButton_save_clicked();

    void on_pushButton_clear_clicked();

private:
    Ui::ProxyDialog *ui;
};

#endif // PROXYDIALOG_H
