#ifndef TEXTEDITDIALOG_H
#define TEXTEDITDIALOG_H

#include <QDialog>
#include <QMenuBar>
#include <QTemporaryFile>
#include <QMessageBox>
#include <QInputDialog>
#include "mdhole.h"
#include "global.h"

namespace Ui {
class TextEditDialog;
}

class TextEditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TextEditDialog(MdHole mdHole, bool isNew, QString path,  QWidget *parent = nullptr);
    ~TextEditDialog();

    void closeEvent ( QCloseEvent * e ) override;

private slots:

    // 网络请求响应信号
    void on_response(int type, QString url, QVariant data);

private:

    /**
     * @brief addMenu 私有函数 - 初始化并添加菜单栏
     */
    void addMenu();

    /**
     * @brief saveFile 私有函数 - 保存文件到远程服务器
     */
    void saveFile();


private:
    Ui::TextEditDialog *ui;

    MdHole mdHole;                  // webshell信息

    QTemporaryFile editTmpFile;     // 编辑窗口临时文件

    QString downTmpFileName;        //下载临时文件

    int blockSize = 256 * 1024;     // 分次上传下载长度

    QString path;                   // 远端文件路径

    bool isNew = false;             // 新建文件/编辑文件
};

#endif // TEXTEDITDIALOG_H
