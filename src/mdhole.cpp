#include "mdhole.h"

int MdHole::getId()
{
    return id;
}

void MdHole::setId(int id)
{
    this->id = id;
}

QString MdHole::getUrl()
{
    return url;
}

void MdHole::setUrl(QString url)
{
    this->url = url;
}

QString MdHole::getPasswd()
{
    return passwd;
}

void MdHole::setPasswd(QString passwd)
{
    this->passwd = passwd;
}

QString MdHole::getCookies()
{
    return cookies;
}

void MdHole::setCookies(QString cookies)
{
    this->cookies = cookies;
}

QString MdHole::getInfo()
{
    return info;
}

void MdHole::setInfo(QString info)
{
    this->info = info;
}

QString MdHole::getCode()
{
    return code;
}

void MdHole::setCode(QString code)
{
    this->code = code;
}

void MdHole::setStatus(QString status)
{
    this->status = status;
}

QString MdHole::getStatus()
{
    return status;
}

QString MdHole::getTime()
{
    return time;
}

void MdHole::setTime(QString time)
{
    this->time = time;
}

QString MdHole::getIv()
{
    return iv;
}

void MdHole::setIv(QString iv)
{
    this->iv = iv;
}

QString MdHole::getDbType()
{
    return dbtype;
}

void MdHole::setDbType(QString dbtype)
{
    this->dbtype = dbtype;
}

QString MdHole::getDbCode()
{
    return dbcode;
}

void MdHole::setDbCode(QString dbcode)
{
    this->dbcode = dbcode;
}

QString MdHole::getDbHost()
{
    return dbhost;
}

void MdHole::setDbHost(QString dbhost)
{
    this->dbhost = dbhost;
}

QString MdHole::getDbUser()
{
    return dbuser;
}

void MdHole::setDbUser(QString dbuser)
{
    this->dbuser = dbuser;
}

QString MdHole::getDbPasswd()
{
    return dbpasswd;
}

void MdHole::setDbPasswd(QString dbpasswd)
{
    this->dbpasswd = dbpasswd;
}

QString MdHole::getScript()
{
    return script;
}

void MdHole::setScript(QString script)
{
    this->script = script;
}
