#ifndef FILEMANAGEWINDOW_H
#define FILEMANAGEWINDOW_H

#include <QDialog>
#include <QStatusBar>
#include <QStyleFactory>
#include <QFileInfo>
#include <QFileIconProvider>
#include <QTreeWidgetItem>
#include <QTableWidgetItem>
#include <QInputDialog>
#include <QMessageBox>
#include <QFileDialog>
#include <QDesktopServices>
#include <QMenu>
#include "global.h"
#include "httpcore.h"
#include "mdhole.h"
#include "mdfileinfo.h"

namespace Ui {
class FileManageWindow;
}

class FileManageWindow : public QDialog
{
    Q_OBJECT

    enum OS_TYPE{
        WIN,LINUX
    };

public:
    explicit FileManageWindow(MdHole mdHole, QWidget *parent = nullptr);
    ~FileManageWindow();

    void closeEvent ( QCloseEvent * e ) override;

private slots:

    // 网络请求响应信号
    void on_response(int type, QString url, QVariant data);

    // UI信号 - 地址栏转到按钮
    void on_goButton_clicked();

    // UI信号 - 树形列表点击
    void on_treeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous);

    // UI信号 - 列表右击菜单
    void on_tableWidget_customContextMenuRequested(const QPoint &pos);

    // UI信号 - 列表双击
    void on_tableWidget_itemDoubleClicked(QTableWidgetItem *item);

private:

    /**
     * 功能：添加树形目录根节点
     * @param name 添加的根节点名称
     * @return 添加的节点指针
     */
    QTreeWidgetItem * addTreeRootItem(QString name);

    /**
     * 功能：添加树形目录根节点
     * @param path 要添加的节点的text
     * @param QTreeWidgetItem 节点指针
     * @return 添加的节点指针
     */
    QTreeWidgetItem * addTreeChildItem(QString name, QTreeWidgetItem *item);

    /**
     * 功能：将完整文件路径格式化到树形目录上
     * @param path 文件路径和文件名（需要完整路径）
     * @return 添加的节点指针
     */
    void formatTreeItem(QString path);

    /**
     * 功能：将完整文件路径从树形目录上摘下来（删除）
     * @param path 文件路径和文件名（需要完整路径）
     * @return 添加的节点指针
     */
    void unformatTreeItem(QString path);

    /**
     * 功能：查找某个节点下的子节点
     * @param name 要查找的节点text
     * @param QTreeWidgetItem 节点指针
     * @return 查找到的首个子节点
     */
    QTreeWidgetItem * findTreeChildItem(QString name, QTreeWidgetItem * item);

    /**
     * 功能：查找根节点
     * @param name 要查找的节点text
     * @return 查找到的首个子节点
     */
    QTreeWidgetItem * findTreeRootItem(QString name);

    /**
     * 功能：添加文件信息到列表
     * @param file 文件信息
     */
    void insertTableItem(MdFileInfo file);

    /**
     * 功能：删除列表中的文件信息
     * @param file 文件信息
     */
    void removeTableItem(MdFileInfo file);

    /**
     * 功能：查找文件信息
     * @param file 文件信息
     * @return 返回所在的行号，没有返回-1
     */
    int findTableItem(MdFileInfo file);

    /**
     * 功能：设置标题栏中当前目录地址
     * @param pwdUrl 当前目录
     */
    void setPwdUrl(QString pwdUrl);

    /**
     * 功能：获取标题栏中当前目录地址
     * @return 返回当前目录地址
     */
    QString getPwdUrl();


private:
    Ui::FileManageWindow *ui;

    MdHole mdHole;

    OS_TYPE os;

    int tmpBlockSize = 0;       //临时变量，用来记录上传数据分块的大小
};

#endif // FILEMANAGEWINDOW_H
