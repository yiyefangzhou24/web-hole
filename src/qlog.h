#ifndef QLOG_H
#define QLOG_H

// **
//
// 日志消息类
//    该类所有成员函数均为静态函数
//
// created by yiyefangzhou24
// created time 2023/1/13
//
// **

#define APP_FILE    "WebHoleLogs.log"

#include <QString>
#include <QFile>
#include <QTime>
#include <QByteArray>
#include <QDebug>

class QLog
{
public:

    static void log(QString context, QString title = "");
};

#endif // QLOG_H
