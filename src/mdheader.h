#ifndef MDHEADER_H
#define MDHEADER_H
// **
//
// HTTP请求头 Model类
//    仅用来存放HTTP请求头键值对数据集
//
// created by yiyefangzhou24
// created time 2024/2/9
//
// **

#include <QString>

class MdHeader
{
public:

    int getId();

    void setId(int id);

    QString getName();

    void setName(QString name);

    QString getVal();

    void setVal(QString val);
private:

    int id = 0;

    QString name;

    QString val;
};

#endif // MDHEADER_H
