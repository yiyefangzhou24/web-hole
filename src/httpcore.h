#ifndef HTTPCORE_H
#define HTTPCORE_H

// **
//
// HTTP请求的核心类
//    该类主要用于：
//      1、存放HTTP请求的配置，包括网址、post内容、cookies等
//      2、完成请求和返回的加解密
//
// created by yiyefangzhou24
// created time 2024/2/9
//
// **
#include <QString>
#include <QObject>
#include <QMap>
#include "netcore.h"
#include "qaes.h"
#include "mdfileinfo.h"
#include "mdpayload.h"
#include "mdhole.h"
#include "qlog.h"

// 响应信息类型
enum RET_TYPE{
    RET_TEST,
    RET_INFO,
    RET_FILELIST,
    RET_FILEDEL,
    RET_DOWNLOADER,
    RET_EXEC,
    RET_RENAME,
    RET_RETIME,
    RET_NEWFOLDER,
    RET_CMD,
    RET_DOWNLOAD,
    RET_UPLOAD,
    RET_DBEXEC,
    RET_DBEXPORTTB,
    RET_SCRIPT
};

// 错误类型
enum ERR_CODE{
    ERR_FORMAT,         //格式错误
    ERR_NET,            //网络错误
    ERR_SCRIPT,         //服务器脚本执行错误
    ERR_LOCAL,          //程序自身执行错误
};


class HttpCore : public NetCore
{
    Q_OBJECT
public:

    // 初始化webshell信息
    void setHole(MdHole mdHole);

    // 获取当前webshell信息
    MdHole getHole();

    // 测试Webshell状态请求
    void testRequest();

    // 获取服务器信息请求
    void getInfoRequest();

    // 读取目录文件列表
    void getFileListRequest(QString path);

    // 删除文件
    void deleteFileRequest(QString path);

    // 下载文件到服务器
    void downloaderRequest(QString fileurl, QString filename);

    // 远程执行
    void execRequest(QString app);

    // 修改文件信息
    void renameRequest(QString pwd, QString oldFileName, QString newFileName);

    // 修改文件时间
    void retimeRequest(QString file, QString newFileTime);

    // 新建文件夹
    void newfolderRequest(QString folder);

    // 执行命令
    void cmdRequest(QString app, QString param);

    // 下载文件
    void downloadRequest(QString reFile, QString loFile, int idx = 0);

    // 上传文件
    void uploadRequest(QString reFile, QString loFile, QString mode, int idx, int tol, QByteArray data);

    // 执行sql语句
    void sqlExecRequest(QString dbType, QString info, QString dbCode, QString dbHost, QString dbUser, QString dbPasswd, QString dbName, QString sql);

    // 导出数据表
    void exportTbRequest(QString dbType, QString dbCode, QString dbHost, QString dbUser, QString dbPasswd, QString dbName, QString tbName);

    // 运行自定义脚本
    void scriptExecRequest(QString data);

signals:

    // 请求响应信号
    // @param type 返回的数据类型，参看HttpCore基类中的RET_TYPE类型
    // @param url weishell 网址
    // @param data 返回的数据
    void sigResponse(int type, QString url, QVariant data);

    // 错误信号
    void sigError(ERR_CODE error, QString url, QString msg);

private:

    // 对称加密函数
    QByteArray encrypt(QByteArray data, QString passwd, QString iv);

    // 对称解密函数
    QByteArray decrypt(QByteArray data, QString passwd, QString iv);

    // 将参数带入payload
    QString formatPayload(QString payload, QMap<QString, QString> params, bool needRep = false);

    // 响应信号槽
    void HttpResponse(int code, QString url, QByteArray data) override;

private:

    /*
    QString url;

    QString passwd;

    QString cookies;

    QString code;

    QString script;

    QString iv;*/

    MdHole mdHole;
};

#endif // HTTPCORE_H
