#include "dbconfigdialog.h"
#include "ui_dbconfigdialog.h"

DBConfigDialog::DBConfigDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DBConfigDialog)
{
    ui->setupUi(this);

    //固定窗口大小
    setFixedSize(this->width(), this->height());
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    //设置UI
    ui->comboBox_dbtype->addItem("请选择");
    ui->comboBox_dbtype->addItem("MYSQL");
    ui->comboBox_dbtype->addItem("MSSQL");
    ui->comboBox_dbtype->addItem("ORACLE");
    ui->comboBox_dbtype->addItem("POSTGRESQL");
    ui->comboBox_codetype->addItem("请选择");
    ui->comboBox_codetype->addItem("UTF8");
    ui->comboBox_codetype->addItem("GBK");
    ui->comboBox_codetype->addItem("BIG5");
    ui->comboBox_codetype->addItem("ISO-8859-1");
}

DBConfigDialog::~DBConfigDialog()
{
    delete ui;
}

void DBConfigDialog::setDbConfig(MdHole mdHole)
{
    this->mdHole = mdHole;

    ui->comboBox_dbtype->setCurrentText(mdHole.getDbType());
    ui->comboBox_codetype->setCurrentText(mdHole.getDbCode());
    ui->lineEdit_host->setText(mdHole.getDbHost());
    ui->lineEdit_user->setText(mdHole.getDbUser());
    ui->lineEdit_passwd->setText(mdHole.getDbPasswd());
}

// ********************* 以下是UI槽函数 ***********************
void DBConfigDialog::on_pushButton_submit_clicked()
{
    isOk = true;
    mdHole.setDbType(ui->comboBox_dbtype->currentText());
    mdHole.setDbCode(ui->comboBox_codetype->currentText());
    mdHole.setDbHost(ui->lineEdit_host->text());
    mdHole.setDbUser(ui->lineEdit_user->text());
    mdHole.setDbPasswd(ui->lineEdit_passwd->text());
    close();
}

void DBConfigDialog::on_pushButton_cancel_clicked()
{
    isOk = false;
    close();
}
