#include "global.h"
#include "httpheaderdialog.h"
#include "ui_httpheaderdialog.h"

HttpHeaderDialog::HttpHeaderDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HttpHeaderDialog)
{
    ui->setupUi(this);

    //UI样式设置
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    ui->tableWidget->setColumnCount(2);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "名称" << "值");
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);              //设置不可编辑
    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);                     //开启右键菜单
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);             //设置整行选中
    ui->tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);            //单行选择
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);                                     //关闭选中虚线框
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);   //设置第一列自动宽度
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);   //设置第二列自动宽度

    // 加载HTTP请求头信息
    mdHeaderList = sqlModel.selectHeader();
    foreach(MdHeader mdHeader, mdHeaderList)
    {
        insertListItem(mdHeader);
    }
}

HttpHeaderDialog::~HttpHeaderDialog()
{
    delete ui;
}

// ******************** 以下为UI私有函数 *****************
void HttpHeaderDialog::insertListItem(MdHeader mdHeader)
{
    int curRow = ui->tableWidget->rowCount();
    ui->tableWidget->insertRow(curRow);

    ui->tableWidget->setItem(curRow,0,new QTableWidgetItem(mdHeader.getName()));
    ui->tableWidget->setItem(curRow,1,new QTableWidgetItem(mdHeader.getVal()));

    // 用每一行的第一列data存储该hole的唯一索引id
    ui->tableWidget->item(curRow, 0)->setData(Qt::UserRole, mdHeader.getId());
}

void HttpHeaderDialog::updateListItem(MdHeader mdHeader)
{
    for (int i = 0; i < ui->tableWidget->rowCount(); i++)
    {
        if(ui->tableWidget->item(i, 0)->data(Qt::UserRole).toInt() == mdHeader.getId())
        {
            ui->tableWidget->item(i, 0)->setText(mdHeader.getName());
            ui->tableWidget->item(i, 1)->setText(mdHeader.getVal());
            break;
        }
    }
}

//刷新Webshell列表信号槽
void HttpHeaderDialog::flashList()
{
    ui->tableWidget->clearContents();
    ui->tableWidget->setRowCount(0);
    mdHeaderList = sqlModel.selectHeader();
    foreach(MdHeader mdHeader, mdHeaderList)
    {
        insertListItem(mdHeader);
    }
}


// ******************** 以下为UI槽函数 *****************
void HttpHeaderDialog::on_tableWidget_customContextMenuRequested(const QPoint &pos)
{
    QMenu menu(this);
    QAction * addAction = menu.addAction(QIcon(":/res/image/add.png"), "新增");
    QAction * editAction = menu.addAction(QIcon(":/res/image/edit.png"), "修改");
    QAction * deleteAction = menu.addAction(QIcon(":/res/image/delete.png"), "删除");
    connect(addAction, &QAction::triggered, [=](){
        QString data = QInputDialog::getMultiLineText(this, "新增HTTP头", "格式如下 键名:键值",  "");
        QStringList key = data.split(":");
        if(key.length() == 2)
        {
            MdHeader mdHeader;
            mdHeader.setName(key.at(0));
            mdHeader.setVal(key.at(1));
            if(sqlModel.insertHeader(mdHeader)){
                // 新增之后要刷新整个列表，因为新增的项目没有获取到id
                flashList();
            }
        }
    });
    connect(deleteAction, &QAction::triggered, [=](){
        if(sqlModel.deleteHeader(ui->tableWidget->item(ui->tableWidget->currentRow(), 0)->data(Qt::UserRole).toInt()))
            ui->tableWidget->removeRow(ui->tableWidget->currentRow());
    });
    connect(editAction, &QAction::triggered, [=](){
        MdHeader mdHeader = sqlModel.findHeader(ui->tableWidget->item(ui->tableWidget->currentRow(), 0)->data(Qt::UserRole).toInt());
        qDebug() <<mdHeader.getId() << mdHeader.getName() << mdHeader.getVal();
        QString data = QInputDialog::getMultiLineText(this, "新增HTTP头", "格式如下 键名:键值",  QString("%1:%2").arg(mdHeader.getName()).arg(mdHeader.getVal()));
        QStringList key = data.split(":");
        if(key.length() == 2)
        {
            mdHeader.setName(key.at(0));
            mdHeader.setVal(key.at(1));
            if(sqlModel.updateHeader(mdHeader))
                updateListItem(mdHeader);
        }

    });

//    qDebug() <<ui->tableWidget->itemAt(ui->tableWidget->viewport()->mapFromGlobal(QCursor::pos()));
    if(ui->tableWidget->itemAt(ui->tableWidget->viewport()->mapFromGlobal(QCursor::pos())) == nullptr){
        deleteAction->setVisible(false);
        editAction->setVisible(false);
    }

    menu.exec(QCursor::pos());
}
