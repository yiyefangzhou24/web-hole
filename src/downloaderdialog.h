#ifndef DOWNLOADERDIALOG_H
#define DOWNLOADERDIALOG_H

#include <QDialog>
#include <QMessageBox>
#include <QUrl>
#include <QDebug>

namespace Ui {
class DownloaderDialog;
}

class DownloaderDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DownloaderDialog(QWidget *parent = nullptr);
    ~DownloaderDialog();

    QString url;

    QString filename;

private slots:
    void on_pushButton_submit_clicked();

    void on_pushButton_cancel_clicked();

    void on_lineEdit_url_textChanged(const QString &arg1);

private:
    Ui::DownloaderDialog *ui;
};

#endif // DOWNLOADERDIALOG_H
