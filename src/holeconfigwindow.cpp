#include "global.h"
#include "holeconfigwindow.h"
#include "ui_holeconfigwindow.h"

HoleConfigWindow::HoleConfigWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::HoleConfigWindow)
{
    ui->setupUi(this);

    //设置UI显示
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    ui->codeBox->addItem("操作系统编码[必选]");
    ui->codeBox->addItem("UTF-8");
    ui->codeBox->addItem("GB2312");
    ui->codeBox->addItem("BIG5");
    ui->codeBox->addItem("ISO-8859-1");

    ui->scriptBox->addItem("脚本类型[必选]");
    ui->scriptBox->addItem("ASP");
    ui->scriptBox->addItem(".NET");
    ui->scriptBox->addItem("PHP");
    ui->scriptBox->addItem("JSP");
    ui->scriptBox->addItem("CFM");
}

HoleConfigWindow::~HoleConfigWindow()
{
    delete ui;
}

void HoleConfigWindow::on_submitButton_clicked()
{
    if(ui->urlEdit->text().isEmpty() || ui->passwdEdit->text().isEmpty() || ui->ivEdit->text().isEmpty())
    {
        QMessageBox::information(this, "提示", "请正确填写URL、密码、加密特征码");
        return;
    }
    if(ui->codeBox->currentText() == "操作系统编码[必选]")
    {
        QMessageBox::information(this, "提示", "请选择被控端操作系统编码");
        return;
    }
    if(ui->scriptBox->currentText() == "脚本类型[必选]")
    {
        QMessageBox::information(this, "提示", "请选择被控端操作脚本类型");
        return;
    }
    mdHole.setUrl(ui->urlEdit->text());
    mdHole.setPasswd(ui->passwdEdit->text());
    mdHole.setCookies(ui->cookiesEdit->toPlainText());
    mdHole.setInfo(ui->infoEdit->toPlainText());
    mdHole.setIv(ui->ivEdit->text());
    mdHole.setCode(ui->codeBox->currentText());
    mdHole.setScript(ui->scriptBox->currentText());
    if(windowTitle() == "添加")
        mdHole.setStatus("Unknown");
    mdHole.setTime(QDateTime::currentDateTime().toString("yyyy/MM/dd hh:mm"));

    bool res = windowTitle() == "添加" ?  sqlModel.insertHole(mdHole) : sqlModel.updateHole(mdHole);

    if(res){
        close();
    }else
    {
        QMessageBox::critical(this, "错误", "数据库读写出错，请检查数据库文件是否正确");
    }
}

void HoleConfigWindow::setHoleData(MdHole mdHole)
{
    this->mdHole = mdHole;
    ui->urlEdit->setText(mdHole.getUrl());
    ui->passwdEdit->setText(mdHole.getPasswd());
    ui->cookiesEdit->setPlainText(mdHole.getCookies());
    ui->infoEdit->setPlainText(mdHole.getInfo());
    ui->ivEdit->setText(mdHole.getIv());
    ui->codeBox->setCurrentText(mdHole.getCode());
    ui->scriptBox->setCurrentText(mdHole.getScript());
}

void HoleConfigWindow::on_ivButton_clicked()
{
    // 随机生成32位字符串
    QString iv;
    qsrand(QDateTime::currentMSecsSinceEpoch());//随机数种子
    const char array_str[] = "abcdefghijklmnopqrstuvwxyz0123456789";
    int array_size = sizeof(array_str);
    int idx = 0;
    for (int i = 0; i < 32; ++i)
    {
        idx = qrand() % (array_size - 1);
        QChar ch = array_str[idx];
        iv.append(ch);
    }
    ui->ivEdit->setText(iv);
}

// ********************** 以下为UI槽函数 **********************
void HoleConfigWindow::on_urlEdit_textChanged(const QString &arg1)
{
    int fg = arg1.lastIndexOf('.');
    if(fg > 0)
    {
        QString type = arg1.right(arg1.length() - fg - 1);
        if(!type.compare("php", Qt::CaseInsensitive))
        {
            ui->scriptBox->setCurrentText("PHP");
        }else if(!type.compare("asp", Qt::CaseInsensitive))
        {
            ui->scriptBox->setCurrentText("ASP");
        }else if(!type.compare("aspx", Qt::CaseInsensitive))
        {
            ui->scriptBox->setCurrentText(".NET");
        }else if(!type.compare("jsp", Qt::CaseInsensitive))
        {
            ui->scriptBox->setCurrentText("JSP");
        }else if(!type.compare("cfm", Qt::CaseInsensitive))
        {
            ui->scriptBox->setCurrentText("CFM");
        }
    }
}
