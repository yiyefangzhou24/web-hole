#include "mdproxy.h"

QString MdProxy::getHost()
{
    return host;
}

void MdProxy::setHost(QString host)
{
    this->host = host;
}

int MdProxy::getPort()
{
    return port;
}

void MdProxy::setPort(int port)
{
    this->port = port;
}

QString MdProxy::getProtocol()
{
    return protocol;
}

void MdProxy::setProtocol(QString protocol)
{
    this->protocol = protocol;
}

QString MdProxy::getUser()
{
    return user;
}

void MdProxy::setUser(QString user)
{
    this->user = user;
}

QString MdProxy::getPass()
{
    return pass;
}

void MdProxy::setPass(QString pass)
{
    this->pass = pass;
}
