#ifndef NETCORE_H
#define NETCORE_H

// **
//
// 网络请求核心类
//    用来发起和接收HTTP/HTTPS通讯数据
//    注意：因请求和返回数据均为加密后转为十六进制字符串，所以统一使用UTF8编码即可
//
// created by yiyefangzhou24
// created time 2024/2/9
//
// **

#include <QObject>
#include <QtNetwork>
#include <QNetworkAccessManager>
#include <QDebug>
#include <QNetworkProxy>
#include "mdheader.h"
#include "mdproxy.h"

class NetCore : public QObject
{
    Q_OBJECT

public:
    NetCore();
    ~NetCore();

    // 获取请求头
    QList<MdHeader> getHeaders();

    // 设置请求头
    void setHeaders(QList<MdHeader> headers);

    // 获取代理服务器
    MdProxy getProxy();

    // 设置代理服务器
    void setProxy(MdProxy mdProxy);

    // GET请求
    void HttpGetRequest(QString url, QString cookies = "");

    // POST请求
    void HttpPostRequest(QString url, QString data, QString cookies = "");

    // 请求响应
    virtual void HttpResponse(int, QString , QByteArray){};

private slots:

    // 网络请求响应槽函数
    void replyFinished(QNetworkReply *reply);
private:

    // 内部函数 - 将cookies字符串解析为QNetworkCookie数组
    QList<QNetworkCookie> parseCookies(QString cookies);

private:

    QNetworkAccessManager *manager;

    // HTTP请求头（系统配置）
    QList<MdHeader> headers;

    // Proxy代理配置
    MdProxy mdProxy;
};

#endif // NETCORE_H
