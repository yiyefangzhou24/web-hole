QT       += core gui network sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    cmdshellwindow.cpp \
    databasewindow.cpp \
    dbconfigdialog.cpp \
    downloaderdialog.cpp \
    filemanagewindow.cpp \
    global.cpp \
    holeconfigwindow.cpp \
    httpcore.cpp \
    httpheaderdialog.cpp \
    main.cpp \
    mainwindow.cpp \
    mdfileinfo.cpp \
    mdheader.cpp \
    mdhole.cpp \
    mdpayload.cpp \
    mdproxy.cpp \
    netcore.cpp \
    payloaddialog.cpp \
    payloadeditdialog.cpp \
    proxydialog.cpp \
    qaes.cpp \
    qlog.cpp \
    scriptexecwindow.cpp \
    sqlmodel.cpp \
    testallholedialog.cpp \
    texteditdialog.cpp \
    uploaddialog.cpp

HEADERS += \
    cmdshellwindow.h \
    databasewindow.h \
    dbconfigdialog.h \
    downloaderdialog.h \
    filemanagewindow.h \
    global.h \
    holeconfigwindow.h \
    httpcore.h \
    httpheaderdialog.h \
    mainwindow.h \
    mdfileinfo.h \
    mdheader.h \
    mdhole.h \
    mdpayload.h \
    mdproxy.h \
    netcore.h \
    payloaddialog.h \
    payloadeditdialog.h \
    proxydialog.h \
    qaes.h \
    qlog.h \
    scriptexecwindow.h \
    sqlmodel.h \
    testallholedialog.h \
    texteditdialog.h \
    uploaddialog.h

FORMS += \
    cmdshellwindow.ui \
    databasewindow.ui \
    dbconfigdialog.ui \
    downloaderdialog.ui \
    filemanagewindow.ui \
    holeconfigwindow.ui \
    httpheaderdialog.ui \
    mainwindow.ui \
    payloaddialog.ui \
    payloadeditdialog.ui \
    proxydialog.ui \
    scriptexecwindow.ui \
    testallholedialog.ui \
    texteditdialog.ui \
    uploaddialog.ui

LIBS += \
    $$PWD/openssl/lib/libcrypto.lib \
    $$PWD/openssl/lib/libssl.lib

INCLUDEPATH += \
    $$PWD/openssl/include

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    src.qrc

DISTFILES += \
    res/image/header.png

#程序版本
VERSION = 1.3.1.1
#程序图标
RC_ICONS = main.ico
#公司名称
QMAKE_TARGET_COMPANY ="yiyefangzhou24"
#程序说明
QMAKE_TARGET_DESCRIPTION = "WebHole"
#版权信息
QMAKE_TARGET_COPYRIGHT = "WebHole Copyright(C) 2024"
#程序名称
QMAKE_TARGET_PRODUCT = "WebHole"
#程序语言
#0x0800代表和系统当前语言一致
RC_LANG = 0x0800
