#include "mdpayload.h"

int MdPayload::getId()
{
    return id;
}

void MdPayload::setId(int id)
{
    this->id = id;
}

QString MdPayload::getScript()
{
    return script;
}

void MdPayload::setScript(QString script)
{
    this->script = script;
}

QString MdPayload::getName()
{
    return name;
}

void MdPayload::setName(QString name)
{
    this->name = name;
}

QString MdPayload::getPayload()
{
    return payload;
}

void MdPayload::setPayload(QString payload)
{
    this->payload = payload;
}

void MdPayload::setNeedRep(QString needRep)
{
    this->needRep = needRep;
}

QString MdPayload::getNeedRep()
{
    return needRep;
}
