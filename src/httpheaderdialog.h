#ifndef HTTPHEADERDIALOG_H
#define HTTPHEADERDIALOG_H

#include <QDialog>
#include <QMenu>
#include <QList>
#include <QInputDialog>
#include <QStatusBar>
#include "mdheader.h"

namespace Ui {
class HttpHeaderDialog;
}

class HttpHeaderDialog : public QDialog
{
    Q_OBJECT

public:
    explicit HttpHeaderDialog( QWidget *parent = nullptr);
    ~HttpHeaderDialog();

private slots:

    void on_tableWidget_customContextMenuRequested(const QPoint &pos);

private:

    // 添加list item
    void insertListItem(MdHeader mdHeader);

    // 修改list item
    void updateListItem(MdHeader mdHeader);

    // 刷新列表
    void flashList();

private:
    Ui::HttpHeaderDialog *ui;

    QList<MdHeader> mdHeaderList;
};

#endif // HTTPHEADERDIALOG_H
