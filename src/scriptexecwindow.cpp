#include "scriptexecwindow.h"
#include "ui_scriptexecwindow.h"

ScriptExecWindow::ScriptExecWindow(MdHole mdHole, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ScriptExecWindow)
{
    ui->setupUi(this);

    this->mdHole = mdHole;

    // 设置窗体最大化和最小化
    Qt::WindowFlags windowFlag  = Qt::Dialog;
    windowFlag |= Qt::WindowMinimizeButtonHint;
    windowFlag |= Qt::WindowMaximizeButtonHint;
    windowFlag |= Qt::WindowCloseButtonHint;
    setWindowFlags(windowFlag);

    // 绑定响应信号
    connect(&http, &HttpCore::sigResponse, this, &ScriptExecWindow::on_response);

    // 初始化网络请求参数请求服务器信息
    http.setHole(mdHole);
}

ScriptExecWindow::~ScriptExecWindow()
{
    delete ui;
}

// ******************** 以下为UI槽函数 ********************
void ScriptExecWindow::on_pushButton_exec_clicked()
{
    if(ui->plainTextEdit_script->toPlainText().length() > 0)
    {
        http.scriptExecRequest(ui->plainTextEdit_script->toPlainText());
    }
}

void ScriptExecWindow::closeEvent(QCloseEvent *e)
{
    // 这里是个坑，必须手动disconnect信号关联
    // 因为qt内存new的对话框在短期内没有释放，再次new还是之前的对话框数据，会产生2次信号重复绑定
    disconnect(&http, &HttpCore::sigResponse, this, &ScriptExecWindow::on_response);
}

// ********************** 以下为网络通信槽函数 *****************************
void ScriptExecWindow::on_response(int type, QString url, QVariant data)
{
    if(url == mdHole.getUrl())
    {
        if(type == RET_SCRIPT)        // |-- 运行自定义脚本
        {
            ui->plainTextEdit_res->setPlainText(data.toString());
        }
    }
}
