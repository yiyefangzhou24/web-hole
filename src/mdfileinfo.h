#ifndef MDFILEINFO_H
#define MDFILEINFO_H
// **
//
// 文件/文件夹信息 Model类
//    仅用来存放文件/文件夹详细信息
//
// created by yiyefangzhou24
// created time 2024/2/9
//
// **

#include <QString>
#include <QMetaType>
#include <QFileInfo>
#include <QFileIconProvider>

enum TYPE
{
    MY_FILE,
    MY_FOLDER
};

class MdFileInfo
{

public:

    TYPE getType();

    void setType(TYPE type);

    QString getPath();

    void setPath(QString path);

    QString getName();

    void setName(QString name);

    QString getTime();

    void setTime(QString time);

    QString getSize();

    void setSize(QString size);

    /**
     * @brief getFullName 获取完整文件路径和文件名，自动区分win和linux系统路径区别
     * @return 完整文件路径
     */
    QString getFullName();

    /**
     * @brief getIcon 获取文件对应的系统图标
     * @return 图标句柄
     */
    QIcon getIcon();
private:

    TYPE type;

    QString path;

    QString name;

    QString time;

    QString size;

//    QIcon icon;
};


// 添加该类到QVariant类型
Q_DECLARE_METATYPE(MdFileInfo)


#endif // MDFILEINFO_H
