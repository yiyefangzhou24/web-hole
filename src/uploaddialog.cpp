#include "uploaddialog.h"
#include "ui_uploaddialog.h"

UploadDialog::UploadDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UploadDialog)
{
    ui->setupUi(this);

    //固定窗口大小
    setFixedSize(this->width(), this->height());
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    ui->comboBox->addItem("256KB");
    ui->comboBox->addItem("512KB");
    ui->comboBox->addItem("1MB");
    ui->comboBox->addItem("2MB");
    ui->comboBox->setCurrentText("512KB");
}

UploadDialog::~UploadDialog()
{
    delete ui;
}

void UploadDialog::on_pushButton_ok_clicked()
{
    if(ui->lineEdit_file->text() == "")
    {
        QMessageBox::critical(this, "提示", "请选择要上传的文件");
        return;
    }
    if(ui->lineEdit_filename->text() == "")
    {
        QMessageBox::critical(this, "提示", "请输入文件名");
        return;
    }
    if(ui->comboBox->currentText() == "请选择分段大小")
    {
        QMessageBox::critical(this, "提示", "请正确选择分段大小");
        return;
    }
    filePath = ui->lineEdit_file->text();
    fileName = ui->lineEdit_filename->text();
    // 因为文件数据经过十六进制编码后长度增加一倍，所以这里应该为选择长度的一半
    if(ui->comboBox->currentText() == "256KB")
    {
        blockSize = 128 * 1024;
    }else if(ui->comboBox->currentText() == "512KB")
    {
        blockSize = 256 * 1024;
    }else if(ui->comboBox->currentText() == "1MB")
    {
        blockSize = 512 * 1024;
    }else if(ui->comboBox->currentText() == "2MB")
    {
        blockSize = 1024 * 1024;
    }
    selected = true;
    close();
}

void UploadDialog::on_pushButton_file_clicked()
{
    QString saveFile = QFileDialog::getOpenFileName(this, "选择上传文件", "./", "All file(*.*)");
    if(saveFile.length() > 0)
    {
        ui->lineEdit_file->setText(saveFile);
        QFileInfo info(saveFile);
        ui->lineEdit_filename->setText(info.fileName());
    }
}

void UploadDialog::on_pushButton_cancel_clicked()
{
    selected = false;
    close();
}
