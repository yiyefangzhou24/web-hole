#ifndef SCRIPTEXECWINDOW_H
#define SCRIPTEXECWINDOW_H

#include <QDialog>
#include "mdhole.h"
#include "global.h"

namespace Ui {
class ScriptExecWindow;
}

class ScriptExecWindow : public QDialog
{
    Q_OBJECT

public:
    explicit ScriptExecWindow(MdHole mdHole, QWidget *parent = nullptr);
    ~ScriptExecWindow();

    void closeEvent(QCloseEvent *e) override;

private slots:
    // 执行按钮点击信号槽
    void on_pushButton_exec_clicked();

    // 网络响应信号槽
    void on_response(int type, QString url, QVariant data);

private:
    Ui::ScriptExecWindow *ui;

    MdHole mdHole;
};

#endif // SCRIPTEXECWINDOW_H
