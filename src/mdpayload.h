#ifndef MDPAYLOAD_H
#define MDPAYLOAD_H

// **
//
// Payload Model类
//    仅用来存放攻击载荷数据集
//
// created by yiyefangzhou24
// created time 2024/4/7
//
// **

#include <QString>

class MdPayload
{
public:
    int getId();

    void setId(int id);

    QString getScript();

    void setScript(QString script);

    QString getName();

    void setName(QString name);

    QString getPayload();

    void setPayload(QString payload);

    void setNeedRep(QString needRep);

    QString getNeedRep();

private:
    int id;             // id

    QString script;     // 脚本类型

    QString name;       // 载荷名称

    QString payload;    // 载荷内容

    QString needRep;    // 是否需要特殊符号转义("true","false")
};

#endif // MDPAYLOAD_H
