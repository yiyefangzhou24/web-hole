#include "payloadeditdialog.h"
#include "ui_payloadeditdialog.h"

PayloadEditDialog::PayloadEditDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PayloadEditDialog)
{
    ui->setupUi(this);

    mdPayload.setId(0);
    payloadNames = {{"TEST", "可用性测试[TEST]"},
                    {"INFO", "获取系统信息[INFO]"},
                    {"FILE_LIST", "列目录[FILE_LIST]"},
                    {"FILE_DEL", "删除文件[FILE_DEL]"},
                    {"FILE_DOWNLOADER", "下载文件到服务器[FILE_DOWNLOADER]"},
                    {"FILE_EXEC", "远程执行[FILE_EXEC]"},
                    {"FILE_RENAME", "重命名[FILE_RENAME]"},
                    {"FILE_RETIME", "修改文件时间[FILE_RETIME]"},
                    {"FILE_NEWFOLDER", "新建文件夹[FILE_NEWFOLDER]"},
                    {"FILE_DOWN", "下载文件[FILE_DOWN]"},
                    {"FILE_UP", "上传文件[FILE_UP]"},
                    {"CMD_EXEC", "执行系统命令[CMD_EXEC]"},
                    {"DB_EXEC", "执行SQL语句[DB_EXEC]"},
                    {"DB_EXPORTTB", "备份数据表[DB_EXPORTTB]"},
                    {"CUSTOM_SCRIPT", "运行自定义脚本[CUSTOM_SCRIPT]"}};
    payloadInfos = {{"TEST", "[功能] 测试被控端功能是否正常\n\n[参数] 无"},
                    {"INFO", "[功能] 获取被控端系统信息\n\n[参数] 无"},
                    {"FILE_LIST", "[功能] 列被控端指定路径下所有文件和文件夹\n\n[参数] {$code} - 被控端操作系统编码\n[参数] {$path} - 目标路径"},
                    {"FILE_DEL", "[功能] 删除指定的文件或文件夹\n\n[参数] {$code} - 被控端操作系统编码\n[参数] {$path} - 目标路径"},
                    {"FILE_DOWNLOADER", "[功能] 下载文件到被控端服务器\n\n[参数] {$code} - 被控端操作系统编码\n[参数] {$file} - 被控端保存文件的绝对路径（包括文件名）\n[参数] {$url} - 要下载文件的URL"},
                    {"FILE_EXEC", "[功能] 远程运行程序\n\n[参数] {$code} - 被控端操作系统编码\n[参数] {$app} - 程序绝对路径及参数"},
                    {"FILE_RENAME", "[功能] 重命名文件或文件夹\n\n[参数] {$code} - 被控端操作系统编码\n[参数] {$pwd} - 文件所在目录\n[参数] {$oldname} - 旧文件/文件夹名\n[参数] {$newname} -  新文件/文件夹名"},
                    {"FILE_RETIME", "[功能] 修改文件时间\n\n[参数] {$code} - 被控端操作系统编码\n[参数] {$file} - 文件绝对路径（包括文件名）\n[参数] {$time} - 新的文件时间"},
                    {"FILE_NEWFOLDER", "[功能] 新建文件夹\n\n[参数] {$code} - 被控端操作系统编码\n[参数] {$folder} - 新建文件夹的绝对路径"},
                    {"FILE_DOWN", "[功能] 下载远程文件到本地\n\n[参数] {$code} - 被控端操作系统编码\n[参数] {$refile} - 远程文件绝对路径（包含文件名）\n[参数] {$lofile} - 下载到本地的绝对路径（包含文件名）\n[参数] {$idx} - 分块下载的块序号"},
                    {"FILE_UP", "[功能] 上传文件到远程服务器\n\n[参数] {$code} - 被控端操作系统编码\n[参数] {$refile} - 远程文件绝对路径（包含文件名）\n[参数] {$lofile} - 下载到本地的绝对路径（包含文件名）\n[参数] {$idx} - 分块下载的块序号\n[参数] {$tol} - 文件分块总数\n[参数] {$mode} - 写入方式，参考fopen的第二个参数\n[参数] {$data} - 十六进制编码的文件数据"},
                    {"CMD_EXEC", "[功能] 执行系统命令\n\n[参数] {$code} - 被控端操作系统编码\n[参数] {$app} - 执行程序绝对路径\n[参数] {$cmd} - 命令内容"},
                    {"DB_EXEC", "[功能] 执行SQL语句\n\n[参数] {$code} - 该参数预留，暂未启用\n[参数] {$info} - 自定义标识符，服务端原样返回\n[参数] {$type} - 数据库类型\n[参数] {$host} - 数据库服务器\n[参数] {$user} - 数据库用户名\n[参数] {$passwd} -数据库密码\n[参数] {$dbname} - 当前数据库\n[参数] {$sql} - SQL语句"},
                    {"DB_EXPORTTB", "[功能] 备份数据表\n\n[参数] {$type} - 数据库类型\n[参数] {$host} - 数据库服务器\n[参数] {$user} - 数据库用户名\n[参数] {$passwd} -数据库密码\n[参数] {$dbname} - 当前数据库\n[参数] {$tbname} - 要备份的数据表"},
                    {"CUSTOM_SCRIPT", "[功能] 运行自定义脚本\n\n[参数] {$script} - 脚本内容"}};

    //UI样式设置
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    ui->comboBox_script->addItem("请选择脚本编码");
    ui->comboBox_script->addItem("ASP");
    ui->comboBox_script->addItem(".NET");
    ui->comboBox_script->addItem("PHP");
    ui->comboBox_script->addItem("JSP");
    ui->comboBox_script->addItem("CFM");
    ui->comboBox_name->addItem("请选择载荷类型");

    ui->radioButton_no->setChecked(true);

    QMap<QString, QString>::iterator it;
    for (it = payloadNames.begin(); it != payloadNames.end(); ++it) {
        ui->comboBox_name->addItem(it.value(), it.key());
    }

    // 以下是测试代码
}

PayloadEditDialog::~PayloadEditDialog()
{
    delete ui;
}

void PayloadEditDialog::setPayload(MdPayload mdPayload)
{
    this->mdPayload = mdPayload;

    // 更新UI
    ui->comboBox_script->setCurrentText(mdPayload.getScript());
    ui->comboBox_name->setCurrentText(payloadNames.value(mdPayload.getName()));
    ui->plainTextEdit->setPlainText(mdPayload.getPayload());
    if(mdPayload.getNeedRep() == "true")
        ui->radioButton_yes->setChecked(true);
    else
        ui->radioButton_no->setChecked(true);
}


// ******************** 以下为UI槽函数 *****************
void PayloadEditDialog::on_pushButton_submit_clicked()
{
    mdPayload.setName(ui->comboBox_name->currentData().toString());
    mdPayload.setScript(ui->comboBox_script->currentText());
    mdPayload.setPayload(ui->plainTextEdit->toPlainText());
    ui->radioButton_yes->isChecked() ? mdPayload.setNeedRep("true") : mdPayload.setNeedRep("false");
    if(mdPayload.getName().isEmpty() || mdPayload.getScript().isEmpty() || mdPayload.getPayload().isEmpty())
    {
        QMessageBox::critical(this, "提示", "请正确填写攻击载荷信息");
        return;
    }
    if(mdPayload.getId() > 0)       // 修改载荷
    {
        sqlModel.updatePayload(mdPayload);
    }else                           // 新增载荷
    {
        sqlModel.insertPayload(mdPayload);
    }
    close();
}

void PayloadEditDialog::on_pushButton_cancel_clicked()
{
    close();
}

void PayloadEditDialog::on_comboBox_name_currentIndexChanged(int index)
{
    ui->plainTextEdit_info->setPlainText(payloadInfos.value(ui->comboBox_name->currentData().toString()));
}
