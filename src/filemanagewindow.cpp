#include "filemanagewindow.h"
#include "ui_filemanagewindow.h"
#include "uploaddialog.h"
#include "texteditdialog.h"
#include "downloaderdialog.h"

FileManageWindow::FileManageWindow(MdHole mdHole, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::FileManageWindow)
{
    ui->setupUi(this);

    this->mdHole = mdHole;

    // 设置窗体最大化和最小化
    Qt::WindowFlags windowFlag  = Qt::Dialog;
    windowFlag |= Qt::WindowMinimizeButtonHint;
    windowFlag |= Qt::WindowMaximizeButtonHint;
    windowFlag |= Qt::WindowCloseButtonHint;
    setWindowFlags(windowFlag);

//    // 底部状态栏
//    statusBar = new QStatusBar(this);
//    ui->mainLayout->addWidget(statusBar);
    //隐藏进度条
    ui->progressBar->setVisible(false);

    //设置UI样式
    ui->tableWidget->setColumnCount(4);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "" << "文件名" << "时间" << "大小");
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);              //设置不可编辑
    ui->tableWidget->setSortingEnabled(true);                                         //开启点击表头排序
    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);                     //开启右键菜单
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);             //设置整行选中
    ui->tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);            //单行选择
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);                                     //关闭选中虚线框
    ui->tableWidget->horizontalHeader()->setFixedHeight(20);
    ui->tableWidget->setColumnWidth(0 , 16);                                              //设置第一列宽度为20
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);   //设置第二列自动宽度
    ui->tableWidget->setColumnWidth(2 , 160);                                             //设置第三列宽度为160
    ui->tableWidget->setColumnWidth(3 , 120);                                             //设置第四列宽度为120

    ui->treeWidget->setStyle((QStyleFactory::create("windows")));
    ui->treeWidget->header()->setSectionResizeMode(QHeaderView::ResizeMode::ResizeToContents);
    ui->treeWidget->header()->setStretchLastSection(false);
    ui->treeWidget->setHeaderLabels(QStringList{""});
    ui->treeWidget->header()->setFixedHeight(20);

    // 绑定响应信号
    connect(&http, &HttpCore::sigResponse, this, &FileManageWindow::on_response);

    // 初始化网络请求参数请求服务器信息
    http.setHole(mdHole);
    http.getInfoRequest();

    // 以下是测试代码
//    QTreeWidgetItem * root = addTreeRootItem("/");
//    addTreeChildItem("111", addTreeChildItem("test", root));
//    MdFileInfo file;
//    file.setName("gest");
//    file.setType(MY_FILE);
//    insertTableItem(file);
}

FileManageWindow::~FileManageWindow()
{
    delete ui;
}

// ********************** 以下为UI槽函数 *****************************
void FileManageWindow::on_goButton_clicked()
{
    QString path = getPwdUrl();
    if(path.length() > 0)
    {
        // 清空列表文件信息和树形目相关文件信息
        ui->tableWidget->clearContents();
        ui->tableWidget->setRowCount(0);
        // 发送请求
        http.getFileListRequest(path);
    }
}

void FileManageWindow::on_treeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
    QTreeWidgetItem * item = current;
    // 获取完整路径
    QStringList names;
    QString path;
    while (item) {
        names.prepend(item->text(0));
        item = item->parent();
    }
    if(os == WIN)
    {
        path = names.join("\\");
    }else
    {
        path = names.join("/");
        path.remove(1, 1);
    }
    // 修改地址栏
    setPwdUrl(path);
    // 清空列表文件信息和树形目相关文件信息
    ui->tableWidget->clearContents();
    ui->tableWidget->setRowCount(0);
    // 发送列文件请求
    http.getFileListRequest(path);
}

void FileManageWindow::on_tableWidget_itemDoubleClicked(QTableWidgetItem *item)
{
    if(ui->tableWidget->item(item->row(), 0)->data(Qt::UserRole) == "folder")
    {
        QString path = getPwdUrl() + (os == WIN ? "\\" : "/") + ui->tableWidget->item(item->row(), 1)->text();
        // 更新地址栏
        setPwdUrl(path);
        // 清空列表文件信息和树形目相关文件信息
        ui->tableWidget->clearContents();
        ui->tableWidget->setRowCount(0);
        // 发送请求
        http.getFileListRequest(path);
    }
}

void FileManageWindow::on_tableWidget_customContextMenuRequested(const QPoint &pos)
{
    QMenu menu(this);
    QAction * execAction = menu.addAction(QIcon(":/res/image/exec.png"), "远程运行");
    menu.addSeparator();
    QAction * downAction = menu.addAction(QIcon(":/res/image/down.png"), "下载文件");
    QAction * uploadAction = menu.addAction(QIcon(":/res/image/upload.png"), "上传文件");
    QAction * downloderAction = menu.addAction(QIcon(":/res/image/downloader.png"), "下载文件到服务器");
    menu.addSeparator();
    QAction * flashAction = menu.addAction(QIcon(":/res/image/flash.png"), "刷新");
    menu.addSeparator();
    QMenu newMenu("新建");
    newMenu.setIcon(QIcon(":/res/image/new.png"));
    QAction * newFileAction = newMenu.addAction(QIcon(":/res/image/file.png"), "文本文件");
    QAction * newFolderAction = newMenu.addAction(QIcon(":/res/image/folder.png"), "文件夹");
    menu.addMenu(&newMenu);
    QAction * editAction = menu.addAction(QIcon(":/res/image/edit.png"), "编辑");
    QAction * deleteAction = menu.addAction(QIcon(":/res/image/delete.png"), "删除");
    QAction * renameAction = menu.addAction(QIcon(":/res/image/rename.png"), "重命名");
    QAction * retimeAction = menu.addAction(QIcon(":/res/image/retime.png"), "修改时间");
    // 远程执行
    connect(execAction, &QAction::triggered, [=](){
        QString file = os == WIN ? getPwdUrl() + "\\" + ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text() :
                                   getPwdUrl() + "/" + ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text();
        http.execRequest(file);
    });
    // 刷新
    connect(flashAction, &QAction::triggered, [=](){
        on_goButton_clicked();
    });
    // 删除文件/文件夹
    connect(deleteAction, &QAction::triggered, [=](){
        QString file = os == WIN ? getPwdUrl() + "\\" + ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text() :
                                   getPwdUrl() + "/" + ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text();
        http.deleteFileRequest(file);
    });
    // 下载文件到服务器
    connect(downloderAction, &QAction::triggered, [=](){
        DownloaderDialog downloaderDlg;
        downloaderDlg.exec();
        if(!downloaderDlg.url.isEmpty() && !downloaderDlg.filename.isEmpty())
        {
            QString file = os == WIN ? getPwdUrl() + "\\" + downloaderDlg.filename :
                                       getPwdUrl() + "/" + downloaderDlg.filename;
            // 判断文件是否已经存在
            MdFileInfo fileinfo;
            fileinfo.setName(downloaderDlg.filename);
            if(findTableItem(fileinfo) >= 0)
                if(QMessageBox::question(this, "提示", "保存的文件名已经存在，是否覆盖？") == QMessageBox::No)
                    return;
            http.downloaderRequest(downloaderDlg.url, file);
        }
    });
    // 重命名
    connect(renameAction, &QAction::triggered, [=](){
        bool isOk = false;
        QString text = QInputDialog::getText(this,
                                             "重命名",
                                             "",
                                             QLineEdit::Normal,
                                             ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text(),
                                             &isOk);
        if(isOk && ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text() != text)
        {
            http.renameRequest(getPwdUrl(), ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text(), text);
        }
    });
    // 修改文件时间
    connect(retimeAction, &QAction::triggered, [=](){
        bool isOk = false;
        QString text = QInputDialog::getText(this,
                                             "修改文件时间",
                                             "",
                                             QLineEdit::Normal,
                                             ui->tableWidget->item(ui->tableWidget->currentRow(), 2)->text(),
                                             &isOk);
        if(isOk && ui->tableWidget->item(ui->tableWidget->currentRow(), 2)->text() != text)
        {
            QString file = os == WIN ? getPwdUrl() + "\\" + ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text() :
                                       getPwdUrl() + "/" + ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text();
            http.retimeRequest(file, text);
        }
    });
    //下载文件
    connect(downAction, &QAction::triggered, [=](){
        QString saveFile = QFileDialog::getSaveFileName(this,
                                                            "保存文件",
                                                            "./"+ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text(),
                                                            "All file(*.*)");
        if(saveFile.length() > 0)
        {
            QString remoteFile = os == WIN ? getPwdUrl() + "\\" + ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text() :
                                             getPwdUrl() + "/" + ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text();
            ui->progressBar->setValue(0);
            ui->progressBar->setVisible(true);
            http.downloadRequest(remoteFile, saveFile);
        }
    });
    //上传文件
    connect(uploadAction, &QAction::triggered, [=](){
        UploadDialog uploadDlg;
        uploadDlg.exec();
        if(uploadDlg.selected)
        {
            MdFileInfo fileInfo;
            fileInfo.setName(uploadDlg.fileName);
            if(findTableItem(fileInfo) >= 0)
            {
                QMessageBox::critical(this, "提示", "文件已经存在，请重新选择文件名");
                return;
            }
            tmpBlockSize = uploadDlg.blockSize;
            // 先发送第一个文件分块数据
            QFileInfo info(uploadDlg.filePath);
            QString remoteFile = os == WIN ? getPwdUrl() + "\\" + uploadDlg.fileName :
                                             getPwdUrl() + "/" + uploadDlg.fileName;
            int tol = qCeil(info.size() *1.0 / uploadDlg.blockSize);
            QFile file(uploadDlg.filePath);
            if(file.open(QIODevice::ReadOnly))
            {
                http.uploadRequest(remoteFile, uploadDlg.filePath, "w+", 0, tol, file.read(uploadDlg.blockSize));
                file.close();
            }else
            {
                QMessageBox::critical(this, "错误", "打开文件失败:" + uploadDlg.filePath);
            }
            uploadDlg.selected = false;
            ui->progressBar->setValue(0);
            ui->progressBar->setVisible(true);
        }
    });
    //新建文件夹
    connect(newFolderAction, &QAction::triggered, [=](){
        bool isOk = false;
        QString text = QInputDialog::getText(this,
                                             "文件夹名称",
                                             "",
                                             QLineEdit::Normal,
                                             "",
                                             &isOk);
        if(isOk)
        {
            QString folder = os == WIN ? getPwdUrl() + "\\" + text :
                                       getPwdUrl() + "/" + text;
            http.newfolderRequest(folder);
        }
    });
    //新建文本
    connect(newFileAction, &QAction::triggered, [=](){
        // 临时断开网络信号连接
        disconnect(&http, &HttpCore::sigResponse, this, &FileManageWindow::on_response);

        TextEditDialog textDlg(mdHole, true, getPwdUrl(), this);
        textDlg.setWindowTitle(os == WIN ? getPwdUrl() + "\\NewFile" :  getPwdUrl() + "/NewFile");
        textDlg.exec();

        // 恢复网络信号连接
        connect(&http, &HttpCore::sigResponse, this, &FileManageWindow::on_response);
        // 刷新当前目录
        on_goButton_clicked();
    });
    //修改文本
    connect(editAction, &QAction::triggered, [=](){
        // 临时断开网络信号连接
        disconnect(&http, &HttpCore::sigResponse, this, &FileManageWindow::on_response);

        QString file = os == WIN ? getPwdUrl() + "\\" + ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text() :
                                   getPwdUrl() + "/" + ui->tableWidget->item(ui->tableWidget->currentRow(), 1)->text();
        TextEditDialog textDlg(mdHole, false, file, this);
        textDlg.setWindowTitle(file);
        textDlg.exec();

        // 恢复网络信号连接
        connect(&http, &HttpCore::sigResponse, this, &FileManageWindow::on_response);
    });

    if(ui->tableWidget->itemAt(ui->tableWidget->viewport()->mapFromGlobal(QCursor::pos())) == nullptr){
        editAction->setVisible(false);
        renameAction->setVisible(false);
        retimeAction->setVisible(false);
        execAction->setVisible(false);
        downAction->setVisible(false);
        deleteAction->setVisible(false);
    }
    menu.exec(QCursor::pos());
}

void FileManageWindow::closeEvent(QCloseEvent *e)
{
    // 这里是个坑，必须手动disconnect信号关联
    // 因为qt内存new的对话框在短期内没有释放，再次new还是之前的对话框数据，会产生2次信号重复绑定
    disconnect(&http, &HttpCore::sigResponse, this, &FileManageWindow::on_response);
}
// ********************** 以下为网络通信槽函数 *****************************
void FileManageWindow::on_response(int type, QString url, QVariant data)
{
    if(url == mdHole.getUrl())
    {
        if(type == RET_INFO)        // |-- 操作系统信息
        {
            if(data.toStringList().length() < 4)    //linux是4个数据，windows多一个路径数据，是5个
            {
                return;
            }
            ui->labelStatus->setText(data.toStringList().at(1));
//            statusBar->showMessage(data.toStringList().at(1));
            os =  data.toStringList().at(1).contains("windows", Qt::CaseInsensitive) ? WIN: LINUX;
            setPwdUrl(data.toStringList().at(0));
            ui->userLabel->setText(QString("当前脚本版本 [%1]\t\t当前用户 [%2]").arg(data.toStringList().at(3)).arg(data.toStringList().at(2)));
            // 设置树形列表根目录
            if(os == WIN){
                for(int i=0; i < data.toStringList().at(4).length() ; i=i+2)
                {
                    addTreeRootItem(data.toStringList().at(4).mid(i, 2));
                }
            }else{
                addTreeRootItem("/");
            }
            // 设置树形列表子目录
            formatTreeItem(data.toStringList().at(0));
            // 读取默认目录
            http.getFileListRequest(data.toStringList().at(0));
        }else if(type == RET_FILELIST)      // |-- 文件列表
        {
            QList<MdFileInfo> files = data.value<QList<MdFileInfo>>();
            foreach(MdFileInfo fileInfo, files)
            {
                formatTreeItem(fileInfo.getPath());
                if(fileInfo.getType() == MY_FOLDER)
                {
                    formatTreeItem(fileInfo.getFullName());
                    insertTableItem(fileInfo);
                }else
                {
                    insertTableItem(fileInfo);
                }
            }
        }else if(type == RET_FILEDEL)       // |-- 删除文件
        {
            unformatTreeItem(data.toString());
            QFileInfo fileInfo(data.toString());
            MdFileInfo file;
            file.setName(fileInfo.fileName());
            removeTableItem(file);
        }else if(type == RET_DOWNLOADER)       // |-- 下载文件到服务器
        {
            MdFileInfo fileinfo = data.value<MdFileInfo>();
            insertTableItem(fileinfo);
        }else if(type == RET_EXEC)          // |-- 远程执行
        {
            if(data.toBool())
                QMessageBox::information(this, "远程执行", "执行成功");
            else
                QMessageBox::critical(this, "远程执行", "执行失败，请检查是否有相关权限");
        }else if(type == RET_RENAME)        // |-- 重命名文件
        {
            if(data.toStringList().length() == 3 && data.toStringList().at(0) == getPwdUrl())
            {
                MdFileInfo fileinfo;
                fileinfo.setName(data.toStringList().at(1));
                int row = findTableItem(fileinfo);
                if(row >=0)
                {
                    ui->tableWidget->item(row, 1)->setText(data.toStringList().at(2));
                }
            }
        }else if(type == RET_RETIME)        // |-- 修改文件时间
        {
            if(data.toStringList().length() == 3)
            {
                MdFileInfo mdFileInfo;
                mdFileInfo.setPath(data.toStringList().at(0));
                mdFileInfo.setName(data.toStringList().at(1));
                mdFileInfo.setTime(data.toStringList().at(2));
                int row = findTableItem(mdFileInfo);
                if(row >=0)
                {
                    ui->tableWidget->item(row, 2)->setText(mdFileInfo.getTime());
                }
            }
        }else if(type == RET_NEWFOLDER)     // |-- 新建文件夹
        {
            if(data.toStringList().length() == 3)
            {
                MdFileInfo mdFileInfo;
                mdFileInfo.setPath(data.toStringList().at(0));
                mdFileInfo.setName(data.toStringList().at(1));
                mdFileInfo.setType(MY_FOLDER);
                mdFileInfo.setSize("0");
                mdFileInfo.setTime(data.toStringList().at(2));
                insertTableItem(mdFileInfo);
            }
        }else if(type == RET_DOWNLOAD)      // |-- 下载文件
        {
            if(data.toStringList().length() == 3)
            {
                int tol = data.toStringList().at(1).toInt();
                int idx = data.toStringList().at(2).toInt();
                ui->progressBar->setVisible(true);
                ui->progressBar->setRange(0, tol);
                ui->progressBar->setValue(idx + 1);
                if(idx +1 >= tol)
                {
                    //下载完成后延时3秒隐藏进度条
                    QTimer * timer = new QTimer(this);
                    timer->setInterval(3000);
                    connect(timer, &QTimer::timeout, [=](){
                        ui->progressBar->setVisible(false);
                        timer->stop();
                        timer->deleteLater();
                    });
                    timer->start();
                }
            }
        }else if(type == RET_UPLOAD)      // |-- 上传文件
        {
            qDebug() << "这是filemanager的槽函数";
            if(data.toStringList().length() == 4)
            {
                int tol = data.toStringList().at(2).toInt();
                int idx = data.toStringList().at(3).toInt();
                ui->progressBar->setVisible(true);
                ui->progressBar->setRange(0, tol);
                ui->progressBar->setValue(idx + 1);
                if(idx +1 >= tol)
                {
                    //下载完成后延时3秒隐藏进度条
                    QTimer * timer = new QTimer(this);
                    timer->setInterval(3000);
                    connect(timer, &QTimer::timeout, [=](){
                        ui->progressBar->setVisible(false);
                        timer->stop();
                        timer->deleteLater();
                    });
                    timer->start();
                    //刷新当前目录
                    on_goButton_clicked();
                }else
                {
                    // 传送下一个文件分块数据
                    QFile file( data.toStringList().at(1));
                    if(file.open(QIODevice::ReadOnly))
                    {
                        file.seek((idx + 1) * tmpBlockSize);
                        http.uploadRequest(data.toStringList().at(0), data.toStringList().at(1), "a", idx + 1, tol, file.read(tmpBlockSize));
                        file.close();
                    }else
                    {
                        QMessageBox::critical(this, "错误", "打开文件失败" + data.toStringList().at(1));
                    }
                }
            }
        }

    }
}

// ********************** 以下为UI私有函数 *****************************
QTreeWidgetItem * FileManageWindow::addTreeRootItem(QString name)
{
    QTreeWidgetItem *root = new QTreeWidgetItem(ui->treeWidget);
    root->setText(0, name);
    root->setIcon(0, QIcon(":/res/image/disk.png"));
    ui->treeWidget->addTopLevelItem(root);
    return root;
}

QTreeWidgetItem * FileManageWindow::addTreeChildItem(QString name, QTreeWidgetItem * item)
{
    QTreeWidgetItem *childItem = new QTreeWidgetItem(item);
    childItem->setText(0, name);
    childItem->setIcon(0, QIcon(":/res/image/folder.png"));
    //item->addChild()
    return childItem;
}

QTreeWidgetItem * FileManageWindow::findTreeRootItem(QString name)
{
    for (int i=0; i < ui->treeWidget->topLevelItemCount(); i++) {
        if(ui->treeWidget->topLevelItem(i)->text(0).compare(name, Qt::CaseInsensitive) == 0)
            return ui->treeWidget->topLevelItem(i);
    }
    return nullptr;
}

QTreeWidgetItem * FileManageWindow::findTreeChildItem(QString name, QTreeWidgetItem *item)
{
    for (int i=0; i<item->childCount(); i++) {
        if(os == WIN)
        {
            if(item->child(i)->text(0).compare(name, Qt::CaseInsensitive) == 0)
                return item->child(i);
        }else
        {
            if(item->child(i)->text(0) == name)
                return item->child(i);
        }
    }
    return nullptr;
}

void FileManageWindow::formatTreeItem(QString path)
{
    if(os == WIN)       //windows系统
    {
        QStringList data = path.split("\\");
        if(data.length() > 0 && data.at(0).contains(':'))   //校验格式
        {
            QTreeWidgetItem * tmpItem = nullptr;
            for(int i=0; i<data.length(); i++)
            {
                if(i == 0)  //根节点
                {
                    QTreeWidgetItem * tmp = findTreeRootItem(data.at(0));
                    tmpItem = tmp == nullptr ? addTreeRootItem(data.at(0)) : tmp;
                }else
                {
                    tmpItem = findTreeChildItem(data.at(i), tmpItem) == nullptr ? addTreeChildItem(data.at(i), tmpItem) : findTreeChildItem(data.at(i), tmpItem);
                }
                if(tmpItem == nullptr)
                    break;
                tmpItem->setExpanded(true);
            }
        }
    }else               //linux系统
    {
        QStringList data = path.split("/");
        if(data.length() > 0 && data.at(0) == "" && data.at(1) != "")
        {
            QTreeWidgetItem * tmpItem = findTreeRootItem("/");
            tmpItem->setExpanded(true);
            for(int i=1; i<data.length(); i++)
            {
                tmpItem = findTreeChildItem(data.at(i), tmpItem) == nullptr ? addTreeChildItem(data.at(i), tmpItem) : findTreeChildItem(data.at(i), tmpItem);
                if(tmpItem == nullptr)
                    break;
                tmpItem->setExpanded(true);
            }
        }
    }
}

void FileManageWindow::unformatTreeItem(QString path)
{
    if(os == WIN)       //windows系统
    {
        QStringList data = path.split("\\");
        if(data.length() > 0 && data.at(0).contains(':'))   //校验格式
        {
            QTreeWidgetItem * tmpItem = nullptr;
            for(int i=0; i<data.length(); i++)
            {
                if(i == 0)  //根节点
                {
                    tmpItem = findTreeRootItem(data.at(0));
                }else
                {
                    tmpItem = findTreeChildItem(data.at(i), tmpItem);
                }
                if(tmpItem == nullptr)
                    break;
            }
            if(tmpItem)
                delete tmpItem;
        }
    }else               //linux系统
    {
        QStringList data = path.split("/");
        if(data.length() > 0 && data.at(0) == "")
        {
            QTreeWidgetItem * tmpItem = findTreeRootItem("/");
            for(int i=1; i<data.length(); i++)
            {
                tmpItem = findTreeChildItem(data.at(i), tmpItem);
                if(tmpItem == nullptr)
                    break;
            }
            if(tmpItem)
                delete tmpItem;
        }
    }
}

void FileManageWindow::insertTableItem(MdFileInfo file)
{
    int curRow = ui->tableWidget->rowCount();
    ui->tableWidget->insertRow(curRow);

    if(file.getType() == MY_FILE)
    {
        ui->tableWidget->setItem(curRow,0,new QTableWidgetItem(file.getIcon(),""));
        ui->tableWidget->item(curRow, 0)->setData(Qt::UserRole, "file");
    }else
    {
        ui->tableWidget->setItem(curRow,0,new QTableWidgetItem(QIcon(":/res/image/folder.png"),""));
        ui->tableWidget->item(curRow, 0)->setData(Qt::UserRole, "folder");
    }
    ui->tableWidget->setItem(curRow,1,new QTableWidgetItem(file.getName()));
    ui->tableWidget->setItem(curRow,2,new QTableWidgetItem(file.getTime()));
    ui->tableWidget->setItem(curRow,3,new QTableWidgetItem(file.getSize()));
}

void FileManageWindow::removeTableItem(MdFileInfo file)
{
    int curRow = ui->tableWidget->rowCount();
    for(int i = 0; i < curRow ; i ++)
    {
        if(ui->tableWidget->item(i, 1)->text() == file.getName())
        {
            //qDebug() << "要移除的dp" <<Descriptor << QString("%1").arg(Descriptor) << "；当前dp" << ui->clientWidget->item(i, 0)->text();
            ui->tableWidget->removeRow(i);
            break;
        }
    }
}

int FileManageWindow::findTableItem(MdFileInfo file)
{
    int row = -1;
    int curRow = ui->tableWidget->rowCount();
    for(int i = 0; i < curRow ; i ++)
    {
        if(ui->tableWidget->item(i, 1)->text() == file.getName())
        {
            row = i;
            break;
        }
    }
    return row;
}

void FileManageWindow::setPwdUrl(QString pwdUrl)
{
    // 将多个\或者/符号视为1个
    pwdUrl =  pwdUrl.replace(QRegExp("\\\\+"), "\\");
    pwdUrl =  pwdUrl.replace(QRegExp("/+"), "/");
    ui->fileUrlEdit->setText(pwdUrl);
}

QString FileManageWindow::getPwdUrl()
{
    QString path = ui->fileUrlEdit->text();
    // 将多个\或者/符号视为1个
    path =  path.replace(QRegExp("\\\\+"), "\\");
    path =  path.replace(QRegExp("/+"), "/");
    // 去掉最后的\或者/
    while (path.length() > 0 && (path.at(path.length() -1) == '\\' || path.at(path.length() -1) == '/')) {
        path = path.left(path.length() - 1);
    }
    return path;
}
