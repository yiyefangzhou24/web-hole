#include "payloaddialog.h"
#include "ui_payloaddialog.h"
#include "payloadeditdialog.h"

PayloadDialog::PayloadDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::PayloadDialog)
{
    ui->setupUi(this);

    //UI样式设置
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "脚本类型"<<"载荷名称" << "载荷内容");
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);              //设置不可编辑
    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);                     //开启右键菜单
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);             //设置整行选中
    ui->tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);            //单行选择
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);                                     //关闭选中虚线框
    ui->tableWidget->setColumnWidth(0 , 80);                                             //设置第一列宽度为60
    ui->tableWidget->setColumnWidth(1 , 140);                                             //设置第二列宽度为100
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);   //设置第三列自动宽度

    // 加载载荷信息
    mdPayloadList = sqlModel.selectPayload();
    foreach(MdPayload mdPayload, mdPayloadList)
    {
        insertListItem(mdPayload);
    }
}

PayloadDialog::~PayloadDialog()
{
    delete ui;
}

// ******************** 以下为UI私有函数 *****************
void PayloadDialog::insertListItem(MdPayload mdPayload)
{
    int curRow = ui->tableWidget->rowCount();
    ui->tableWidget->insertRow(curRow);

    ui->tableWidget->setItem(curRow,0,new QTableWidgetItem(mdPayload.getScript()));
    ui->tableWidget->setItem(curRow,1,new QTableWidgetItem(mdPayload.getName()));
    ui->tableWidget->setItem(curRow,2,new QTableWidgetItem(mdPayload.getPayload()));

    // 用每一行的第一列data存储该hole的唯一索引id
    ui->tableWidget->item(curRow, 0)->setData(Qt::UserRole, mdPayload.getId());
}

void PayloadDialog::updateAllPayload()
{
    ui->tableWidget->clearContents();
    ui->tableWidget->setRowCount(0);
    mdPayloadList = sqlModel.selectPayload();
    foreach(MdPayload mdPayload, mdPayloadList)
    {
        insertListItem(mdPayload);
    }
}

// ******************** 以下为UI槽函数 *****************
void PayloadDialog::on_tableWidget_customContextMenuRequested(const QPoint &pos)
{
    QMenu menu(this);
    QAction * addAction = menu.addAction(QIcon(":/res/image/add.png"), "新增");
    QAction * editAction = menu.addAction(QIcon(":/res/image/edit.png"), "修改");
    QAction * deleteAction = menu.addAction(QIcon(":/res/image/delete.png"), "删除");
    connect(addAction, &QAction::triggered, [=](){
        PayloadEditDialog payloadEditDlg;
        payloadEditDlg.exec();
        updateAllPayload();
    });
    connect(deleteAction, &QAction::triggered, [=](){
        if(sqlModel.deletePayload(ui->tableWidget->item(ui->tableWidget->currentRow(), 0)->data(Qt::UserRole).toInt()))
            ui->tableWidget->removeRow(ui->tableWidget->currentRow());
    });
    connect(editAction, &QAction::triggered, [=](){
        PayloadEditDialog payloadEditDlg;
        payloadEditDlg.setPayload(sqlModel.findPayload(ui->tableWidget->item(ui->tableWidget->currentRow(), 0)->data(Qt::UserRole).toInt()));
        payloadEditDlg.exec();
        updateAllPayload();
    });

//    qDebug() <<ui->tableWidget->itemAt(ui->tableWidget->viewport()->mapFromGlobal(QCursor::pos()));
    if(ui->tableWidget->itemAt(ui->tableWidget->viewport()->mapFromGlobal(QCursor::pos())) == nullptr){
        deleteAction->setVisible(false);
        editAction->setVisible(false);
    }

    menu.exec(QCursor::pos());
}

void PayloadDialog::on_tableWidget_itemDoubleClicked(QTableWidgetItem *item)
{
    PayloadEditDialog payloadEditDlg;
    payloadEditDlg.setPayload(sqlModel.findPayload(ui->tableWidget->item(item->row(), 0)->data(Qt::UserRole).toInt()));
    payloadEditDlg.exec();
    updateAllPayload();
}
