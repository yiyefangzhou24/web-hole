#include "global.h"
#include "testallholedialog.h"
#include "ui_testallholedialog.h"

TestAllHoleDialog::TestAllHoleDialog(QList<MdHole> holeList, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TestAllHoleDialog)
{
    ui->setupUi(this);

    mdHoleList = holeList;

    //UI样式设置
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
    ui->progressBar->setRange(0, mdHoleList.size());

    // 绑定错误和响应信号
    connect(&http, &HttpCore::sigError, this, &TestAllHoleDialog::on_error);
    connect(&http, &HttpCore::sigResponse, this, &TestAllHoleDialog::on_response);

    // 循环发送检活请求
    for (int i = 0; i < mdHoleList.size(); i++)
    {
        http.setHole(mdHoleList[i]);
        http.testRequest();
    }
}

TestAllHoleDialog::~TestAllHoleDialog()
{
    delete ui;
}


void TestAllHoleDialog::closeEvent(QCloseEvent *event)
{
    // 解除绑定错误和响应信号
    disconnect(&http, &HttpCore::sigError, this, &TestAllHoleDialog::on_error);
    disconnect(&http, &HttpCore::sigResponse, this, &TestAllHoleDialog::on_response);
}

// ******************** 以下为网络信号槽函数 **************
void TestAllHoleDialog::on_error(ERR_CODE code, QString url, QString msg)
{
    qDebug() << "错误类型:" << code << "|网址：" << url << "|错误信息："<< msg;
    QLog::log(QString("错误类型:%1|网址:%2|详细信息:%3").arg(strErrType.at(code)).arg(url).arg(msg));

    if(code == ERR_NET){                        // 网络说明shell丢失
        updateHole(url, "Deleted");
    }else if(code == ERR_FORMAT){               // 返回格式错误可能是shell失效
        updateHole(url, "Useless");
    }else{                                      // 返回其他类型记录未知（理论上不会返回其他类型）
        updateHole(url, "Unknown");
    }
}

void TestAllHoleDialog::on_response(int type, QString url, QVariant data)
{
    if(type == RET_TEST)        // |-- 测试可用性
    {
        data.toBool() ? updateHole(url, "Normal") : updateHole(url, "Useless");
    }
}

// ******************** 以下为私有函数 **************
void TestAllHoleDialog::updateHole(QString url, QString status)
{
    for (int i = 0; i < mdHoleList.size(); i++)
    {
        if(mdHoleList[i].getUrl() == url)
        {
            mdHoleList[i].setStatus(status);

            // 更新本地的db文件
            sqlModel.updateHole(mdHoleList[i]);

            // 更新UI显示
            testNum ++;
            ui->progressBar->setValue(testNum);
            setWindowTitle(QString("正在批量检活 [ 进度: %1 / %2 ]").arg(testNum).arg(mdHoleList.size()));

            break;
        }
    }
}
