#include "sqlmodel.h"

//SqlModel::SqlModel()
//{
//    if(QSqlDatabase::contains("qt_sql_default_connection"))
//    {
//        sqlDB = QSqlDatabase::database("qt_sql_default_connection");
//    }
//    else {
//        sqlDB = QSqlDatabase::addDatabase("QSQLITE");
//    }
//    query = QSqlQuery();
//}

//SqlModel::SqlModel(QString dbname)
//{
//    if(QSqlDatabase::contains("qt_sql_default_connection"))
//    {
//        sqlDB = QSqlDatabase::database("qt_sql_default_connection");
//    }
//    else {
//        sqlDB = QSqlDatabase::addDatabase("QSQLITE");
//    }
//    query = QSqlQuery();

//    connect(dbname);
//}

bool SqlModel::connect(QString dbname)
{
    if(dbname.isEmpty())
        return false;

    if(QSqlDatabase::contains("qt_sql_default_connection"))
    {
        sqlDB = QSqlDatabase::database("qt_sql_default_connection");
    }
    else {
        sqlDB = QSqlDatabase::addDatabase("QSQLITE");
    }
    query = QSqlQuery();
    sqlDB.setDatabaseName(dbname);

    // 检测是否有该数据库
    QFile file(dbname);
    return file.exists() ? sqlDB.open() : false;
}

void SqlModel::close()
{
    sqlDB.close();
}

QString SqlModel::getLastError()
{
    return sqlDB.lastError().text() + query.lastError().text();
}

bool SqlModel::initTables()
{
    keepAlive();
    QString sql1 = "CREATE TABLE \"webshell\" ( \"id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"url\" TEXT NOT NULL, \"passwd\" TEXT NOT NULL, \"info\" TEXT,\"iv\" TEXT NOT NULL,\"cookies\" TEXT, \"code\" TEXT NOT NULL, \"status\" TEXT NOT NULL, \"script\" TEXT NOT NULL, \"dbtype\" TEXT, \"dbcode\" TEXT, \"dbhost\" TEXT, \"dbuser\" TEXT, \"dbpasswd\" TEXT, \"time\" TEXT NOT NULL)";
    QString sql2 = "CREATE TABLE \"headers\" ( \"id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"name\" TEXT NOT NULL, \"val\" TEXT)";
    QString sql3 = "CREATE TABLE \"payloads\" ( \"id\" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, \"script\" TEXT NOT NULL,  \"name\" TEXT NOT NULL, \"payload\" TEXT NOT NULL, \"needrep\" TEXT NOT NULL)";
    QString sql4 = "CREATE TABLE \"config\" ( \"cnfname\" TEXT NOT NULL,  \"cnfval\" TEXT)";
    return query.exec(sql1) && query.exec(sql2) && query.exec(sql3) && query.exec(sql3);
}

void SqlModel::keepAlive()
{
    if(!sqlDB.isOpen())
    {
        close();
        connect();
    }
}

bool SqlModel::insertHole(MdHole mdHole)
{
    keepAlive();
    query.prepare("INSERT INTO webshell(url,passwd,cookies,info,iv,code,status,script,dbtype,dbcode,dbhost,dbuser,dbpasswd,time) VALUES (:url, :passwd,  :cookies, :info, :iv,:code,:status,:script,:dbtype,:dbcode,:dbhost,:dbuser,:dbpasswd,:time)");
    query.bindValue(":url", mdHole.getUrl());
    query.bindValue(":passwd", mdHole.getPasswd());
    query.bindValue(":cookies", mdHole.getCookies());
    query.bindValue(":info", mdHole.getInfo());
    query.bindValue(":iv", mdHole.getIv());
    query.bindValue(":code", mdHole.getCode());
    query.bindValue(":script", mdHole.getScript());
    query.bindValue(":status", mdHole.getStatus());
    query.bindValue(":dbtype", mdHole.getDbType());
    query.bindValue(":dbcode", mdHole.getDbCode());
    query.bindValue(":dbhost", mdHole.getDbHost());
    query.bindValue(":dbuser", mdHole.getDbUser());
    query.bindValue(":dbpasswd", mdHole.getDbPasswd());
    query.bindValue(":time", mdHole.getTime());
    return query.exec();
}

bool SqlModel::updateHole(MdHole mdHole)
{
    if(mdHole.getId() < 0)
        return false;

    keepAlive();
    QStringList col,val;
    if(!mdHole.getUrl().isEmpty())
    {
        col.append("url=?");
        val.append(mdHole.getUrl());
    }
    if(!mdHole.getPasswd().isEmpty())
    {
        col.append("passwd=?");
        val.append(mdHole.getPasswd());
    }
    if(!mdHole.getCookies().isEmpty())
    {
        col.append("cookies=?");
        val.append(mdHole.getCookies());
    }
    if(!mdHole.getInfo().isEmpty())
    {
        col.append("info=?");
        val.append(mdHole.getInfo());
    }
    if(!mdHole.getScript().isEmpty())
    {
        col.append("script=?");
        val.append(mdHole.getScript());
    }
    if(!mdHole.getIv().isEmpty())
    {
        col.append("iv=?");
        val.append(mdHole.getIv());
    }
    if(!mdHole.getCode().isEmpty())
    {
        col.append("code=?");
        val.append(mdHole.getCode());
    }
    if(!mdHole.getStatus().isEmpty())
    {
        col.append("status=?");
        val.append(mdHole.getStatus());
    }
    if(!mdHole.getDbType().isEmpty())
    {
        col.append("dbtype=?");
        val.append(mdHole.getDbType());
    }
    if(!mdHole.getDbCode().isEmpty())
    {
        col.append("dbcode=?");
        val.append(mdHole.getDbCode());
    }
    if(!mdHole.getDbHost().isEmpty())
    {
        col.append("dbhost=?");
        val.append(mdHole.getDbHost());
    }
    if(!mdHole.getDbUser().isEmpty())
    {
        col.append("dbuser=?");
        val.append(mdHole.getDbUser());
    }
    if(!mdHole.getDbPasswd().isEmpty())
    {
        col.append("dbpasswd=?");
        val.append(mdHole.getDbPasswd());
    }
    if(!mdHole.getTime().isEmpty())
    {
        col.append("time=?");
        val.append(mdHole.getTime());
    }
    query.prepare(QString("UPDATE webshell SET %1 WHERE id = ?").arg(col.join(",")));
    foreach(QString item, val)
    {
        query.addBindValue(item);
    }
    query.addBindValue(mdHole.getId());
    return query.exec();
}


QList<MdHole> SqlModel::selectHole()
{
    keepAlive();
    query.prepare("SELECT * FROM webshell");
    QList<MdHole> mdHoleList;
    if(query.exec())
    {
        while(query.next())
        {
            MdHole mdHole;
            mdHole.setId(query.value("id").toInt());
            mdHole.setUrl(query.value("url").toString());
            mdHole.setPasswd(query.value("passwd").toString());
            mdHole.setCookies(query.value("cookies").toString());
            mdHole.setInfo(query.value("info").toString());
            mdHole.setIv(query.value("iv").toString());
            mdHole.setCode(query.value("code").toString());
            mdHole.setStatus(query.value("status").toString());
            mdHole.setScript(query.value("script").toString());
            mdHole.setDbType(query.value("dbtype").toString());
            mdHole.setDbCode(query.value("dbcode").toString());
            mdHole.setDbHost(query.value("dbhost").toString());
            mdHole.setDbUser(query.value("dbuser").toString());
            mdHole.setDbPasswd(query.value("dbpasswd").toString());
            mdHole.setTime(query.value("time").toString());
            mdHoleList.append(mdHole);
        }
    }
    return mdHoleList;
}

MdHole SqlModel::findHole(int id)
{
    keepAlive();
    MdHole mdHole;
    query.prepare("SELECT * FROM webshell WHERE id=:id");
    query.bindValue(":id", id);
    if(query.exec() && query.next())
    {
        mdHole.setId(query.value("id").toInt());
        mdHole.setUrl(query.value("url").toString());
        mdHole.setPasswd(query.value("passwd").toString());
        mdHole.setCookies(query.value("cookies").toString());
        mdHole.setInfo(query.value("info").toString());
        mdHole.setIv(query.value("iv").toString());
        mdHole.setCode(query.value("code").toString());
        mdHole.setStatus(query.value("status").toString());
        mdHole.setScript(query.value("script").toString());
        mdHole.setDbType(query.value("dbtype").toString());
        mdHole.setDbCode(query.value("dbcode").toString());
        mdHole.setDbHost(query.value("dbhost").toString());
        mdHole.setDbUser(query.value("dbuser").toString());
        mdHole.setDbPasswd(query.value("dbpasswd").toString());
        mdHole.setTime(query.value("time").toString());
    }
    return mdHole;
}

MdHole SqlModel::findHole(QString url)
{
    keepAlive();
    MdHole mdHole;
    query.prepare("SELECT * FROM webshell WHERE url=:url");
    query.bindValue(":url", url);
    if(query.exec() && query.next())
    {
        mdHole.setId(query.value("id").toInt());
        mdHole.setUrl(query.value("url").toString());
        mdHole.setPasswd(query.value("passwd").toString());
        mdHole.setCookies(query.value("cookies").toString());
        mdHole.setInfo(query.value("info").toString());
        mdHole.setIv(query.value("iv").toString());
        mdHole.setCode(query.value("code").toString());
        mdHole.setStatus(query.value("status").toString());
        mdHole.setScript(query.value("script").toString());
        mdHole.setDbType(query.value("dbtype").toString());
        mdHole.setDbCode(query.value("dbcode").toString());
        mdHole.setDbHost(query.value("dbhost").toString());
        mdHole.setDbUser(query.value("dbuser").toString());
        mdHole.setDbPasswd(query.value("dbpasswd").toString());
        mdHole.setTime(query.value("time").toString());
    }
    return mdHole;
}

bool SqlModel::deleteHole(int id)
{
    keepAlive();
    query.prepare("DELETE FROM webshell WHERE id=:id");
    query.bindValue(":id", id);
    return query.exec();
}


QList<MdHeader> SqlModel::selectHeader()
{
    keepAlive();
    query.prepare("SELECT * FROM headers");
    QList<MdHeader> mdHeaderList;
    if(query.exec())
    {
        while(query.next())
        {
            MdHeader mdHeader;
            mdHeader.setId(query.value("id").toInt());
            mdHeader.setName(query.value("name").toString());
            mdHeader.setVal(query.value("val").toString());
            mdHeaderList.append(mdHeader);
        }
    }
    return mdHeaderList;
}

bool SqlModel::deleteHeader(int id)
{
    keepAlive();
    query.prepare("DELETE FROM headers WHERE id=:id");
    query.bindValue(":id", id);
    return query.exec();
}

bool SqlModel::insertHeader(MdHeader mdHeader)
{
    keepAlive();
    query.prepare("INSERT INTO headers(name,val) VALUES (:name, :val)");
    query.bindValue(":name", mdHeader.getName());
    query.bindValue(":val", mdHeader.getVal());
    return query.exec();
}

bool SqlModel::updateHeader(MdHeader mdHeader)
{
    if(mdHeader.getId() < 0)
        return false;

    keepAlive();
    QStringList col,val;
    if(!mdHeader.getName().isEmpty())
    {
        col.append("name=?");
        val.append(mdHeader.getName());
    }
    if(!mdHeader.getVal().isEmpty())
    {
        col.append("val=?");
        val.append(mdHeader.getVal());
    }
    query.prepare(QString("UPDATE headers SET %1 WHERE id = ?").arg(col.join(",")));
    foreach(QString item, val)
    {
        query.addBindValue(item);
    }
    query.addBindValue(mdHeader.getId());
    return query.exec();
}

MdHeader SqlModel::findHeader(int id)
{
    keepAlive();
    MdHeader mdHeader;
    query.prepare("SELECT * FROM headers WHERE id=:id");
    query.bindValue(":id", id);
    if(query.exec() && query.next())
    {
        mdHeader.setId(query.value("id").toInt());
        mdHeader.setName(query.value("name").toString());
        mdHeader.setVal(query.value("val").toString());
    }
    return mdHeader;
}

QList<MdPayload> SqlModel::selectPayload()
{
    keepAlive();
    query.prepare("SELECT * FROM payloads");
    QList<MdPayload> mdPayloadList;
    if(query.exec())
    {
        while(query.next())
        {
            MdPayload mdPayload;
            mdPayload.setId(query.value("id").toInt());
            mdPayload.setScript(query.value("script").toString());
            mdPayload.setName(query.value("name").toString());
            mdPayload.setPayload(query.value("payload").toString());
            mdPayload.setNeedRep(query.value("needreq").toString());
            mdPayloadList.append(mdPayload);
        }
    }
    return mdPayloadList;
}

bool SqlModel::insertPayload(MdPayload mdPayload)
{
    keepAlive();
    query.prepare("INSERT INTO payloads(script,name,payload,needrep) VALUES (:script,:name, :payload, :needrep)");
    query.bindValue(":script", mdPayload.getScript());
    query.bindValue(":name", mdPayload.getName());
    query.bindValue(":payload", mdPayload.getPayload());
    query.bindValue(":needrep", mdPayload.getNeedRep());
    return query.exec();
}

bool SqlModel::deletePayload(int id)
{
    keepAlive();
    query.prepare("DELETE FROM payloads WHERE id=:id");
    query.bindValue(":id", id);
    return query.exec();
}

bool SqlModel::updatePayload(MdPayload mdPayload)
{
    if(mdPayload.getId() < 0)
        return false;

    keepAlive();
    QStringList col,val;
    if(!mdPayload.getName().isEmpty())
    {
        col.append("name=?");
        val.append(mdPayload.getName());
    }
    if(!mdPayload.getScript().isEmpty())
    {
        col.append("script=?");
        val.append(mdPayload.getScript());
    }
    if(!mdPayload.getPayload().isEmpty())
    {
        col.append("payload=?");
        val.append(mdPayload.getPayload());
    }
    if(!mdPayload.getNeedRep().isEmpty())
    {
        col.append("needrep=?");
        val.append(mdPayload.getNeedRep());
    }
    query.prepare(QString("UPDATE payloads SET %1 WHERE id = ?").arg(col.join(",")));
    foreach(QString item, val)
    {
        query.addBindValue(item);
    }
    query.addBindValue(mdPayload.getId());
    return query.exec();
}

MdPayload SqlModel::findPayload(int id)
{
    keepAlive();
    MdPayload mdPayload;
    query.prepare("SELECT * FROM payloads WHERE id=:id");
    query.bindValue(":id", id);
    if(query.exec() && query.next())
    {
        mdPayload.setId(query.value("id").toInt());
        mdPayload.setName(query.value("name").toString());
        mdPayload.setScript(query.value("script").toString());
        mdPayload.setPayload(query.value("payload").toString());
        mdPayload.setNeedRep(query.value("needrep").toString());
    }
    return mdPayload;
}

MdPayload SqlModel::findPayload(QString script, QString name)
{
    keepAlive();
    MdPayload mdPayload;
    query.prepare("SELECT * FROM payloads WHERE script=:script AND name=:name");
    query.bindValue(":script", script);
    query.bindValue(":name", name);
    if(query.exec() && query.next())
    {
        mdPayload.setId(query.value("id").toInt());
        mdPayload.setName(query.value("name").toString());
        mdPayload.setScript(query.value("script").toString());
        mdPayload.setPayload(query.value("payload").toString());
        mdPayload.setNeedRep(query.value("needrep").toString());
    }
    return mdPayload;
}

QString SqlModel::findLogLevel()
{
    keepAlive();
    QString loglevel;
    query.prepare("SELECT * FROM config WHERE cnfname=\"loglevel\"");
    if(query.exec() && query.next())
    {
        loglevel = query.value("cnfval").toString();
    }
    return loglevel;
}

bool SqlModel::updateLogLevel(QString level)
{
    keepAlive();
    query.prepare("UPDATE config SET cnfval=:val WHERE cnfname = \"loglevel\"");
    query.bindValue(":val", level);
    return query.exec();
}

MdProxy SqlModel::findProxy()
{
    MdProxy mdProxy;
    keepAlive();
    query.prepare("SELECT * FROM config WHERE cnfname=\"proxyhost\" OR cnfname=\"proxyport\" OR cnfname=\"proxyptl\" OR cnfname=\"proxyuser\" OR cnfname=\"proxypass\"");
    if(query.exec())
    {
        query.next();
        mdProxy.setHost(query.value("cnfval").toString());
        query.next();
        mdProxy.setPort(query.value("cnfval").toString().toInt());
        query.next();
        mdProxy.setProtocol(query.value("cnfval").toString());
        query.next();
        mdProxy.setUser(query.value("cnfval").toString());
        query.next();
        mdProxy.setPass(query.value("cnfval").toString());
    }
    return mdProxy;
}

bool SqlModel::updateProxy(MdProxy mdProxy)
{
    keepAlive();
    query.prepare("UPDATE config SET cnfval=:proxyhost WHERE cnfname = \"proxyhost\"");
    query.bindValue(":proxyhost", mdProxy.getHost());
    bool retHost = query.exec();
    query.prepare("UPDATE config SET cnfval=:proxyport WHERE cnfname = \"proxyport\"");
    query.bindValue(":proxyport", QString::number(mdProxy.getPort()));
    bool retPort = query.exec();
    query.prepare("UPDATE config SET cnfval=:proxyptl WHERE cnfname = \"proxyptl\"");
    query.bindValue(":proxyptl", mdProxy.getProtocol());
    bool retPtl = query.exec();
    query.prepare("UPDATE config SET cnfval=:proxyuser WHERE cnfname = \"proxyuser\"");
    query.bindValue(":proxyuser", mdProxy.getUser());
    bool retUser = query.exec();
    query.prepare("UPDATE config SET cnfval=:proxypass WHERE cnfname = \"proxypass\"");
    query.bindValue(":proxypass", mdProxy.getPass());
    bool retPass = query.exec();
    return retHost && retPort && retPtl && retUser && retPass;
}
