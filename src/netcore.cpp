#include "global.h"
#include "netcore.h"

NetCore::NetCore()
{
    manager = new QNetworkAccessManager(this);

    // 连接请求响应信号和槽
    connect(manager, &QNetworkAccessManager::finished, this, &NetCore::replyFinished);
}

NetCore::~NetCore()
{
    // 断开请求响应信号和槽
    disconnect(manager, &QNetworkAccessManager::finished, this, &NetCore::replyFinished);
}

MdProxy NetCore::getProxy()
{
    return mdProxy;
}

void NetCore::setProxy(MdProxy mdProxy)
{
    this->mdProxy = mdProxy;

    QNetworkProxy proxy;
    if(mdProxy.getProtocol() == "SOCKS5")
        proxy.setType(QNetworkProxy::Socks5Proxy);
    else if(mdProxy.getProtocol() == "HTTP")
        proxy.setType(QNetworkProxy::HttpProxy);
    proxy.setHostName(mdProxy.getHost());
    proxy.setPort(mdProxy.getPort());
    proxy.setUser(mdProxy.getUser());
    proxy.setPassword(mdProxy.getPass());
    manager->setProxy(proxy);
}

QList<MdHeader> NetCore::getHeaders()
{
    return headers;
}

void NetCore::setHeaders(QList<MdHeader> headers)
{
    this->headers = headers;
}

void NetCore::HttpGetRequest(QString url, QString cookies)
{
    QNetworkRequest request;
    request.setUrl(url);
    request.setHeader(QNetworkRequest::CookieHeader,QVariant::fromValue(parseCookies(cookies)));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    foreach(MdHeader mdHeader, headers)
    {
        request.setRawHeader(mdHeader.getName().toLatin1(), mdHeader.getVal().toLatin1());
    }
    manager->get(request);
}

void NetCore::HttpPostRequest(QString url, QString data, QString cookies)
{
    QNetworkRequest request;
    request.setUrl(url);
    request.setHeader(QNetworkRequest::CookieHeader,QVariant::fromValue(parseCookies(cookies)));
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/json");
    foreach(MdHeader mdHeader, headers)
    {
        request.setRawHeader(mdHeader.getName().toLatin1(), mdHeader.getVal().toLatin1());
    }
    manager->post(request, data.toUtf8());
}

void NetCore::replyFinished(QNetworkReply *reply)
{
    HttpResponse(reply->attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt(),
                           reply->url().toString(),
                           reply->readAll());
    reply->deleteLater();
}

QList<QNetworkCookie> NetCore::parseCookies(QString cookies)
{
    QList<QNetworkCookie> cookiesList;
    QStringList stringList = cookies.split(";");
    foreach(QString co, stringList){
        QStringList val = co.split("=");
        if(val.length() == 2){
            QNetworkCookie cookie(val.at(0).toLatin1(), val.at(1).toLatin1());
            cookiesList.append(cookie);
        }
    }
    return cookiesList;
}
