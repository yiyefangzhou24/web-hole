#include "mdheader.h"

QString MdHeader::getName()
{
    return name;
}

void MdHeader::setName(QString name)
{
    this->name = name;
}

QString MdHeader::getVal()
{
    return val;
}

void MdHeader::setVal(QString val)
{
    this->val = val;
}

int MdHeader::getId()
{
    return id;
}

void MdHeader::setId(int id)
{
    this->id = id;
}
