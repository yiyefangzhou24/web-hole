#ifndef CMDSHELLWINDOW_H
#define CMDSHELLWINDOW_H

#include <QDialog>
#include <QStatusBar>
#include <QTextBlock>
#include <QMessageBox>
#include "mdhole.h"
#include "global.h"

namespace Ui {
class CmdShellWindow;
}

class CmdShellWindow : public QDialog
{
    Q_OBJECT

    enum OS_TYPE{
        WIN,LINUX
    };

public:
    explicit CmdShellWindow(MdHole mdHole, QWidget *parent = nullptr);
    ~CmdShellWindow();

    // 重载关闭函数
    void closeEvent(QCloseEvent *e) override;

    // plainTextEdit的输入事件过滤器
    bool eventFilter(QObject *obj, QEvent *event) override;

private slots:

    // 网络请求响应信号
    void on_response(int type, QString url, QVariant data);

private:



private:
    Ui::CmdShellWindow *ui;

    MdHole mdHole;

    OS_TYPE os;
};

#endif // CMDSHELLWINDOW_H
