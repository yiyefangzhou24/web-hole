#ifndef UPLOADDIALOG_H
#define UPLOADDIALOG_H

#include <QDialog>
#include <QFileDialog>
#include <QMessageBox>

namespace Ui {
class UploadDialog;
}

class UploadDialog : public QDialog
{
    Q_OBJECT

public:
    explicit UploadDialog(QWidget *parent = nullptr);
    ~UploadDialog();

    bool selected = false;

    QString filePath;

    QString fileName;

    int blockSize;

private slots:
    void on_pushButton_ok_clicked();

    void on_pushButton_file_clicked();

    void on_pushButton_cancel_clicked();

private:
    Ui::UploadDialog *ui;
};

#endif // UPLOADDIALOG_H
