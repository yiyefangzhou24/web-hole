#ifndef DATABASEWINDOW_H
#define DATABASEWINDOW_H

#include <QDialog>
#include <QStatusBar>
#include <QDebug>
#include <QMenu>
#include <QTreeWidgetItem>
#include <QStyleFactory>
#include <QFileDialog>
#include <QInputDialog>
#include <QMessageBox>
#include "mdhole.h"
#include "global.h"

namespace Ui {
class DatabaseWindow;
}

class DatabaseWindow : public QDialog
{
    Q_OBJECT

public:
    explicit DatabaseWindow(MdHole mdHole, QWidget *parent = nullptr);
    ~DatabaseWindow();

    // 关闭窗口重载函数
    void closeEvent(QCloseEvent *e);

signals:

    // 刷新Webshell列表信号
    void sig_update_hole(MdHole mdHole);


private slots:

    // 配置按钮点击槽函数
    void on_pushButton_config_clicked();

    // 网络请求响应信号
    void on_response(int type, QString url, QVariant data);

    // 树形列表双击槽函数
    void on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column);

    // 查询按钮点击槽函数
    void on_execButton_clicked();

    // 表格点击事件
    void on_treeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous);

    // 树形列表右键菜单
    void on_treeWidget_customContextMenuRequested(const QPoint &pos);

    // 表格右键菜单
    void on_tableWidget_customContextMenuRequested(const QPoint &pos);

private:

    /**
     * 功能：添加树形目录根节点
     * @param name 添加的根节点名称
     * @return 添加的节点指针
     */
    QTreeWidgetItem * addTreeRootItem(QString name);

    /**
     * 功能：查找根节点
     * @param name 要查找的节点text
     * @return 查找到的首个子节点
     */
    QTreeWidgetItem * findTreeRootItem(QString name);

    /**
     * 功能：添加树形目录根节点
     * @param path 要添加的节点的text
     * @param QTreeWidgetItem 节点指针
     * @return 添加的节点指针
     */
    QTreeWidgetItem * addTreeChildItem(QString name, QTreeWidgetItem *item);


private:
    Ui::DatabaseWindow *ui;

    MdHole mdHole;

    QString currentDB;  //当前数据库

};

#endif // DATABASEWINDOW_H
