#ifndef GLOBAL_H
#define GLOBAL_H

// **
//
// 该文件用于定义全局变量
// |- 该文件用于定义整个程序生命周期只能初始化一次的实例，且任意位置可以使用
//
// created by yiyefangzhou24
// created time 2024/2/9
//
// **

#include "sqlmodel.h"
#include "httpcore.h"

// SQLITE数据库操作实例
extern SqlModel sqlModel;


extern HttpCore http;

#endif // GLOBAL_H
