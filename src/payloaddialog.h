#ifndef PAYLOADDIALOG_H
#define PAYLOADDIALOG_H

#include <QDialog>
#include <QMenu>
#include <QTableWidgetItem>
#include "global.h"
#include "mdpayload.h"

namespace Ui {
class PayloadDialog;
}

class PayloadDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PayloadDialog(QWidget *parent = nullptr);
    ~PayloadDialog();

private slots:
    void on_tableWidget_customContextMenuRequested(const QPoint &pos);

    void on_tableWidget_itemDoubleClicked(QTableWidgetItem *item);

private:
    // UI函数 - 添加一个payload
    void insertListItem(MdPayload mdPayload);

    // UI函数 - 跟新所有payload
    void updateAllPayload();

private:
    Ui::PayloadDialog *ui;

    QList<MdPayload> mdPayloadList;
};

#endif // PAYLOADDIALOG_H
