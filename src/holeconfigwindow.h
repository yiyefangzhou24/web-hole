#ifndef HOLECONFIGWINDOW_H
#define HOLECONFIGWINDOW_H

#include <QDialog>
#include <QMessageBox>
#include <QDateTime>
#include "mdhole.h"

namespace Ui {
class HoleConfigWindow;
}

class HoleConfigWindow : public QDialog
{
    Q_OBJECT

public:
    explicit HoleConfigWindow( QWidget *parent = nullptr);
    ~HoleConfigWindow();

    void setHoleData(MdHole mdHole);

    MdHole mdHole;

private slots:
    void on_submitButton_clicked();

    void on_ivButton_clicked();

    void on_urlEdit_textChanged(const QString &arg1);

private:
    Ui::HoleConfigWindow *ui;
};

#endif // HOLECONFIGWINDOW_H
