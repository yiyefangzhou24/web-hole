#include "databasewindow.h"
#include "ui_databasewindow.h"
#include "dbconfigdialog.h"

DatabaseWindow::DatabaseWindow(MdHole mdHole, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DatabaseWindow)
{
    ui->setupUi(this);
    this->mdHole = mdHole;

    // 设置窗体最大化和最小化
    Qt::WindowFlags windowFlag  = Qt::Dialog;
    windowFlag |= Qt::WindowMinimizeButtonHint;
    windowFlag |= Qt::WindowMaximizeButtonHint;
    windowFlag |= Qt::WindowCloseButtonHint;
    setWindowFlags(windowFlag);

    // 设置UI样式
    ui->treeWidget->setStyle((QStyleFactory::create("windows")));
    ui->treeWidget->header()->setSectionResizeMode(QHeaderView::ResizeMode::ResizeToContents);
    ui->treeWidget->header()->setStretchLastSection(false);
    ui->treeWidget->setHeaderLabels(QStringList{""});
    ui->treeWidget->header()->setFixedHeight(20);
    ui->treeWidget->setContextMenuPolicy(Qt::CustomContextMenu);                    //开启右键菜单
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);              //设置不可编辑
    ui->tableWidget->setSortingEnabled(true);                                         //开启点击表头排序
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);             //设置整行选中
    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);                    //开启右键菜单
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);    //设置自动宽度
    ui->tableWidget->horizontalHeader()->setStretchLastSection(true);
    ui->tableWidget->horizontalHeader()->setFixedHeight(20);
    ui->tableWidget->verticalHeader()->setDefaultSectionSize(24);
    ui->progressBar->setVisible(false);


    // 绑定响应信号
    connect(&http, &HttpCore::sigResponse, this, &DatabaseWindow::on_response);

    // 初始化网络请求参数请求服务器信息
    http.setHole(mdHole);

    // 如果已经有数据库配置，直接发送初始化数据库指令
    if(this->mdHole.getDbType() == "MYSQL")
    {
        http.sqlExecRequest(mdHole.getDbType(), "init", mdHole.getDbCode(), mdHole.getDbHost(), mdHole.getDbUser(), mdHole.getDbPasswd(),"", "SHOW DATABASES");
    }else if(this->mdHole.getDbType() == "MSSQL")
    {
        http.sqlExecRequest(mdHole.getDbType(), "init", mdHole.getDbCode(), mdHole.getDbHost(), mdHole.getDbUser(), mdHole.getDbPasswd(),"", "select [name] from master.dbo.sysdatabases");
    }

    // 以下是测试代码
//    qDebug() << mdHole.getDbType() << mdHole.getDbCode() << mdHole.getDbHost() << mdHole.getDbUser() << mdHole.getDbPasswd();
}

DatabaseWindow::~DatabaseWindow()
{
    delete ui;
}

// *********************** 以下是UI槽函数 ****************************
void DatabaseWindow::closeEvent(QCloseEvent *e)
{
    // 这里是个坑，必须手动disconnect信号关联
    // 因为qt内存new的对话框在短期内没有释放，再次new还是之前的对话框数据，会产生2次信号重复绑定
    disconnect(&http, &HttpCore::sigResponse, this, &DatabaseWindow::on_response);
}


void DatabaseWindow::on_pushButton_config_clicked()
{
    DBConfigDialog dbConfigDlg;
    dbConfigDlg.setDbConfig(mdHole);
    dbConfigDlg.exec();
    if(dbConfigDlg.isOk)
    {
        // 清除所有的显示数据
        ui->treeWidget->clear();
        ui->tableWidget->clear();
        // 更新webshell数据
        mdHole = dbConfigDlg.mdHole;
        sqlModel.updateHole(mdHole);
        // 通知父窗口更新webshell信息
        emit this->sig_update_hole(mdHole);
        // 重新获取数据库基本信息
        if(mdHole.getDbType() == "MYSQL")
        {
            http.sqlExecRequest(mdHole.getDbType(), "init", mdHole.getDbCode(), mdHole.getDbHost(), mdHole.getDbUser(), mdHole.getDbPasswd(),"", "SHOW DATABASES");
        }else if(mdHole.getDbType() == "MSSQL")
        {
            http.sqlExecRequest(mdHole.getDbType(), "init", mdHole.getDbCode(), mdHole.getDbHost(), mdHole.getDbUser(), mdHole.getDbPasswd(),"", "select [name] from master.dbo.sysdatabases");
        }
    }
}

void DatabaseWindow::on_execButton_clicked()
{
    if(ui->comboBox->currentText().isEmpty())
        return;
    http.sqlExecRequest(mdHole.getDbType(), "exec", mdHole.getDbCode(), mdHole.getDbHost(), mdHole.getDbUser(), mdHole.getDbPasswd(),currentDB, ui->comboBox->currentText());
}

void DatabaseWindow::on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    if(!item->parent())
    {
        currentDB = item->text(0);
        ui->label_status->setText(QString("当前数据库:%1").arg(currentDB));
        // 重新获取数据库基本信息
        if(mdHole.getDbType() == "MYSQL")
        {
            http.sqlExecRequest(mdHole.getDbType(), "gettbs", mdHole.getDbCode(), mdHole.getDbHost(), mdHole.getDbUser(), mdHole.getDbPasswd(),item->text(0), QString("SHOW TABLES FROM `%1`").arg(item->text(0)));
        }else if(mdHole.getDbType() == "MSSQL")
        {
            http.sqlExecRequest(mdHole.getDbType(), "gettbs", mdHole.getDbCode(), mdHole.getDbHost(), mdHole.getDbUser(), mdHole.getDbPasswd(),item->text(0), "SELECT name AS TableName FROM sys.tables");
        }
    }
}


void DatabaseWindow::on_treeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
    if(current->parent())       //如果点击的是表，则修改下拉列表内容和当前数据库
    {
        currentDB = current->parent()->text(0);
        ui->label_status->setText(QString("当前数据库:%1").arg(currentDB));

        ui->comboBox->clear();
        ui->comboBox->addItem(QString("SELECT * FROM %1 ORDER BY 1 DESC LIMIT 0,20").arg(current->text(0)));
        ui->comboBox->addItem(QString("SELECT COLUMN_NAME, COLUMN_COMMENT FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA = '%1' AND TABLE_NAME = '%2'").arg(currentDB).arg(current->text(0)));
    }else
    {
        currentDB = current->text(0);
        ui->label_status->setText(QString("当前数据库:%1").arg(currentDB));
    }
}

void DatabaseWindow::on_treeWidget_customContextMenuRequested(const QPoint &pos)
{
    QTreeWidgetItem * item = ui->treeWidget->itemAt(ui->treeWidget->viewport()->mapFromGlobal(QCursor::pos()));

    if(item && item->parent())
    {
        QMenu menu(this);
        QAction * exportAction = menu.addAction(QIcon(":/res/image/export.png"), "导出数据表");
        connect(exportAction, &QAction::triggered, [=](){
            ui->progressBar->setValue(0);
            ui->progressBar->setVisible(true);
            http.exportTbRequest(mdHole.getDbType(), mdHole.getDbCode(), mdHole.getDbHost(), mdHole.getDbUser(), mdHole.getDbPasswd(), currentDB, item->text(0));
        });

        menu.exec(QCursor::pos());
    }
}

void DatabaseWindow::on_tableWidget_customContextMenuRequested(const QPoint &pos)
{
    QTableWidgetItem * item = ui->tableWidget->itemAt(ui->tableWidget->viewport()->mapFromGlobal(QCursor::pos()));
    if(item)
    {
        QMenu menu(this);
        QAction * saveAction = menu.addAction(QIcon(":/res/image/save.png"), "保存结果");
        connect(saveAction, &QAction::triggered, [=](){
            QString saveFile = QFileDialog::getSaveFileName(this, "保存文件", "./", "Text Files(*.txt);;All Files(*.*)");
            if(saveFile.length() > 0)
            {
                QFile file(saveFile);
                if(file.open(QIODevice::WriteOnly|QIODevice::Text))
                {
                    QTextStream out(&file);
                    int row = ui->tableWidget->rowCount();
                    int col = ui->tableWidget->columnCount();
                    for(int i = 0; i < row ; i ++)
                    {
                        QStringList lineList;
                        for (int j=0; j< col; j++) {
                            lineList.append(ui->tableWidget->item(i, j)->text());
                        }
                        out << lineList.join("\t") << "\n";
                    }
                    file.close();
                }else
                {
                    QMessageBox::critical(this, "错误", "创建文件失败");
                }

            }
        });

        menu.exec(QCursor::pos());
    }
}


// ********************** 以下为网络通信槽函数 *****************************
void DatabaseWindow::on_response(int type, QString url, QVariant data)
{
    if(url == mdHole.getUrl())
    {
        if(type == RET_DBEXEC)        // |-- 数据库基本信息
        {
            if(data.toStringList().at(0) == "init")        //初始化数据库
            {
                ui->treeWidget->clear();
                for(int i=2 ;i < data.toStringList().count(); i++)
                {
                    addTreeRootItem(data.toStringList().at(i));
                }
            }else if(data.toStringList().at(0) == "gettbs")     //获取表信息
            {
                if(QTreeWidgetItem * root = findTreeRootItem(currentDB))
                {
                    root->takeChildren();
                    for(int i=2 ;i < data.toStringList().count(); i++)
                    {
                        addTreeChildItem(data.toStringList().at(i), root);
                    }
                    root->setExpanded(true);
                }

            }else if(data.toStringList().at(0) == "exec")     //普通sql执行返回
            {
                ui->tableWidget->clear();
                ui->tableWidget->setRowCount(0);
                QStringList header = data.toStringList().at(1).split("\t|\t");
                ui->tableWidget->setColumnCount(header.count());
                ui->tableWidget->setHorizontalHeaderLabels(header);
                for(int i=2 ;i < data.toStringList().count(); i++)
                {
                    QStringList row = data.toStringList().at(i).split("\t|\t");
                    int curRow = ui->tableWidget->rowCount();
                    ui->tableWidget->insertRow(curRow);
                    for(int j=0; j<row.length(); j++)
                    {
                        ui->tableWidget->setItem(curRow,j,new QTableWidgetItem(row.at(j)));
                    }
                }
            }
        }else if(type == RET_DBEXPORTTB)        // |-- 导出数据表
        {
            // 返回临时文件名称，发送下载文件指令
            qDebug() << data.toString();
            QString saveFile = QFileDialog::getSaveFileName(this,
                                                                "保存数据表",
                                                                "./",
                                                                "TEXT Files(*.txt);;All Files(*.*)");
            if(saveFile.length() > 0)
            {
                http.downloadRequest(data.toString(), saveFile);
            }
        }else if(type == RET_DOWNLOAD)      // |-- 下载文件
        {
            if(data.toStringList().length() == 3)
            {
                int tol = data.toStringList().at(1).toInt();
                int idx = data.toStringList().at(2).toInt();
                ui->progressBar->setVisible(true);
                ui->progressBar->setRange(0, tol);
                ui->progressBar->setValue(idx + 1);
                if(idx +1 >= tol)
                {
                    //下载完成后延时3秒隐藏进度条
                    QTimer * timer = new QTimer(this);
                    timer->setInterval(3000);
                    connect(timer, &QTimer::timeout, [=](){
                        ui->progressBar->setVisible(false);
                        timer->stop();
                        timer->deleteLater();
                    });
                    timer->start();
                }
            }
        }
    }
}

// ********************** 以下为UI私有函数 *****************************
QTreeWidgetItem * DatabaseWindow::addTreeRootItem(QString name)
{
    QTreeWidgetItem *root = new QTreeWidgetItem(ui->treeWidget);
    root->setText(0, name);
    root->setIcon(0, QIcon(":/res/image/db.png"));
    ui->treeWidget->addTopLevelItem(root);
    return root;
}


QTreeWidgetItem * DatabaseWindow::findTreeRootItem(QString name)
{
    for (int i=0; i < ui->treeWidget->topLevelItemCount(); i++) {
        if(ui->treeWidget->topLevelItem(i)->text(0).compare(name, Qt::CaseInsensitive) == 0)
            return ui->treeWidget->topLevelItem(i);
    }
    return nullptr;
}

QTreeWidgetItem * DatabaseWindow::addTreeChildItem(QString name, QTreeWidgetItem * item)
{
    QTreeWidgetItem *childItem = new QTreeWidgetItem(item);
    childItem->setText(0, name);
    childItem->setIcon(0, QIcon(":/res/image/table.png"));
    //item->addChild()
    return childItem;
}
