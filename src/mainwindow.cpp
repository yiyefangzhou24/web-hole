#include "global.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "filemanagewindow.h"
#include "cmdshellwindow.h"
#include "databasewindow.h"
#include "holeconfigwindow.h"
#include "httpheaderdialog.h"
#include "scriptexecwindow.h"
#include "payloaddialog.h"
#include "proxydialog.h"
#include "testallholedialog.h"
#include "mdhole.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // 配置全局样式文件
    QFile file(":/res/qss/psblack.qss");
    if (file.open(QFile::ReadOnly)) {
        qApp->setStyleSheet(QString(file.readAll()));
        file.close();
    }

    // 最小化事件隐藏到系统托盘图标
    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setIcon(QIcon(":/res/image/main.png"));
    trayIcon->setToolTip("WebHole");
    QMenu *trayMenu = new QMenu(this);
    QAction *restoreAction = new QAction(QIcon(":/res/image/main.png"),"显示", this);
    QAction *quitAction = new QAction(QIcon(":/res/image/exit.png"),"退出", this);
    connect(restoreAction, &QAction::triggered, this, &MainWindow::showNormal);
    connect(quitAction, &QAction::triggered, qApp, &QApplication::quit);
    connect(trayIcon, &QSystemTrayIcon::activated, this, &MainWindow::on_trayIcon_activated);
    trayMenu->addAction(restoreAction);
    trayMenu->addAction(quitAction);
    trayIcon->setContextMenu(trayMenu);
    trayIcon->show();

    //设置UI样式
    ui->tableWidget->setColumnCount(6);
    ui->tableWidget->setHorizontalHeaderLabels(QStringList() << "" << "" << "" << "" << "" << "");
    ui->tableWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);              //设置不可编辑
    ui->tableWidget->setSortingEnabled(true);                                         //开启点击表头排序
    ui->tableWidget->setContextMenuPolicy(Qt::CustomContextMenu);                     //开启右键菜单
    ui->tableWidget->setSelectionBehavior(QAbstractItemView::SelectRows);             //设置整行选中
    ui->tableWidget->setSelectionMode(QAbstractItemView::SingleSelection);            //单行选择
    ui->tableWidget->setFocusPolicy(Qt::NoFocus);                                     //关闭选中虚线框
    ui->tableWidget->horizontalHeader()->setFixedHeight(20);
    ui->tableWidget->setColumnWidth(0 , 30);                                              //设置第一列宽度为30
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);   //设置第二列自动宽度
    ui->tableWidget->setColumnWidth(2 , 60);                                             //设置第三列宽度为80
    ui->tableWidget->setColumnWidth(3 , 70);                                             //设置第四列宽度为60
    ui->tableWidget->setColumnWidth(4 , 120);                                             //设置第五列宽度为120
    ui->tableWidget->setColumnWidth(5 , 140);                                            //设置第五列宽度为140

    //设置菜单组，外观是单选的
    QActionGroup* styleActionGroup = new QActionGroup(this);
    styleActionGroup->addAction(ui->action_psblack);
    styleActionGroup->addAction(ui->action_flatgray);
    ui->action_psblack->setCheckable(true);
    ui->action_flatgray->setCheckable(true);
    ui->action_psblack->setChecked(true);
    //设置菜单组，日志等级是单选的
    QActionGroup* loglevelActionGroup = new QActionGroup(this);
    loglevelActionGroup->addAction(ui->action_logdebug);
    loglevelActionGroup->addAction(ui->action_loginfo);
    ui->action_logdebug->setCheckable(true);
    ui->action_loginfo->setCheckable(true);
    //手动设置菜单组的信号和槽
    connect(styleActionGroup, &QActionGroup::triggered , this, &MainWindow::on_styleActionGroup_triggered);
    connect(loglevelActionGroup, &QActionGroup::triggered , this, &MainWindow::on_loglevelActionGroup_triggered);

    // 检测SQLITE数据库配置
    if(!sqlModel.connect("WebHole.db"))
    {
        QLog::log(QString("错误类型:数据库错误|数据库驱动:%1|详细信息:%2").arg(QSqlDatabase::drivers().join(",")).arg(sqlModel.getLastError()));
        QMessageBox::critical(this, "错误", "数据库连接错误，请检查数据库配置和驱动");
        return;
    }

    // 设定HTTP请求全局配置(HTTP请求头、代理设置)
    http.setHeaders(sqlModel.selectHeader());
    http.setProxy(sqlModel.findProxy());

    // 加载Webshell信息
    mdHoleList = sqlModel.selectHole();
    foreach(MdHole mdHole, mdHoleList)
    {
        insertListItem(mdHole);
    }

    // 加载日志等级信息
    sqlModel.findLogLevel() == "DEBUG" ? ui->action_logdebug->setChecked(true) : ui->action_loginfo->setChecked(true);

    // 绑定网络信号
    connect(&http, &HttpCore::sigError, this, &MainWindow::on_error);
    connect(&http, &HttpCore::sigResponse, this, &MainWindow::on_response);

    // 以下为测试代码
}

MainWindow::~MainWindow()
{
    sqlModel.close();
    delete ui;
}

void MainWindow::changeEvent(QEvent *event) {
        if (event->type() == QEvent::WindowStateChange) {
            if (isMinimized()) {
                QTimer::singleShot(0, this, &QWidget::hide);
            }
        }
        QMainWindow::changeEvent(event);
    }

// ******************** 以下为UI私有函数 *****************

void MainWindow::on_trayIcon_activated(QSystemTrayIcon::ActivationReason reason) {
    if (reason == QSystemTrayIcon::DoubleClick) {
        showNormal();
        raise();
        activateWindow();
    }
}

void MainWindow::insertListItem(MdHole mdHole)
{
    int curRow = ui->tableWidget->rowCount();
    ui->tableWidget->insertRow(curRow);

    if(mdHole.getScript() == "ASP") {
        ui->tableWidget->setItem(curRow,0,new QTableWidgetItem(QIcon(":/res/image/asp.png"), ""));
    }else if(mdHole.getScript() == ".NET"){
        ui->tableWidget->setItem(curRow,0,new QTableWidgetItem(QIcon(":/res/image/net.png"), ""));
    }else if(mdHole.getScript() == "PHP"){
        ui->tableWidget->setItem(curRow,0,new QTableWidgetItem(QIcon(":/res/image/php.png"), ""));
    }else if(mdHole.getScript() == "JSP"){
        ui->tableWidget->setItem(curRow,0,new QTableWidgetItem(QIcon(":/res/image/jsp.png"), ""));
    }else if(mdHole.getScript() == "CFM"){
        ui->tableWidget->setItem(curRow,0,new QTableWidgetItem(QIcon(":/res/image/cfm.png"), ""));
    }

    ui->tableWidget->setItem(curRow,1,new QTableWidgetItem(mdHole.getUrl()));
    ui->tableWidget->setItem(curRow,2,new QTableWidgetItem(mdHole.getPasswd()));
    ui->tableWidget->setItem(curRow,4,new QTableWidgetItem(mdHole.getTime()));
    ui->tableWidget->setItem(curRow,5,new QTableWidgetItem(mdHole.getInfo()));

    if(mdHole.getStatus() == "Normal") {
        ui->tableWidget->setItem(curRow,3,new QTableWidgetItem(QIcon(":/res/image/normal.png"),"正常"));
    }else if(mdHole.getStatus() == "Deleted"){
        ui->tableWidget->setItem(curRow,3,new QTableWidgetItem(QIcon(":/res/image/deleted.png"),"丢失"));
    }else if(mdHole.getStatus() == "Useless"){
        ui->tableWidget->setItem(curRow,3,new QTableWidgetItem(QIcon(":/res/image/useless.png"),"失效"));
    }else if(mdHole.getStatus() == "Unknown"){
        ui->tableWidget->setItem(curRow,3,new QTableWidgetItem(QIcon(":/res/image/Unknown.png"), "未知"));
    }

    // 用每一行的第一列data存储该hole的唯一索引id
    ui->tableWidget->item(curRow, 0)->setData(Qt::UserRole, mdHole.getId());
}

void MainWindow::updateListItem(MdHole mdHole)
{
    int curRow = ui->tableWidget->rowCount();
    for(int i = 0; i < curRow ; i ++)
    {
        if(ui->tableWidget->item(i, 0)->data(Qt::UserRole) == mdHole.getId())
        {
            ui->tableWidget->setItem(i,1,new QTableWidgetItem(mdHole.getUrl()));
            ui->tableWidget->setItem(i,2,new QTableWidgetItem(mdHole.getPasswd()));
            ui->tableWidget->setItem(i,4,new QTableWidgetItem(mdHole.getTime()));
            ui->tableWidget->setItem(i,5,new QTableWidgetItem(mdHole.getInfo()));

            if(mdHole.getStatus() == "Normal") {
                ui->tableWidget->setItem(i,3,new QTableWidgetItem(QIcon(":/res/image/normal.png"),"正常"));
            }else if(mdHole.getStatus() == "Deleted"){
                ui->tableWidget->setItem(i,3,new QTableWidgetItem(QIcon(":/res/image/deleted.png"),"丢失"));
            }else if(mdHole.getStatus() == "Useless"){
                ui->tableWidget->setItem(i,3,new QTableWidgetItem(QIcon(":/res/image/useless.png"),"失效"));
            }else if(mdHole.getStatus() == "Unknown"){
                ui->tableWidget->setItem(i,3,new QTableWidgetItem(QIcon(":/res/image/Unknown.png"), "未知"));
            }
            break;
        }
    }
}

// ******************** 以下为UI信号槽函数 **************

// 双击列表槽函数
void MainWindow::on_tableWidget_itemDoubleClicked(QTableWidgetItem *item)
{
    MdHole mdHole = sqlModel.findHole(ui->tableWidget->item(item->row(), 0)->data(Qt::UserRole).toInt());
    FileManageWindow * fileDlg = new FileManageWindow(mdHole, this);
    fileDlg->show();
}

void MainWindow::on_action_http_header_triggered()
{
    HttpHeaderDialog httpHeaderDlg;
    httpHeaderDlg.exec();
    // http头编辑完成后，更新http类中对应信息
    http.setHeaders(sqlModel.selectHeader());
}

void MainWindow::on_action_log_triggered()
{
    QDesktopServices::openUrl(QUrl(APP_FILE));
}

void MainWindow::on_action_about_app_triggered()
{
    QMessageBox::information(this, "关于软件", "WebHole v1.3.1.1\n©2024 WebHole. All rights reserved.\n\n本程序仅供研究和测试使用，请勿用于非法用途\n\n本程序使用LGPL标准");
}

void MainWindow::on_action_payload_triggered()
{
    PayloadDialog payloadDlg;
    payloadDlg.exec();
}

void MainWindow::on_action_testall_triggered()
{
    // 临时解绑错误和响应信号
    disconnect(&http, &HttpCore::sigError, this, &MainWindow::on_error);
    disconnect(&http, &HttpCore::sigResponse, this, &MainWindow::on_response);

    TestAllHoleDialog testAllHoleDlg(mdHoleList);
    testAllHoleDlg.exec();
    on_update_all_hole();

    // 重新绑定错误和响应信号
    connect(&http, &HttpCore::sigError, this, &MainWindow::on_error);
    connect(&http, &HttpCore::sigResponse, this, &MainWindow::on_response);
}

void MainWindow::on_action_exit_triggered()
{
    close();
}

void MainWindow::on_styleActionGroup_triggered(QAction *action)
{
    QString resFile = ":/res/qss/" + (action->objectName().length() > 7 ? action->objectName().mid(7) : "") + ".qss";
    QFile file(resFile);
    if (file.open(QFile::ReadOnly)) {
        qApp->setStyleSheet(QString(file.readAll()));
        file.close();
    }
}

void MainWindow::on_loglevelActionGroup_triggered(QAction *action)
{
    if(action->objectName() == "action_logdebug")
    {
        sqlModel.updateLogLevel("DEBUG");
    }else if(action->objectName() == "action_loginfo")
    {
        sqlModel.updateLogLevel("INFO");
    }
}

void MainWindow::on_action_proxy_triggered()
{
    ProxyDialog proxyDlg;
    proxyDlg.exec();

    // proxy编辑完成后，更新http类中对应信息
    http.setProxy(sqlModel.findProxy());
}

// ******************** 以下为网络信号槽函数 **************
void MainWindow::on_error(ERR_CODE code, QString url, QString msg)
{
    qDebug() << "错误类型:" << code << "|网址：" << url << "|错误信息："<< msg;

    if(code == ERR_NET){    // 网络说明shell丢失
        int curRow = ui->tableWidget->rowCount();
        for(int i = 0; i < curRow ; i ++)
        {
            if(ui->tableWidget->item(i, 1)->text() == url)
            {
                ui->tableWidget->setItem(i,3,new QTableWidgetItem(QIcon(":/res/image/deleted.png"),"丢失"));
                MdHole mdHole;
                mdHole.setId(ui->tableWidget->item(i, 0)->data(Qt::UserRole).toInt());
                mdHole.setStatus("Deleted");
                sqlModel.updateHole(mdHole);
                break;
            }
        }

    }else if(code == ERR_FORMAT){               // 返回格式错误可能是shell失效
        int curRow = ui->tableWidget->rowCount();
        for(int i = 0; i < curRow ; i ++)
        {
            if(ui->tableWidget->item(i, 1)->text() == url)
            {
                ui->tableWidget->setItem(i,3,new QTableWidgetItem(QIcon(":/res/image/useless.png"),"失效"));
                MdHole mdHole;
                mdHole.setId(ui->tableWidget->item(i, 0)->data(Qt::UserRole).toInt());
                mdHole.setStatus("Useless");
                sqlModel.updateHole(mdHole);
                break;
            }
        }
    }
    QLog::log(QString("错误类型:%1|网址:%2|详细信息:%3").arg(strErrType.at(code)).arg(url).arg(msg));
    QMessageBox::critical(this, "错误", QString("错误类型:%1\n网址:%2\n\n详细信息:%3\n").arg(strErrType.at(code)).arg(url).arg(msg));
}

void MainWindow::on_response(int type, QString url, QVariant data)
{
    if(type == RET_TEST)        // |-- 测试可用性
    {
        MdHole mdHole = sqlModel.findHole(url);
        if(data.toBool())
        {
            mdHole.setStatus("Normal");
            ui->tableWidget->setItem(ui->tableWidget->currentRow(),3,new QTableWidgetItem(QIcon(":/res/image/normal.png"),"正常"));
        }else
        {
            mdHole.setStatus("Useless");
            ui->tableWidget->setItem(ui->tableWidget->currentRow(),3,new QTableWidgetItem(QIcon(":/res/image/useless.png"),"失效"));
        }
        sqlModel.updateHole(mdHole);
    }
}

// 右键菜单槽函数
void MainWindow::on_tableWidget_customContextMenuRequested(const QPoint &pos)
{
    QMenu menu(this);
    QAction * fileAction = menu.addAction(QIcon(":/res/image/path.png"), "文件管理");
    QAction * databaseAction = menu.addAction(QIcon(":/res/image/database.png"), "数据库管理");
    QAction * shellAction = menu.addAction(QIcon(":/res/image/shell.png"), "虚拟终端");
    QAction * execAction = menu.addAction(QIcon(":/res/image/execscript.png"), "运行脚本");
    menu.addSeparator();
    QAction * testAction = menu.addAction(QIcon(":/res/image/test.png"), "有效性测试");
    menu.addSeparator();
    QAction * editAction = menu.addAction(QIcon(":/res/image/edit.png"), "编辑");
    QAction * deleteAction = menu.addAction(QIcon(":/res/image/delete.png"), "删除");
    QAction * addAction = menu.addAction(QIcon(":/res/image/add.png"), "添加");

    connect(fileAction, &QAction::triggered, [=](){
        MdHole mdHole = sqlModel.findHole(ui->tableWidget->item(ui->tableWidget->currentRow(), 0)->data(Qt::UserRole).toInt());
        FileManageWindow * fileDlg = new FileManageWindow(mdHole, this);
        fileDlg->show();
    });

    connect(databaseAction, &QAction::triggered, [=](){
        MdHole mdHole = sqlModel.findHole(ui->tableWidget->item(ui->tableWidget->currentRow(), 0)->data(Qt::UserRole).toInt());
        DatabaseWindow *dataDlg = new DatabaseWindow(mdHole, this);
        connect(dataDlg, &DatabaseWindow::sig_update_hole, this, &MainWindow::on_update_one_hole);
        dataDlg->show();
    });
    connect(shellAction, &QAction::triggered, [=](){
        MdHole mdHole = sqlModel.findHole(ui->tableWidget->item(ui->tableWidget->currentRow(), 0)->data(Qt::UserRole).toInt());
        CmdShellWindow *shellDlg = new CmdShellWindow(mdHole, this);
        shellDlg->show();
    });
    connect(execAction, &QAction::triggered, [=](){
        MdHole mdHole = sqlModel.findHole(ui->tableWidget->item(ui->tableWidget->currentRow(), 0)->data(Qt::UserRole).toInt());
        ScriptExecWindow *scriptDlg = new ScriptExecWindow(mdHole, this);
        scriptDlg->show();
    });
    connect(editAction, &QAction::triggered, [=](){
        MdHole mdHole = sqlModel.findHole(ui->tableWidget->item(ui->tableWidget->currentRow(), 0)->data(Qt::UserRole).toInt());
        HoleConfigWindow holeDlg;
        holeDlg.setWindowTitle("编辑");
        holeDlg.setWindowIcon(QIcon(":/res/image/edit.png"));
        holeDlg.setHoleData(mdHole);
        holeDlg.exec();
        on_update_one_hole(holeDlg.mdHole);
        //connect(holeDlg, &HoleConfigWindow::sig_update_hole, this, &MainWindow::on_update_hole);
    });
    connect(deleteAction, &QAction::triggered, [=](){
        if(QMessageBox::question(this, "提示", "确定删除？") == QMessageBox::Yes)
        {
            if(sqlModel.deleteHole(ui->tableWidget->item(ui->tableWidget->currentRow(), 0)->data(Qt::UserRole).toInt()))
                ui->tableWidget->removeRow(ui->tableWidget->currentRow());
        };
    });
    connect(testAction, &QAction::triggered, [=](){
        MdHole mdHole = sqlModel.findHole(ui->tableWidget->item(ui->tableWidget->currentRow(), 0)->data(Qt::UserRole).toInt());
        http.setHole(mdHole);
        http.testRequest();
    });
    connect(addAction, &QAction::triggered, [=](){
        HoleConfigWindow holeDlg;
        holeDlg.setWindowTitle("添加");
        holeDlg.setWindowIcon(QIcon(":/res/image/add.png"));
        holeDlg.exec();
        on_update_all_hole();   //这里必须刷新列表，因为添加的webshell没有id
    });

    if(ui->tableWidget->itemAt(ui->tableWidget->viewport()->mapFromGlobal(QCursor::pos())) == nullptr){
        fileAction->setVisible(false);
        databaseAction->setVisible(false);
        shellAction->setVisible(false);
        editAction->setVisible(false);
        deleteAction->setVisible(false);
        testAction->setVisible(false);
        execAction->setVisible(false);
    }

    menu.exec(QCursor::pos());
}



// ******************** 以下为自定义信号槽函数 **************
void MainWindow::on_update_all_hole()
{
    ui->tableWidget->clearContents();
    ui->tableWidget->setRowCount(0);
    mdHoleList = sqlModel.selectHole();
    foreach(MdHole mdHole, mdHoleList)
    {
        insertListItem(mdHole);
    }
}

void MainWindow::on_update_one_hole(MdHole mdHole)
{
    // 修改列表
    for(int i=0 ; i< mdHoleList.size(); i++)
    {
        if(mdHoleList[i].getId() == mdHole.getId())
        {
            mdHoleList[i] = mdHole;
            break;
        }
    }
    // 更新UI
    updateListItem(mdHole);
}
