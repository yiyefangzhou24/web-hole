#ifndef MDPROXY_H
#define MDPROXY_H
// **
//
// Proxy Model类
//    仅用来存放代理配置数据集
//
// created by yiyefangzhou24
// created time 2024/4/29
//
// **
#include <QString>

class MdProxy
{
public:

    QString getHost();

    void setHost(QString host);

    int getPort();

    void setPort(int port);

    QString getProtocol();

    void setProtocol(QString protocol);

    QString getUser();

    void setUser(QString user);

    QString getPass();

    void setPass(QString pass);

private:
    QString host;

    int port = 0;

    QString protocol;

    QString user;

    QString pass;
};

#endif // MDPROXY_H
