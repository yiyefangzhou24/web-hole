#ifndef PAYLOADEDITDIALOG_H
#define PAYLOADEDITDIALOG_H

#include <QDialog>
#include <QMessageBox>
#include <QMap>
#include "mdpayload.h"
#include "global.h"

namespace Ui {
class PayloadEditDialog;
}

class PayloadEditDialog : public QDialog
{
    Q_OBJECT

public:
    explicit PayloadEditDialog(QWidget *parent = nullptr);
    ~PayloadEditDialog();

    void setPayload(MdPayload mdPayload);

private slots:
    // 提交修改信号槽
    void on_pushButton_submit_clicked();

    // 取消信号槽
    void on_pushButton_cancel_clicked();

    // 载荷类型信号槽
    void on_comboBox_name_currentIndexChanged(int index);

private:
    Ui::PayloadEditDialog *ui;

    MdPayload mdPayload;

    QMap <QString,QString> payloadNames;

    QMap <QString,QString> payloadInfos;
};

#endif // PAYLOADEDITDIALOG_H
