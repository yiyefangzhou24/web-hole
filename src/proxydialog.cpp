#include "proxydialog.h"
#include "ui_proxydialog.h"
#include "global.h"

ProxyDialog::ProxyDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ProxyDialog)
{
    ui->setupUi(this);

    //固定窗口大小
    setFixedSize(this->width(), this->height());
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);

    //设置UI
    ui->comboBox_protocol->addItem("请选择", "");
    ui->comboBox_protocol->addItem("HTTP", "HTTP");
    ui->comboBox_protocol->addItem("SOCKS5", "SOCKS5");

    MdProxy mdProxy = sqlModel.findProxy();
    ui->comboBox_protocol->setCurrentText(mdProxy.getProtocol());
    ui->lineEdit_host->setText(mdProxy.getHost());
    if(mdProxy.getPort() > 0)
        ui->lineEdit_port->setText(QString::number(mdProxy.getPort()));
    ui->lineEdit_user->setText(mdProxy.getUser());
    ui->lineEdit_pass->setText(mdProxy.getPass());
}

ProxyDialog::~ProxyDialog()
{
    delete ui;
}


// *********************** 以下为UI槽函数 ********************

void ProxyDialog::on_pushButton_cancel_clicked()
{
    close();
}

void ProxyDialog::on_pushButton_save_clicked()
{
//    if(ui->lineEdit_host->text().isEmpty() || ui->lineEdit_port->text().isEmpty() || ui->comboBox_protocol->currentData().toString().isEmpty())
//    {
//        QMessageBox::critical(this, "提示", "请正确填写代理配置");
//        return;
//    }

    MdProxy mdProxy;
    mdProxy.setHost(ui->lineEdit_host->text());
    mdProxy.setPort(ui->lineEdit_port->text().toUInt());
    mdProxy.setProtocol(ui->comboBox_protocol->currentData().toString());
    mdProxy.setUser(ui->lineEdit_user->text());
    mdProxy.setPass(ui->lineEdit_pass->text());

    if(sqlModel.updateProxy(mdProxy))
    {
        close();
    }else
    {
        QMessageBox::critical(this, "错误", "配置修改失败" + sqlModel.getLastError());
    }
}

void ProxyDialog::on_pushButton_clear_clicked()
{
    ui->comboBox_protocol->setCurrentText("请选择");
    ui->lineEdit_host->setText("");
    ui->lineEdit_port->setText("");
    ui->lineEdit_user->setText("");
    ui->lineEdit_pass->setText("");
}
