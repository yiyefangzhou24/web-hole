#include "mdfileinfo.h"

TYPE MdFileInfo::getType()
{
    return type;
}

void MdFileInfo::setType(TYPE type)
{
    this->type = type;
}

QString MdFileInfo::getName()
{
    return name;
}

void MdFileInfo::setName(QString name)
{
    this->name = name;
}

QString MdFileInfo::getPath()
{
    return path;
}

void MdFileInfo::setPath(QString path)
{
    this->path = path;
}

QString MdFileInfo::getTime()
{
    return time;
}

void MdFileInfo::setTime(QString time)
{
    this->time = time;
}

QString MdFileInfo::getSize()
{
    return size;
}

void MdFileInfo::setSize(QString size)
{
    this->size = size;
}

QString MdFileInfo::getFullName()
{
    if(path.contains('\\') || path.contains(':'))
        return QString("%1\\%2").arg(path).arg(name);
    else
    {
        return path == "/" ? QString("/%1").arg(name): QString("%1/%2").arg(path).arg(name);
    }
}

QIcon MdFileInfo::getIcon()
{
    if(path.contains('\\') || path.contains(':'))
    {
        QFileInfo file_info(getFullName());
        QFileIconProvider icon_provider;
        return icon_provider.icon(file_info);
    }
    else
    {
        QFileInfo file_info("C:"+getFullName());
        QFileIconProvider icon_provider;
        return icon_provider.icon(file_info);
    }

}
