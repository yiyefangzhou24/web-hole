#ifndef SQLMODEL_H
#define SQLMODEL_H

// **
//
// sqlite数据库操作Model类
//    该类的生命周期应该和主窗口MainWindow的生命周期一样
//
// created by yiyefangzhou24
// created time 2023/1/13
//
// **

#include <QSqlDatabase>
#include <QSqlQuery>
#include <QSqlError>
#include <QApplication>
#include <QFile>
#include <QList>
#include <QDebug>
#include "mdhole.h"
#include "mdheader.h"
#include "mdpayload.h"
#include "mdproxy.h"

class SqlModel
{
public:
//    SqlModel();

//    SqlModel(QString dbname);

    // 连接数据库
    bool connect(QString dbname = "");

    // 断开数据库连接
    void close();

    // 获取最后一次错误信息
    QString getLastError();

    // 添加webshell
    bool insertHole(MdHole mdHole);

    // 修改webshell
    bool updateHole(MdHole mdHole);

    // 查询所有的webshell
    QList<MdHole> selectHole();

    // 查询符合条件的webshell
    MdHole findHole(int id);

    // 查询符合条件的webshell
    MdHole findHole(QString url);

    // 删除shell
    bool deleteHole(int id);

    // 查询所有的HTTP请求头
    QList<MdHeader> selectHeader();

    // 添加HTTP请求头
    bool insertHeader(MdHeader mdHeader);

    // 删除HTTP请求头
    bool deleteHeader(int id);

    // 修改HTTP请求头
    bool updateHeader(MdHeader mdHeader);

    // 查询符合条件的HTTP请求头
    MdHeader findHeader(int id);

    // 查询所有攻击载荷
    QList<MdPayload> selectPayload();

    // 添加攻击载荷
    bool insertPayload(MdPayload mdPayload);

    // 删除攻击载荷
    bool deletePayload(int id);

    // 修改攻击载荷
    bool updatePayload(MdPayload mdPayload);

    // 查询符合条件的HTTP请求头
    MdPayload findPayload(int id);

    // 查询符合条件的HTTP请求头
    MdPayload findPayload(QString script, QString name);

    // 查询日志等级
    QString findLogLevel();

    // 修改日志等级
    bool updateLogLevel(QString level);

    // 查询代理信息
    MdProxy findProxy();

    // 更新代理信息
    bool updateProxy(MdProxy mdProxy);

private:

    // 初始化表
    bool initTables();

    // 保持数据库连接
    void keepAlive();

private:

    QSqlDatabase sqlDB;

    QSqlQuery query;
};

#endif // SQLMODEL_H
