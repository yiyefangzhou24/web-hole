#ifndef DBCONFIGDIALOG_H
#define DBCONFIGDIALOG_H

#include <QDialog>
#include "mdhole.h"

namespace Ui {
class DBConfigDialog;
}

class DBConfigDialog : public QDialog
{
    Q_OBJECT

public:
    explicit DBConfigDialog(QWidget *parent = nullptr);
    ~DBConfigDialog();

    void setDbConfig(MdHole mdHole);

    MdHole mdHole;

    bool isOk = false;

private slots:
    void on_pushButton_submit_clicked();

    void on_pushButton_cancel_clicked();

private:
    Ui::DBConfigDialog *ui;
};

#endif // DBCONFIGDIALOG_H
