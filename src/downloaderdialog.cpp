#include "downloaderdialog.h"
#include "ui_downloaderdialog.h"

DownloaderDialog::DownloaderDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DownloaderDialog)
{
    ui->setupUi(this);

    //固定窗口大小
    setFixedSize(this->width(), this->height());
    setWindowFlags(windowFlags() & ~Qt::WindowContextHelpButtonHint);
}

DownloaderDialog::~DownloaderDialog()
{
    delete ui;
}


// ******************** 以下为UI槽函数 ******************
void DownloaderDialog::on_pushButton_submit_clicked()
{
    url = ui->lineEdit_url->text();
    filename = ui->lineEdit_filename->text();
    if(url.isEmpty() || filename.isEmpty())
    {
        QMessageBox::critical(this, "提示", "请输入远程地址和要保存的文件名");
        return;
    }
    close();
}

void DownloaderDialog::on_pushButton_cancel_clicked()
{
    url.clear();
    filename.clear();
    close();
}

void DownloaderDialog::on_lineEdit_url_textChanged(const QString &arg1)
{
    QUrl qUrl(arg1);
    if(qUrl.isValid() && (qUrl.scheme() == "http" || qUrl.scheme() == "https") && !qUrl.fileName().isEmpty())
    {
        ui->lineEdit_filename->setText(qUrl.fileName());
    }

}
