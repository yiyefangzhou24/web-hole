#ifndef TESTALLHOLEDIALOG_H
#define TESTALLHOLEDIALOG_H

#include <QDialog>
#include "mdhole.h"
#include "httpcore.h"

namespace Ui {
class TestAllHoleDialog;
}

class TestAllHoleDialog : public QDialog
{
    Q_OBJECT

public:
    explicit TestAllHoleDialog(QList<MdHole> holeList, QWidget *parent = nullptr);
    ~TestAllHoleDialog();

private:

    // 获取一个检活结果后，更新UI显示，修改db文件
    void updateHole(QString url, QString status);

private slots:

    // 错误信息信号槽
    void on_error(ERR_CODE code, QString url, QString msg);

    // 网络请求响应信号
    void on_response(int type, QString url, QVariant data);

protected:

    // 窗口关闭信号
    void closeEvent(QCloseEvent *event) override;
private:
    Ui::TestAllHoleDialog *ui;

    QList<MdHole> mdHoleList;   // webshell列表的镜像复制

    int testNum = 0;            // 用来记录有测试结果的shell数量

    QStringList strErrType = {"数据格式错误" , "网络错误", "脚本执行错误", "控制端程序错误"};

};

#endif // TESTALLHOLEDIALOG_H
